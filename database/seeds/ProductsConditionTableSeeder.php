<?php

use Illuminate\Database\Seeder;

class ProductsConditionTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table("products_condition")->insert([
    	["condition_id" => \App\Helper::getUID(), "name" => 'Brand New', 				"created_at" => \Carbon\Carbon::now()],
    	["condition_id" => \App\Helper::getUID(), "name" => 'Mint', 						"created_at" => \Carbon\Carbon::now()],
    	["condition_id" => \App\Helper::getUID(), "name" => 'Excellent', 				"created_at" => \Carbon\Carbon::now()],
    	["condition_id" => \App\Helper::getUID(), "name" => 'Very Good', 				"created_at" => \Carbon\Carbon::now()],
    	["condition_id" => \App\Helper::getUID(), "name" => 'Good', 						"created_at" => \Carbon\Carbon::now()],
    	["condition_id" => \App\Helper::getUID(), "name" => 'Fair', 						"created_at" => \Carbon\Carbon::now()],
    	["condition_id" => \App\Helper::getUID(), "name" => 'Poor', 						"created_at" => \Carbon\Carbon::now()],
    	["condition_id" => \App\Helper::getUID(), "name" => 'Non Functioning', 	"created_at" => \Carbon\Carbon::now()]
    ]);
    $this->command->info('Products Condition has been successfully seeded');
  }
}
