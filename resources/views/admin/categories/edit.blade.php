@extends('layouts.admin.app')

@section('title', 'Edit Category')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>Categories</strong>
          </div>

          <div class="panel-body">
            @if( count($category) > 0 )
              {!! Form::open(['route' => ['admin::update-category', $category->category_id], 'class' => 'form-horizontal', 'method' => 'put']) !!}

                <!-- Name -->
                <div class="form-group{{ $errors->has('category_name') ? ' has-error' : '' }}">
                  <label for="category_name" class="col-sm-4 control-label">Category Name</label>
                  <div class="col-sm-6">
                    {!! Form::text('category_name', $category->name, ['class' => 'form-control', 'id' => 'category_name', 'placeholder' => 'Category Name']) !!}
                    @if ($errors->has('category_name'))
                      <span class="help-block">
                        <strong>{{ $errors->first('category_name') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <!-- Description -->
                <div class="form-group{{ $errors->has('category_description') ? ' has-error' : '' }}">
                  <label for="category_description" class="col-sm-4 control-label">Category Description</label>
                  <div class="col-sm-6">
                    {!! Form::textarea('category_description', $category->description, ['class' => 'form-control', 'id' => 'category_description', 'placeholder' => 'Category Description', 'rows' => '4']) !!}
                    @if ($errors->has('category_description'))
                      <span class="help-block">
                        <strong>{{ $errors->first('category_description') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <!-- Parent -->
                <div class="form-group">
                  <label for="category_parent" class="col-sm-4 control-label">Parent</label>
                  <div class="col-sm-6">
                    <select name="category_parent" id="category_parent" class="form-control">
                      <option value="-">None</option>
                      @if( count($categories) > 0 )
                        @foreach($categories as $cat)
                          <option {{ $category->parent == $cat->category_id ? 'selected' : '' }} value="{{ $cat->category_id }}">{{ $cat->name }}</option>
                        @endforeach
                      @endif
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-6">
                    <button type="submit" class="btn btn-primary">
                      <i class="fa fa-save fa-fw"></i> Save
                    </button>
                  </div>
                </div>
              {!! Form::close() !!}
            @else
              <div class="alert alert-danger text-center">
                Category does not exist
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
