@extends('layouts.admin.app')

@section('title', ($blog ? $blog->title : 'Update Blog'))

@section('content')
    
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Update Blog</strong>
                        <small class="pull-right"><a href="{{ route('admin::createBlog') }}"><i class="fa fa-plus"></i> Create blog</a></small>
                    </div>

                    <div class="panel-body">
                        @if( $blog )
                            {!! Form::open(['url' => route('admin::updateBlog', [$blogId]), 'class' => 'form-horizontal', 'id' => 'updateBlog', 'files' => true]) !!}
                                <!-- Photo -->
                                <div class="form-group">
                                    <label for="blogTitle" class="col-sm-2 control-label">Photo</label>
                                    <div class="col-sm-8">
                                        <img class="img-responsive" src="{{ url('images/blogs/thumbs/', [$blog->photo]) }}" alt="{{ $blog->title }}">
                                    </div>
                                </div>

                                <!-- Title -->
                                <div class="form-group">
                                    <label for="blogTitle" class="col-sm-2 control-label">Title</label>
                                    <div class="col-sm-8">
                                        {!! Form::text('title', $blog->title, ['class' => 'form-control', 'id' => 'blogTitle', 'placeholder' => 'Title here']) !!}
                                    </div>
                                </div>

                                <!-- Description -->
                                <div class="form-group">
                                    <label for="blogContent" class="col-sm-2 control-label">Content</label>
                                    <div class="col-sm-8">
                                        {!! Form::textarea('content', $blog->content, ['class' => 'form-control', 'id' => 'blogContent', 'placeholder' => 'Content here...']) !!}
                                    </div>
                                </div>

                                <!-- Photo -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">&nbsp;</label>
                                    <div class="col-sm-8">
                                        <label for="blogPhoto" class="control-label">Photo</label>
                                        <input type="file" name="photo" id="blogPhoto" accept="image/x-png, image/gif, image/jpeg">
                                        <p class="help-block">This is optional. If has a file, current photo will be replaced</p>
                                    </div>
                                </div>

                                <!-- Status -->
                                <div class="form-group">
                                    <label for="blogStatus" class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-3">
                                        <select name="status" id="blogStatus" class="form-control">
                                            <option value="active" {!! $blog->status == 'active' ? 'selected' : '' !!}>Active</option>
                                            <option value="inactive" {!! $blog->status == 'inactive' ? 'selected' : '' !!}>Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-8">
                                        <a href="{{ route('admin::blogs') }}" title="" class="btn btn-default">
                                            <i class="fa fa-fw fa-arrow-left"></i> Back
                                        </a>
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-save fa-fw"></i> Save
                                        </button>
                                    </div>
                                </div>

                            {!! Form::close() !!}
                        @else
                            <div class="alert alert-danger text-center">
                                <strong>Error!</strong> Blog does not exist
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if( $blog )
        <script type="text/javascript">
            $(document).ready(function(){
                $('#updateBlog').on('submit', function(e){
                    var self = $(this);
                    var btn = self.find('.btn').button('loading');

                    e.preventDefault();
                    self.ajaxSubmit({
                        url: "{{ route('admin::updateBlog', [$blogId]) }}",
                        type: 'post',
                        success: function(response){
                            btn.button('reset');
                            showToastr(response.status, response.message);
                        }
                    });

                });
            });
        </script>
    @endif

@endsection
