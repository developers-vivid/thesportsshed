@extends('layouts.admin.app')

@section('title', 'Blogs')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Blogs</strong>
                        <small class="pull-right"><a href="{{ route('admin::createBlog') }}"><i class="fa fa-plus"></i> Create blog</a></small>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-8">
                                <blockquote>
                                    <small>List of all blogs</small>
                                </blockquote>
                            </div>
                            <div class="col-md-4">
                                {!! Form::open(['method' => 'get']) !!}
                                    <div class="form-group">
                                        <div class='input-group date'>
                                            {!! Form::text('keyword', \Input::get('keyword'), ['class' => 'form-control', 'placeholder' => 'Search blog here...']) !!}
                                            <span class="input-group-addon">
                                                <span class="fa fa-search fa-fw"></span>
                                            </span>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>

                        <table class="table table-striped table-hovered" style="margin-bottom: 0px;">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th class="text-center">Respondents</th>
                                    <th>Created</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if( $blogs->count() > 0 )
                                    @foreach($blogs as $blog)
                                        <tr>
                                            <td>
                                                <code data-toggle="tooltip" title="{{ $blog->title }}"><strong>{{ str_limit($blog->title, 50, '...') }}</strong></code>
                                            </td>
                                            <td><span class="label label-info">{{ $blog->author }}</span></td>
                                            <td class="text-center">
                                                <strong>{{ \App\BlogsComments::whereBlogId( $blog->blog_id )->count() }}</strong>
                                            </td>
                                            <td>
                                                <small data-toggle="tooltip" data-placement="top" title="{{ $blog->created_at }}">
                                                    {{ \Carbon\Carbon::createFromTimeStamp( strtotime($blog->created_at) )->diffForHumans() }}
                                                </small>
                                            </td>
                                            <td>
                                                {!! Form::open(['url' => route('admin::deleteBlog', [$blog->blog_id]), 'id' => 'delete-blog-'. $blog->blog_id, 'method' => 'POST', 'style' => 'display: none;']) !!}
                                                {!! Form::close() !!}
                                                <div class="btn-group" role="group" aria-label="...">
                                                    <a href="{{ route('admin::updateBlog', [$blog->blog_id]) }}" title="" class="btn btn-default btn-xs">
                                                        edit
                                                    </a>
                                                    <button type="button" class="btn btn-danger btn-xs" onclick="event.preventDefault(); document.getElementById('delete-blog-{{ $blog->blog_id }}').submit();">delete</button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5" class="text-center">No blog(s) yet</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer" style="padding: 0;">
                        {!! $blogs->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
