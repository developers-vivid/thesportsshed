@extends('layouts.master')

@section('title', "Cart")

@section('content')

<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
   
    <div id="main-container" class="single-page checkout">
        <div class="container">
            <div class="row main-row">
                <div class="col-lg-12">
				
					<!--
                	<div class="clearfix">
                		<h3 class="pull-left">My Cart</h3>
	                	<div class="pull-right">
	                		<img src="{{ url('images/GearPlanet_Logo.png') }}" class="img-responsive" alt="Gear Planet logo" title="Gear Planet logo" />
	                	</div>
                	</div>
					-->
					
                	<div class="checkout-items">
                		@if( count($cart) > 0 )
	                		<table class="table-checkout">
	                			<thead>
	                				<tr class="header">
		                				<th style="background-color:#6ed5d4;">Your Office</th>
		                				@if( ! config('gp_conf.isBuyNow') )
			                				<th>Item price</th>
			                				<th>Quantity</th>
		                				@endif
		                				<th style="background-color:#6ed5d4;">Amount</th>
		                			</tr>
	                			</thead>
	                			<tbody>
	                				<?php $theTotal = 0; $shippingCost = 0; ?>
                					@foreach( $cart as $crt )
                						<?php
                							if( ! is_null($crt->shipping_cost) )
                								$shippingCost += $crt->shipping_cost;

                							$theTotal+=$subTotal = ($crt->product_price * $crt->qty);
                						?>
		                				<tr class="item">
			                				<td>
			                					<div class="checkout-img inline-block">
													<p style="width:100%;"><strong>{{ $crt->product_title }}</strong></p>
			                						<a href="{{ route('gears::marketplace', [$crt->product_blurb]) }}">
			                							<img class="img-responsive" src="{{ url('/images/gears/thumbs/', [$crt->photo_filename]) }}">
			                						</a>
			                					</div>
			                					<div class="checkout-description inline-block">
			                						<div class="item-header">			                							
			                						</div>
			                						<div class="item-body">
			                							<div class="item-body">
				                							<div class="item">
				                								<div class="item-content">
				                									<div class="label-item inline-block block" style="color:#1c241c;">Floor Area -</div>
				                									<div class="content inline-block block" style="color:#a1a3a1;">{{ $crt->product_condition }}</div>
				                								</div>

				                								<div class="item-content">
				                									<div class="label-item inline-block block" style="color:#1c241c;">Tenure Type -</div>
					                								<div class="content inline-block block" style="color:#a1a3a1;">{{ ucfirst($crt->product_type) }}</div>
				                								</div>

					                							<div class="item-content">
					                								<div class="label-item inline-block block" style="color:#1c241c;">Lease Term -</div>
					                								<div class="content inline-block block" style="color:#a1a3a1;">{{ $crt->product_model }}</div>
					                							</div>

					                							<div class="item-content">
					                								<div class="label-item inline-block block" style="color:#1c241c;">Car Spaces -</div>
					                								<div class="content inline-block block" style="color:#a1a3a1;">{{ ucwords($crt->product_made_in) }}</div>
					                							</div>
																
																<!--
					                							<div class="item-content">
					                								<div class="label-item inline-block block">Categories:</div>
					                								<div class="content inline-block block">{{ $crt->category_name }}</div>
					                							</div>
																-->
																
					                							<!-- <div class="item-content">
					                								<div class="label-item inline-block block">Shipping Amount:</div>
					                								<div class="content inline-block block">{{ $crt->category_name }}</div>
					                							</div> -->

				                							</div>
				                						</div>
			                							
			                						</div>
			                						<div class="item-btn">
			                							<button style="background-color:#6ed5d4;" class="btn btn-removeItem" data-cart-id="{{ $crt->cart_id }}">
			                								<!--<i class="fa fa-trash fa-fw"></i>--> Remove
			                							</button>
			                						</div>
			                					</div>
			                				</td>
			                				@if( ! config('gp_conf.isBuyNow') )
				                				<td>&dollar;{{ number_format($crt->product_price, 2) }}</td>
				                				<td><input class="form-control quantity" data-product-id="{{ $crt->product_id }}" placeholder="Quantity" maxlength="3" name="quantity" type="number" value="{{ $crt->qty }}"></td>
				                			@endif
			                				<td style="font-size:18pt;">&dollar;<span class="product-total-price"><strong>{{ number_format($subTotal, 2) }}</strong></span></td>
			                			</tr>
		                			@endforeach				
	                			</tbody>
	                		</table>
							
							<div class="footer clearfix">
								<div class="border-top">
									<!--
									<div class="col-lg-5 col-md-3"></div>
									<div class="col-lg-7 col-md-9 checkout-section">
										<div class="checkout-total">
											<div class="block clearfix">
												<div class="checkout-total-label inline-block">
													Item (<span class="total-cart">{{ count($cart) }}</span>) Subtotal
												</div>
												<div class="checkout-total-content inline-block text-right">
													&dollar;<span class="cart-total">{{ number_format($theTotal, 2) }}</span>
												</div>
											</div>
											<div class="block clearfix">
												<div class="checkout-shipping-label inline-block">
													Shipping
												</div>
												<div class="checkout-shipping-content inline-block text-right">
													&dollar;<span class="cart-shipping">{{ ($shippingCost > 0 ? number_format($shippingCost, 2) : '0.00') }}</span>
												</div>
											</div>
											<div class="overall-total block">
												<div class="checkout-total-label inline-block">
													TOTAL
												</div>
												<div class="checkout-total-content inline-block text-right">
													&dollar;<span class="cart-total">{{ number_format(($theTotal + $shippingCost), 2) }}</span>
												</div>
											</div>
											@if( Auth::check() )
												<div class="block clearfix shipping-con">
													<div class="checkout-total-label inline-block">
														Shipping Details 
													</div>
													<div class="checkout-total-content inline-block">
														<div class="shipping-details">
															{!! Form::text('address_1', (isset($shipping->address_1) ? $shipping->address_1 : null), ['class' => 'form-control address-1', 'placeholder' => 'Address 1']) !!}
														</div>
														<div class="shipping-details">
															{!! Form::text('address_2',(isset($shipping->address_2) ? $shipping->address_2 : null), ['class' => 'form-control address-2', 'placeholder' => 'Address 2']) !!}
														</div>
														<div class="shipping-details">
															{!! Form::text('city', (isset($shipping->city) ? $shipping->city : null), ['class' => 'form-control city inline-block', 'placeholder' => 'City'] ) !!}
															<div class="state inline-block">
																{!! Form::select('state', \App\Helper::getStates(), (isset($shipping->state) ? $shipping->state : null), ['id' => 'state', 'class' => 'form-control chosen', 'placeholder' => 'State']) !!}
															</div>	
															{!! Form::text('post_code', (isset($shipping->post_code) ? $shipping->post_code : null), ['class' => 'form-control zip inline-block', 'placeholder' => 'Post code']) !!}
														</div>
													</div>
												</div>
												<div class="block clearfix shipping-con">
													<div class="checkout-total-label inline-block">
														Notes <small>(optional)</small>
													</div>
													<div class="checkout-total-content inline-block">
														<div class="notes-details">
															{!! Form::textarea('notes', null, ['class' => 'form-control address-1', 'placeholder' => 'Please leave a notes here...']) !!}
														</div>
													</div>
												</div>
											@endif
											<div class="block clearfix shipping-con">
												<div class="checkout-total-label inline-block">&nbsp;</div>
												<div class="checkout-total-content">
													
													@if( Auth::check() )
														<div class="btn-checkout-submit">
															<button type="button" class="btn btn-paypal pull-right btn-checkout" data-loading-text="Processing... Please wait.">Check out with <img class="btn-img" src="{{ url('/images/paypal-logo.png') }}"></button>
															<!-- <span class="" style="color: #c0c0c0; font-size: 15px;">&nbsp; or &nbsp;</span>
															<button type="button" class="btn btn-checkout">Proceed to Checkout</button> -->
														<!--
														</div>
													@else
														<div class="alert-message">
															<div class="alert alert-info text-center">
																Please login to proceed
															</div>
														</div>
													@endif
												</div>
											</div>
										</div>
									</div>
								</div>
								-->
							</div>
                		@else
							<!--
	                		<div class="alert message-alert text-center">
	                			<div class="icon-empty">
	                				<i class="fa fa-shopping-cart" aria-hidden="true"></i>
	                			</div>
	                			<p>{{ trans('messages.cart.empty') }}</p>
	                			<div class="button-link">
	                				<a href="{{ url('/') }}">
		                				<div class="btn-link-view">
		                					View Listings
		                				</div>
	                				</a>
	                			</div>
	                		</div>
							-->
	                	@endif
						<table class="table-checkout" style="margin-top:30px !important;">
						<h1 style="font-size:24pt; color:#161e18; padding:50px !important; font-family: Lato, Helvetica, sans-serif; font-weight:normal;">Interested in this property? Contact an Agent today</h1>
								<tbody>
		                		<tr class="">
			                		<td>
									<div style="width:75%; margin:0 auto;">
									<div class="inline-block" style="padding-bottom:50px; float:left; margin-right: 120px;">
									<div class="inline-block">
									<img src="{{ url('images/cart-image-contact.png') }}" class="img-responsive" alt="Forsublease logo" title="Forsublease logo" />
									</div>
									<div class="inline-block">
			                			<div class="item-body inline-block">
			                				<div class="item-body">
				                			<div class="item">
				                				<div class="item-content" style="font-size:18pt; margin-top:20px; margin-left:10px;">
				                				<div class="label-item inline-block block" style="color:#1c241c; padding:1px; font-family: 'Lato', sans-serif;"><strong>Lewis Harper</strong></div><br>
				                				<div class="label-item inline-block block" style="color:#1c241c; padding:1px;"><strong><i class="fa fa-phone fa-fw"></i> 043 111 222</strong></div><br>
				                				<div class="label-item inline-block block" style="color:#1c241c; padding:1px;"><strong><i class="fa fa-envelope fa-fw"></i> harper@live.com</strong></div><br>
												<button style="background-color:#6ed5d4; color:#ffffff; padding:10px 40px; margin-top:10px;" class="btn btn-default">
			                					<a href="mailto:harper@live.com" style="color:#ffffff;"><!--<i class="fa fa-trash fa-fw"></i>--> Email</a>
			                					</button>
												</div>
				                			</div>
				                			</div>				
			                			</div>
									</div>
									</div>
									
									<div class="inline-block" style="padding-bottom:50px; float:left;">
									<div class="inline-block">
									<img src="{{ url('images/cart-image-contact.png') }}" class="img-responsive" alt="Forsublease logo" title="Forsublease logo" />
									</div>
									<div class="inline-block">
			                			<div class="item-body inline-block">
			                				<div class="item-body">
				                			<div class="item">
				                				<div class="item-content" style="font-size:18pt; margin-top:20px; margin-left:10px;">
				                				<div class="label-item inline-block block" style="color:#1c241c; padding:1px; font-family: 'Lato', sans-serif;"><strong>John Snow</strong></div><br>
				                				<div class="label-item inline-block block" style="color:#1c241c; padding:1px;"><strong><i class="fa fa-phone fa-fw"></i> 043 090 234</strong></div><br>
				                				<div class="label-item inline-block block" style="color:#1c241c; padding:1px;"><strong><i class="fa fa-envelope fa-fw"></i> snowjohn@live.com</strong></div><br>
												<button style="background-color:#6ed5d4; color:#ffffff; padding:10px 40px; margin-top:10px;" class="btn btn-default">
			                					<a href="mailto:snowjohn@live.com" style="color:#ffffff;"><!--<i class="fa fa-trash fa-fw"></i>--> Email</a>
			                					</button>
												</div>
				                			</div>
				                			</div>				
			                			</div>
									</div>
									</div>
									
									</div>
									</td>
			                	</tr>		
	                			</tbody>		
	                	</table>						
                	</div>
                </div>
            </div>
        </div>
    </div>
    <script>
    	$(document).ready(function(){
    		var timeout;
    		$('.quantity').on('keyup, change', function(){
    			var self = $(this);

    			if( parseInt(self.val()) <= 0 )
    				self.val(1);

    			var gear = {
    				"id" : self.data('product-id'),
    				"qty" : self.val()
    			};
    			
    			if(timeout) {
			        clearTimeout(timeout);
			        timeout = null;
			    }

			    timeout = setTimeout(function(){
			    	self.prop('disabled', true);
	    			
	    			$.ajax({
	    				"url": "{{ route('cart::update-cart-qty') }}",
	    				"type": 'post',
	    				"data": gear
	    			}).done(function(response){

	    				showToastr(response.status, response.message);

	    				self.prop('disabled', false);
	    				self.parents('tr').find('.product-total-price').text(response.data.new_price);
	    				$('.cart-total').text(response.data.total_price);
	    			});
	    		}, 1000);
    		});

    		/**
    		 * Action to remove item from cart
    		 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
    		 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T14:57:22+0800]
    		 * @param  {[type]} ){                 			var self [description]
    		 * @return {[type]}     [description]
    		 */
    		$('.btn-removeItem').on('click', function(){
    			var self = $(this);
    			var btn = self.button('loading');

    			$.ajax({
    				"url": "{{ route('cart::remove-product') }}",
    				"type": 'post',
    				"data": {
    					"id" : self.data('cart-id')
    				}
    			}).done(function(response){
    				btn.button('reset');
    				toastr.clear();
    				toastr[response.status](response.message);

    				if( response.status == 'success' ){
    					$('.cart-total').text(response.data.total_price);
    					$('.total-cart').text(response.data.total);
    					self.parents('tr.item').fadeOut(300, function() {
		    				$(this).remove();

		    				if( $('.table-checkout').find('tbody').find('.item').length == 0 )
		    					window.location.reload();
		    			});
    				}
    			});
    		});

    		/**
    		 * Checkout with PayPal
    		 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
    		 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T15:56:18+0800]
    		 * @param  {[type]} ){                 			var self [description]
    		 * @return {[type]}     [description]
    		 */
    		$('.btn-checkout').on('click', function(){
    			var self = $(this);
    			var btn = self.button('loading');

    			$('.shipping-con').find('.form-control, .chzn-single').removeClass('has-error');

    			$.ajax({
    				"url": "{{ route('cart::checkout') }}",
    				"type": 'post',
    				"data": {
    					"address_1": $('input[name="address_1"]').val(),
    					"address_2": $('input[name="address_2"]').val(),
    					"city": $('input[name="city"]').val(),
    					"state": $('select[name="state"]').val(),
    					"post_code": $('input[name="post_code"]').val()
    				}
    			}).done(function(response){
    				toastr.clear();
    				if( response.status == 'success' ) {
    					window.location.href = response.data.redirectUrl;
    				}
    				else {
    					// toastr[response.status](response.message);
    					showToastr(response.status, response.message);
    					btn.button('reset');
    					var errorData = response.data.target;
    					var arrayLength = errorData.length;
    					for (var i = 0; i < arrayLength; i++) {
						    var errorTarget = $('input[name="'+ errorData[i] +'"]');
						    errorTarget.addClass('has-error');

						    if( errorData[i] == 'state' ){
						    	$('a.chzn-single').addClass('has-error');
						    }
						}    					
    				}
    			});
    		});
    	});
    </script>
@endsection