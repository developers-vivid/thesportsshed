@extends('layouts.master')

@section('title', "Checkout Summary")

@section('content')
   
    <div id="main-container" class="single-page checkout-summary">
        <div class="container">
            <div class="row">
            	<div class="heading-content col-lg-12 clearfix"><p style="color: #{{ strtolower($orderDetails->order_status) != 'completed' ? 'd9534f' : '5cb85c' }}">{{ $orderMessage }}</p></div>
            	<div class="col-lg-6 col-md-6 col-xs-12">
                    <!-- Order Information -->
            		<table id="order-info" class="section-information-container">
            			<thead>
            				<tr class="section-heading">
                                <th colspan="2"><h4>Order Information</h4></th>
                			</tr>
            			</thead>
            			<tbody>
            				<tr>
            					<td class="section-label" width="45%"><span class="lbl-name">Order ID</span> <span class="colon-separator">:</span></td>
            					<td class="section-content" width="55%">{{ $orderDetails->order_id }}</td>
            				</tr>
            				<tr>
            					<td class="section-label" width="45%"><span class="lbl-name">Invoice ID</span> <span class="colon-separator">:</span></td>
            					<td class="section-content" width="55%">{{ $orderDetails->invoice_id }}</td>
            				</tr>
            				<tr>
            					<td class="section-label" width="45%"><span class="lbl-name">Date {{ $orderDetails->order_status }}</span> <span class="colon-separator">:</span></td>
            					<td class="section-content" width="55%">{{ \Carbon\Carbon::parse(strtolower($orderDetails->order_status) != 'completed' ? $orderDetails->date_cancelled : $orderDetails->date_completed )->format('F j, Y @ h:i A') }}</td>
            				</tr>
            				<tr>
            					<td class="section-label" width="45%"><span class="lbl-name">Status</span> <span class="colon-separator">:</span></td>
            					<td class="section-content" width="55%" style="color: #{{ strtolower($orderDetails->order_status) != 'completed' ? 'd9534f' : '5cb85c' }}">{{ $orderDetails->order_status }}</td>
            				</tr>
                            @if( strtolower($orderDetails->order_status) == 'completed' )
                                <tr>
                                    <td class="section-label"><span class="lbl-name">Paypal Email</span> <span class="colon-separator">:</span></td>
                                    <td class="section-content">{{ $orderDetails->paypal_payer_email ?: 'n/a' }}</td>
                                </tr>
                            @endif
            				<tr class="tbl-row-total">
            					<td class="section-label" width="45%"><span class="lbl-name total">Total</span> <span class="colon-separator">:</span></td>
            					<td class="section-content" width="55%"><small>AUD</small> <strong>{{ number_format($orderDetails->total, 2) }}</strong></td>
            				</tr>
            			</tbody>
            		</table>
                    {{-- 
                    <!-- Paypal Payment Information -->
                    <table id="paypay-payment-info"class="section-information-container">
                        <thead>
                            <tr class="section-heading">
                                <th colspan="2"><h4>Paypal Payment Information</h4></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="section-label" width="40%"><span class="lbl-name">PayKey</span> <span class="colon-separator">:</span></td>
                                <td class="section-content" width="60%">{{ $orderDetails->paypal_pay_key }}</td>
                            </tr>
                            <tr>
                                <td class="section-label" width="40%"><span class="lbl-name">Payer Email Address</span> <span class="colon-separator">:</span></td>
                                <td class="section-content" width="60%">{{ $orderDetails->paypal_payer_email ?: 'n/a' }}</td>
                            </tr>
                            <tr>
                                <td class="section-label" width="40%"><span class="lbl-name">Payer Account ID</span> <span class="colon-separator">:</span></td>
                                <td class="section-content" width="60%">{{ $orderDetails->paypal_payer_id ?: 'n/a' }}</td>
                            </tr>
                        </tbody>
                    </table> 
                    --}} 
                    <!-- Item -->
                    <table id="item" class="section-information-container">
                        <thead>
                            <tr class="section-heading">
                                <th colspan="4"><h4>Item <small>in AUD</small></h4></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="section-sub-heading">
                                <td><h4>Product Name</h4></td>
                                <td><h4>Price</h4></td>
                                <td><h4>Shipping</h4></td>
                                <td><h4>Sub Total</h4></td>
                            </tr>
                            <?php $ot = 0; ?>
                            @if( count($orderDetails->ordered_products) > 0 )
                                @foreach($orderDetails->ordered_products as $prod)
                                    <?php 
                                        $ot+=$total = (($prod->price + $prod->shipping_cost) * $prod->qty);
                                    ?>
                                    <tr>
                                        <td class="section-label">{{ $prod->product_title }}</td>
                                        <td class="section-content" valign="top" style="vertical-align: top; text-align: right;">
                                            <small class="muted"></small> {{ number_format($prod->price, 2) }}
                                        </td>
                                        <td class="section-content" valign="top" style="vertical-align: top; text-align: right;">
                                            <small class="muted"></small> {{ number_format($prod->shipping_cost, 2) }}
                                        </td>
                                        <td class="section-content" valign="top" style="vertical-align: top; text-align: right;">
                                            <small class="muted"></small> {{ number_format($total, 2) }}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4" class="text-center">You did not placed an order</td>
                                </tr>
                            @endif
                            <tr class="tbl-row-total">
                                <td class="section-label" colspan="3"><span class="lbl-name total">Total:</span></td>
                                <td class="section-content"><small class="text-muted"></small> {{ number_format($ot, 2) }}</td>
                            </tr>
                        </tbody>
                    </table>  
            	</div>
            	<div class="col-lg-6 col-md-6 col-xs-12">
                    <!-- Customer and Shipping Information -->
                    <table id="customer-shipping-info" class="section-information-container">
                        <thead>
                            <tr class="section-heading">
                                <th colspan="2"><h4>Customer and Shipping Information</h4></th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Customer -->
                            <tr class="tbl-row">
                                <td class="section-title" colspan="2"><h5>Customer</h5></td>
                            </tr>
                            <tr class="tbl-row">
                                <td class="section-label" width="25%"><span class="lbl-name">Name</span> <span class="colon-separator">:</span></td>
                                <td class="section-content" width="75%">{{ $orderDetails->customer_name }}</td>
                            </tr>
                            <tr class="tbl-row">
                                <td class="section-label" width="25%"><span class="lbl-name">Email</span> <span class="colon-separator">:</span></td>
                                <td class="section-content" width="75%">{{ $orderDetails->customer_email }}</td>
                            </tr>
                            <tr class="tbl-row">
                                <td class="section-label" width="25%"><span class="lbl-name">Telephone</span> <span class="colon-separator">:</span></td>
                                <td class="section-content" width="75%">{{ $orderDetails->customer_telephone ?: 'n/a' }}</td>
                            </tr>
                            <tr class="tbl-row">
                                <td class="section-label" width="25%"><span class="lbl-name">Mobile</span> <span class="colon-separator">:</span></td>
                                <td class="section-content" width="75%">{{ $orderDetails->customer_mobile ?: 'n/a' }}</td>
                            </tr>

                            <!-- Seller -->
                            <tr class="tbl-row">
                                <td class="section-title" colspan="2"><h5>Seller</h5></td>
                            </tr>
                            <tr class="tbl-row">
                                <td class="section-label" width="25%"><span class="lbl-name">Name</span> <span class="colon-separator">:</span></td>
                                <td class="section-content" width="75%">{{ $orderDetails->seller->seller_name }}</td>
                            </tr>
                            <tr class="tbl-row">
                                <td class="section-label" width="25%"><span class="lbl-name">Email</span> <span class="colon-separator">:</span></td>
                                <td class="section-content" width="75%">{{ $orderDetails->seller->seller_email }}</td>
                            </tr>
                            <tr class="tbl-row">
                                <td class="section-label" width="25%"><span class="lbl-name">Telephone</span> <span class="colon-separator">:</span></td>
                                <td class="section-content" width="75%">{{ $orderDetails->seller->seller_telephone ?: 'n/a' }}</td>
                            </tr>
                            <tr class="tbl-row">
                                <td class="section-label" width="25%"><span class="lbl-name">Mobile</span> <span class="colon-separator">:</span></td>
                                <td class="section-content" width="75%">{{ $orderDetails->seller->seller_mobile ?: 'n/a' }}</td>
                            </tr>

                            <!-- Shipping -->
                            <tr class="tbl-row">
                                <td class="section-title" colspan="2"><h5>Shipping</h5></td>
                            </tr>
                            <tr class="tbl-row">
                                <td class="section-label" width="25%"><span class="lbl-name">Address 1</span> <span class="colon-separator">:</span></td>
                                <td class="section-content" width="75%">{{ $orderDetails->shipping_address_1 }}</td>
                            </tr>
                            <tr class="tbl-row">
                                <td class="section-label" width="25%"><span class="lbl-name">Address 2</span> <span class="colon-separator">:</span></td>
                                <td class="section-content" width="75%">{{ $orderDetails->shipping_address_2 }}</td>
                            </tr>
                            <tr class="tbl-row">
                                <td class="section-label" width="25%"><span class="lbl-name">City</span> <span class="colon-separator">:</span></td>
                                <td class="section-content" width="75%">{{ $orderDetails->shipping_city }}</td>
                            </tr>
                            <tr class="tbl-row">
                                <td class="section-label" width="25%"><span class="lbl-name">State</span> <span class="colon-separator">:</span></td>
                                <td class="section-content" width="75%">{{ $orderDetails->shipping_state }}</td>
                            </tr>
                            <tr class="tbl-row">
                                <td class="section-label" width="25%"><span class="lbl-name">Post Code</span> <span class="colon-separator">:</span></td>
                                <td class="section-content" width="75%">{{ $orderDetails->shipping_post_code }}</td>
                            </tr>
                        </tbody>
                    </table>  
                    <div class="bottom-content">
                        <p>Notes: {{ $orderDetails->notes ?: '-' }}</p>
                        <a href="{{ url('/') }}" class="btn btn-danger btn-cont-schop" id="btn-continue-shopping" title="Continue Shipping">CONTINUE SHOPPING</a>
                        {{-- <button type="button" class="btn btn-danger btn-cont-schop" id="btn-continue-shopping">CONTINUE SHOPPING</button> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection