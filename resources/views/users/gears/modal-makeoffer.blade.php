<form class="make-an-offer form-horizontal">
  <span class="make-an-offer-heading">Make an Offer</span>
  <div class="fields-container">
    <div class="row form-group">
      <span class="col-lg-12"><input type="text" class="form-control" id="inputAmount" placeholder="e.g $500.00"></span>
    </div>
  <div class="submit-container">
    <button type="button" class="col-lg-12 btn btn-default btn-primary btn-submit">Submit</button>
  </div>
</form>