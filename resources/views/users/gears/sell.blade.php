@extends('layouts.master')

@section('content')
<div class="single-page sell">

    <div class="col-lg-12 col-md-12 col-xs-12 banner-item"  style="margin-top:-15px;">
        <div class="heading-container">
            <h2>YOUR OFFICE JUST A CLICK AWAY</h2>
        </div>
        <div class="img-container" style="background: url({{ url('images/homepage_banner1.jpg') }}) 0 0 no-repeat; background-size: cover; background-position: center;"></div>
    </div>

    <div class="container">
        <div class="row">
            @if( Auth::check() && Auth::user()->seller == 'no' )
                <div class="alert alert-danger text-center">
                    <strong>Error!</strong> Your account is not allowed to sell
                </div>
            @else
                <div class="col-lg-12 col-md-12 col-xs-12">
                    
                    <div class="panel panel-default">
                        <div class="row panel-heading">
                            <div class="col-lg-11 col-md-10 col-xs-9 heading-title">
                                <h2>List your office space for lease</h2>
                            </div>
                        </div>
                        {!! Form::open(['route' => 'gears::create', 'id' => 'create-gear-form', 'class' => 'form-horizontal', 'role' => 'form', 'file' => true]) !!}
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-7 col-md-7 col-xs-12 colm1">
                                        <div class="list">
                                            <ul>
                                                <li><p>Enter your item details</p></li>
                                                <li><p>Let buyers know how much postage costs, and your returns/warranty policy</p></li>
                                                <li><p>Add some photos and LIST</p></li>
                                            </ul>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <!-- Category -->
                                                <div class="row form-group">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">Category</label>
                                                    <div class="col-lg-12 col-md-12 col-xs-12 select-chosen" style="background-color: #ffffff !important;">
                                                        <select name="category" id="category" class="form-control chosen">
                                                            <option value="">Find Category</option>
                                                            @if( count($categories) > 0 )
                                                              @foreach($categories as $category)
                                                                <option value="{{ $category->category_id }}">{{ $category->name }}</option>
                                                              @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <!-- Sub-Category -->
                                                <div class="row form-group">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">Sub Category</label>
                                                    <div class="col-lg-12 col-md-12 col-xs-12 select-chosen">
                                                        <select name="sub_category" id="sub-category" class="form-control chosen">
                                                            <option value="">Find Sub-Category</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                <!-- Type -->
                                                <div class="row form-group">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">Type</label>
                                                    <div class="col-lg-12 col-md-12 col-xs-12 select-chosen">
                                                        {{ Form::select('type', ['new' => 'New', 'pre-owned' => 'Pre Owned'], null, ['id' => 'type', 'class' => 'form-control chosen', 'placeholder' => 'Type']) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                <!-- Condition -->
                                                <div class="row form-group">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">Condition</label>
                                                    <div class="col-lg-12 col-md-12 col-xs-12 select-chosen">
                                                        <select name="condition" id="condition" class="form-control chosen">
                                                            <option value="">Condition</option>
                                                                @if( count($conditions = \App\ProductsCondition::all()) > 0 )
                                                                  @foreach($conditions as $condition)
                                                                    <option value="{{ $condition->condition_id }}">{{ $condition->name }}</option>
                                                                  @endforeach
                                                                @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>   

                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-xs-12">
                                                <!-- Brand -->
                                                <div class="row form-group">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">Brand</label>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        {!! Form::text('brand', null, ['class' => 'form-control' ]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <!-- Model -->
                                                <div class="row form-group">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">Model</label>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        {!! Form::text('model', null, ['class' => 'form-control' ]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-xs-12">
                                                <!-- Year -->
                                                <div class="row form-group">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">Year</label>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        {!! Form::text('year', null, ['class' => 'form-control' ]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-xs-12">
                                                <!-- Colour -->
                                                <div class="row form-group">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">Colour</label>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        {!! Form::text('colour', null, ['class' => 'form-control' ]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <!-- Where was it made? -->
                                                <div class="row form-group">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">Where was it made?</label>
                                                    <div class="col-lg-12 col-md-12 col-xs-12 select-chosen">
                                                        {!! Form::select('made_in', \App\Helper::getCountries( true ), null, ['id' => 'country', 'class' => 'form-control chosen']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                <!-- Listing title -->
                                                <div class="row form-group">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">Listing Title</label>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        {!! Form::text('listing_title', old('listing_title'), ['class' => 'form-control' ]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <!-- Price -->
                                                <div class="row form-group">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">Price</label>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        {!! Form::text('price', old('price'), ['class' => 'form-control' ]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-xs-12">
                                                <!-- Sale Price -->
                                                <div class="row form-group">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">Sale Price</label>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        {!! Form::text('sale_price', old('sale_price'), ['class' => 'form-control' ]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>   

                                        {{--
                                        <div class="row">
                                            <div class="col-lg-5 col-lg-offset-6 col-md-6 col-xs-12 colmn">
                                                <!-- Accept offers -->
                                                <div class="form-group">
                                                    <div class="col-md-8">
                                                        {!! Form::checkbox('accept_offers', null, false, ['class' => 'accept_offers']) !!}<label class="control-label">Accept offers</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        --}}

                                        <!-- Description -->
                                        <div class="row form-group">
                                            <label class="col-lg-12 col-md-12 col-xs-12 control-label">Description</label>
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                {!! Form::textarea('description', old('description'), ['class' => 'form-control' ]) !!}
                                            </div>
                                        </div>  

                                        <!-- Shopping Policy -->
                                        <div class="row form-group">
                                            <label class="col-lg-12 col-md-12 col-xs-12 control-label">Returns Policy, Other Information</label>
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                {!! Form::textarea('shopping_policy', old('shopping_policy'), ['class' => 'form-control']) !!}
                                            </div>
                                        </div>

                                        <!-- Billing Details -->
                                        <div class="row form-group">
                                            <label class="col-lg-12 col-md-12 col-xs-12 control-label">Billing Details</label>
                                        </div> 
                                        <div class="row form-group">
                                            <label class="col-lg-12 col-md-12 col-xs-12 control-label">PayPal Email <small class="text-muted pull-right">This is where you will received the payment</small></label>
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                {!! Form::email('paypal_email', null, ['class' => 'form-control', 'placeholder' => 'paypal@email.com']) !!}
                                            </div>
                                        </div> 

                                        <!-- Where are you shipping from? -->
                                        <div class="row form-group">
                                            <label class="col-lg-12 col-md-12 col-xs-12 control-label">Where are you shipping from?</label>
                                        </div> 

                                        <div class="row form-group">
                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                <!-- City -->
                                                <div class="row form-group">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">City</label>
                                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                                        {!! Form::text('city', old('city'), ['class' => 'form-control' ]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-xs-12">
                                                <!-- State -->
                                                <div class="row form-group">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">State</label>
                                                    <div class="col-lg-12 col-md-12 col-xs-12 select-chosen">
                                                        {!! Form::select('state', \App\Helper::getStates( true ), null, ['id' => 'state', 'class' => 'form-control chosen']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 

                                        <!-- Shipping Policy -->
                                        <div class="row form-group">
                                            <label class="col-lg-12 col-md-12 col-xs-12 control-label">Shipping Policy</label>
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                {!! Form::textarea('shipping_policy', old('shipping_policy'), ['class' => 'form-control' ]) !!}
                                            </div>
                                        </div> 
                                        <div class="row form-group">
                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                                    
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Shipping Cost</label>
                                                            {!! Form::number('shipping_costs', old('shipping_costs'), ['class' => 'form-control', 'placeholder' => 'AUD' ]) !!}
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                                <label class="col-lg-12 col-md-12 col-xs-12 control-label">Shipping Methods</label>
                                                
                                                <div class="col-md-6">
                                                    {!! Form::checkbox('shipping', null, false, ['class' => 'shipping']) !!}<label class="control-label" >Shipping</label>
                                                </div>
                                                
                                                <div class="col-md-6">
                                                    {!! Form::checkbox('local_pickup', null, false, ['class' => 'local_pickup']) !!}<label class="control-label">Local Pickup</label>
                                                </div>
                                            </div>
                                           
                                        </div>

                                        <!-- Shipping to -->
                                        <!-- <div class="row form-group">
                                            <label class="col-lg-12 col-md-12 col-xs-12 control-label">Shipping to</label>
                                        </div> --> 

                                        <!-- Shipping Heading -->
                                        <!-- <div class="row form-group shipping-heading">
                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                                <label class="col-lg-12 col-md-12 col-xs-12 control-label">Location</label>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                                <label class="col-lg-12 col-md-12 col-xs-12 control-label">Shipping Cost*</label>
                                            </div>
                                        </div> --> 

                                        <!-- Shipping Field -->
                                        <!-- <div class="row form-group multi-field-wrapper">
                                            {!! Form::hidden('shipping_cost', null, ['class' => 'form-control', 'id' => 'shipping-cost']) !!}
                                            <div class="col-lg-12 col-md-12 col-xs-12 multi-fields">
                                                <div class="col-lg-12 col-md-12 col-xs-12 shipping-to multi-field">
                                                   <label class="col-lg-6 col-md-6 col-xs-6 control-label">Metro Areas</label>
                                                    <div class="col-lg-6 col-md-6 col-xs-6 currency-field">
                                                        <i class="fa fa-usd" aria-hidden="true"></i>
                                                        {!! Form::number('metro_areas_cost', Input::get('metro_areas_cost'), ['class' => 'form-control shipping-costs', 'placeholder' => 'AUD' ]) !!}
                                                    </div>
                                                    <div class="col-lg-2 optionsbtn"></div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-xs-12 shipping-to multi-field no-border">
                                                   <label class="col-lg-6 col-md-6 col-xs-6 control-label">Regional Areas</label>
                                                    <div class="col-lg-6 col-md-6 col-xs-6 currency-field">
                                                        <i class="fa fa-usd" aria-hidden="true"></i>
                                                        {!! Form::number('regional_areas_cost', Input::get('regional_areas_cost'), ['class' => 'form-control shipping-costs', 'placeholder' => 'AUD' ]) !!}
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-xs-12 multi-field-option hidden">
                                                   <div class="col-lg-6 col-md-6 col-xs-6 select-chosen">
                                                        {!! Form::select('location', \App\Helper::getStates( true ), null, ['id' => 'location', 'class' => 'form-control chosen']) !!}
                                                    </div>
                                                    <div class="col-lg-5 col-md-5 col-xs-5 currency-field">
                                                        <i class="fa fa-usd" aria-hidden="true"></i>
                                                        {!! Form::number('location_cost', Input::get('location_cost'), ['class' => 'form-control input-field', 'placeholder' => 'AUD' ]) !!}
                                                    </div>
                                                    <button type="button" class="remove-field">SAVE</button>
                                                    <div class='col-lg-1 col-md-1 col-xs-1 optionsbtn'><button type="button" class="save-field fa fa-floppy-o"></button></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xs-12 add-container"><div class="col-lg-12 col-md-12 col-xs-12"><button type="button" class="add-field fa fa-plus-circle"></button> Add shipping Locations</div></div>
                                        </div> --> 
                                    </div>
                                    <!--<div class="col-lg-5 col-md-5 col-xs-12"> 
                                        <div class="row form-group">
                                            <div class="col-lg-12 col-md-12 col-xs-12 sell-thumb-image">
                                                <img src="{{ url('images/sell-thumb-image.jpg') }}" class="img-responsive" alt="" title="" />
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <ul class="sell-content-list">
                                                    <li><i class="fa fa-check-circle" aria-hidden="true"></i>Reach an <span>Australia-wide</span> audience of buyers</li>
                                                    <li><i class="fa fa-check-circle" aria-hidden="true"></i>Only a <span>3.5% final value</span> fee if your item sells (payment processing fees may apply depending on payment type)</li>
                                                    <li><i class="fa fa-check-circle" aria-hidden="true"></i>Great photos help sell items! check out our <span>Guide to Great Gear Photos</span></li>
                                                    <li><i class="fa fa-check-circle" aria-hidden="true"></i>Unsure how to pack your item for shipping? We’ve created a <span>Guide to Packing terms</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                            <div class="sell-body upload-sign-up">
                                <div class="row section-form upload-container">
                                    <div class="col-lg-6 col-md-6 col-xs-12 clearfix">
                                        <div class="form-group">
                                            <label class="col-lg-12 col-md-12 col-xs-12 control-label multiple-uploads">Upload Photos <span class="doc-limits">(10 docs max)</span><span class="capacity-limits">(png, jpg max of 2mb)</span></label>
                                            <div class="col-lg-12 col-md-12 col-xs-12">
                                                <div id="upload-gear-photos" class="dropzone" style="border-radius: 10px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-xs-12 clearfix">
                                         <div id="imagePreviews" class="col-lg-12 col-md-12 col-xs-12 dropzone" ></div>
                                    </div>
                                </div>
                                @if( !Auth::check() )
                                    <div class="row product-info-heading">
                                        <div class="col-lg-12 col-md-12 col-xs-12 section-title">Make an account. <small class="text-muted">Already have an account? Just fill in your email and password below.</small></div>
                                    </div>
                                    <div class="row section-form">
                                        <div class="col-lg-6 col-md-6 col-xs-12 colmn">
                                            <!-- First Name -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">First name</label>
                                                <div class="col-md-8">
                                                    {!! Form::text('first_name', old('first_name'), ['class' => 'form-control']) !!}
                                                </div>
                                            </div>

                                            <!-- Email -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Email</label>
                                                <div class="col-md-8">
                                                    {!! Form::email('email', old('email'), ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-xs-12 colmn">
                                            <!-- Last Name -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Last name</label>
                                                <div class="col-md-8">
                                                    {!! Form::text('last_name', old('last_name'), ['class' => 'form-control']) !!}
                                                </div>
                                            </div>

                                            <!-- Last Name -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Password</label>
                                                <div class="col-md-8">
                                                   {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Must be at least 8 characters']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <!-- Agreement -->
                                <div class="form-group">
                                    <div class="col-lg-12 col-md-12 col-xs-12 agreement">
                                        <input style="background: #6ed5d4 !important;" type="checkbox" name="agree" value="{{ old('agree') }}"><label class="control-label">I agree to the terms of use and privacy policy</label>
                                    </div>
                                </div>

                                 <!-- Sell -->
                                <div class="form-group">
                                    <div class="col-lg-12 col-md-12 col-xs-12">
                                        <button type="button" class="btn btn-success btn-sell" disabled="disabled" id="btn-submit-gear" data-loading-text="Processing... Please wait.">Submit</button>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}    
                    </div>                    
                </div>
            @endif
        </div>
    </div>
</div>
@endsection
