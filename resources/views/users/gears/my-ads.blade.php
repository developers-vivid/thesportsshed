@extends('layouts.master')

@section('content')
<?php $categories = \App\Helper::getCategories(); ?>
    <div id="main-container" class="single-page my-gears">
        <div class="container">
            <div class="row main-row">
                <div class="col-lg-3 col-sm-3 col-xs-12 sidebar-container">
                     @include('layouts.includes.gp_categories')
                     @include('layouts.filters.refined-filter', ['includeType' => true, 'includeSort' => true])
                </div>
                <div class="col-lg-9 col-sm-9 col-xs-12 items-container">
                    <div class="row options-container">
                        <div class="col-md-12 new-pre-owned-container text-center">
                            <ul class="refined-filter-status-ul">
                                <li {!! \Input::get('status') == '' || strtolower(\Input::get('status')) == 'all' ? "class='selected'" : "" !!}>
                                    <a href="javascript:;" class="refined-filter-status" data-value="all">All</a>
                                </li>
                                <li {!! \Input::has('status') && strtolower(\Input::get('status')) == 'active' ? "class='selected'" : "" !!}>
                                    <a href="javascript:;" class="refined-filter-status" data-value="active">Active</a>
                                </li>
                                <li {!! \Input::has('status') && strtolower(\Input::get('status')) == 'cancelled' ? "class='selected'" : "" !!}>
                                    <a href="javascript:;" class="refined-filter-status" data-value="cancelled">Cancelled</a>
                                </li>

                                <li {!! \Input::has('status') && strtolower(\Input::get('status')) == 'sold' ? "class='selected'" : "" !!}>
                                    <a href="javascript:;" class="refined-filter-status" data-value="sold">Sold</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row product-gear">
                        <div class="col-lg-12 clearfix">
                            <h3 class="title-area">My Ads</h3>
                        </div>
                        @if( count($myAds) > 0 )
                            <div class="list">
                                @foreach($myAds as $gear)
                                    <div class="col-xs-12 product-item">
                                        @include('layouts.gears.list')
                                    </div>
                                @endforeach
                            </div>          
                            <div class="col-lg-12 pagination-container">
                                {!! $myAds->appends([
                                    'location'  => Input::get('location'),
                                    'brand'     => Input::get('brand'),
                                    'type'      => Input::get('type'),
                                    'sort'      => Input::get('sort'),
                                    'price_min' => Input::get('price_min'),
                                    'price_max' => Input::get('price_max'),
                                    'status'    => Input::get('status')
                                ])->links() !!}
                            </div>                  
                        @else
                            <div class="col-lg-12">
                                <div class="alert alert-danger text-center">
                                    <!--No {{-- (Input::has('status') && Input::get('status') != 'all' ? Input::get('status') : '' ) --}} gears found-->
                                    You have no active ads - click Sell above to start listing!
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div><!-- .container -->
    </div><!-- #main-container -->
@endsection
