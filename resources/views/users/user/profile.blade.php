@extends('layouts.master')

@section('title', Auth::user()->name)

@section('content')

	<!-- <div id="main-container" class="single-page profile">
	    <div class="container">
	      <div class="row">
	        <div class="col-lg-12 col-md-12 col-xs-12">
	        	{{ Auth::user()->name }}'s Profile
	        </div>
	      </div>
	    </div>
	</div> -->

 	<div class="single-page sign-up">
	    <div class="container">
	        <div class="row">
	            <div class="col-lg-12 col-md-12 col-xs-12">
	                <div class="panel panel-default">

	                  <div class="container reg-container">
	                    <div class="row panel-heading">
	                            <div class="col-lg-11 col-md-10 col-xs-9 heading-title"><h2>Account Settings</h2></div>
	                            <div class="col-lg-1 col-md-2 col-xs-3 logo-thumb"><img src="/images/GearPlanet_Logo.png" class="img-responsive" alt="Forusblease logo" title="Forusblease logo"></div>
	                    </div>
	                    <div class="clr"></div>
	                    <div class="panel-body">
	                        {!! Form::open(['url' => '', 'class' => 'form-horizontal', 'role' => 'form', 'file' => true]) !!}

	                            <div class="row">
	                                <div class="col-lg-11 col-md-12 col-xs-12 section-title">Personal Details</div>
	                            </div>
	                            <div class="row section-form">
	                                <div class="col-lg-12">
	                                    <div class="row">
	                                        <div class="col-lg-6">
	                                            <div class="form-group">
	                                                <label class="col-md-12 control-label">First name*</label>
	                                                <div class="col-md-12">
	                                                    {!! Form::text('first_name', $userDetails->first_name, ['class' => 'form-control', 'placeholder' => 'Your first name', 'autofocus' => true ]) !!}
	                                                </div>
	                                            </div>
	                                        </div>
	                                        <div class="col-lg-6">
	                                            <div class="form-group">
	                                                <label class="col-md-12 control-label">Last name*</label>
	                                                <div class="col-md-12">
	                                                    {!! Form::text('last_name', $userDetails->last_name, ['class' => 'form-control', 'placeholder' => 'Your last name']) !!}
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-lg-6 col-md-6 col-xs-12 colmn">
	                                    <!-- Email -->
	                                    <div class="form-group">
	                                        <label class="col-md-12 control-label">Email*</label>
	                                        <div class="col-md-12">
	                                            {!! Form::text('email', $userDetails->email, ['class' => 'form-control', 'placeholder' => 'Your email address']) !!}
	                                        </div>
	                                    </div>

	                                </div>
	                                <div class="col-lg-6 col-md-6 col-xs-6 colmn">
	                                	<!-- Telephone -->
	                                    <div class="form-group">
	                                        <label class="col-md-12 control-label">Telephone</label>
	                                        <div class="col-md-12">
	                                            {!! Form::text('telephone', $userDetails->telephone, ['class' => 'form-control']) !!}
	                                        </div>
	                                    </div>
	                                    <!-- Password -->
	                                    <!-- <div class="form-group">
	                                        <label class="col-md-12 control-label">Password*</label>
	                                        <div class="col-md-12">
	                                            {!! Form::password('password', ['class' => 'form-control']) !!}
	                                        </div>
	                                    </div> -->
	                                    
	                                </div>

	                                <!-- <div class="col-lg-6 col-md-6 col-xs-6 colmn"> -->

	                                <!-- Confirm password -->
	                                    <!-- <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
	                                        <label class="col-md-12 control-label">Confirm Password*</label>
	                                        <div class="col-md-12">
	                                            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
	                                        </div>
	                                    </div>
	                                </div> -->

	                                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 hidden">
	                                    <!-- Register seller -->
	                                    <div class="form-group">
	                                        <div class="col-md-6 col-lg-6">
	                                            {!! Form::checkbox('registered_seller', null, false, ['class' => 'registered_seller', 'checked' => 'checked']) !!}<label class="control-label regseller-label">Register as Seller</label>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>

	                            <div class="row company-info-heading{{ config('gp_conf.allseller') ? '' : ' hidden' }}">
	                                <div class="col-lg-11 col-md-12 col-xs-12 section-title">Company Details</div>
	                            </div>
	                            <div class="row section-form company-info-details{{ config('gp_conf.allseller') ? '' : ' hidden' }}">
	                                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
	                                    <!-- Company Name -->
	                                    <div class="form-group">
	                                        <label class="col-md-12 control-label">Username / Store Name <small class="text-muted">(optional)</small></label>
	                                        <div class="col-md-12">
	                                            {!! Form::text('company_name', $userDetails->company_name, ['class' => 'form-control']) !!}
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="col-lg-12 col-md-12 col-xs-12 colmn comp-address">
	                                    

	                                    <!-- Company State -->
	                                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 compaddr-input">
	                                        <div class="form-group">
	                                            <label class="control-label">State*</label>
	                                            <div class="select-chosen">
	                                                {!! Form::select('company_state', \App\Helper::getStates(), $userDetails->company_state, ['class' => 'form-control chosen'] ) !!}
	                                            </div>
	                                        </div>
	                                    </div>

	                                     <!-- Company City -->
	                                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 compaddr-input">
	                                        <div class="form-group">
	                                            <label class="control-label">City*</label>
	                                            <div>
	                                                {!! Form::text('company_city', $userDetails->company_city, ['class' => 'form-control']) !!}
	                                            </div>
	                                        </div>
	                                    </div>

	                                    <!-- Company postcode -->
	                                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 compaddr-input no-mr no-pr">
	                                        <div class="form-group">
	                                            <label class="control-label">Postcode</label>
	                                            <div>
	                                                {!! Form::text('company_postcode', $userDetails->company_post_code, ['class' => 'form-control']) !!}
	                                            </div>
	                                        </div>
	                                    </div>
	                                    
	                                </div>
	                                
	                            </div>


	                        	<div class="row">
	                                <div class="col-lg-11 col-md-12 col-xs-12 section-title">Message Settings</div>
	                            </div>

	                            <div class="row section-form">
	                            	<div class="col-lg-12 col-md-12 col-xs-12 colmn comp-address">
	                                    <!-- Company State -->
	                                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 compaddr-input">
	                                        <div class="form-group">
	                                            <label class="control-label">Email Message Notification</label>
	                                            <div class="select-chosen">
	                                                {!! Form::select('email_message_notification', [ 'on'=> 'On', 'off'=> 'Off' ], $userDetails->email_message_notification, ['class' => 'form-control chosen'] ) !!}
	                                            </div>
	                                        </div>
	                                    </div>
	                                    
	                                </div>
	                            </div>

	                            <!-- Agreement -->
	                            <!-- <div class="form-group">
	                                <div class="col-lg-12 col-md-12 col-xs-12 agreement text-center">
	                                    <input type="checkbox" name="agree" value="0">
	                                    <label class="control-label">I agree to the <a href="{{ route('pages::termsAndCondition') }}" target="_blank">terms of use</a> and <a href="{{ route('pages::policy') }}" target="_blank">privacy policy</a></label>
	                                </div>
	                            </div> -->

	                             <!-- Sign Up -->
	                            <div class="form-group">
	                                <div class="col-lg-12 col-md-12 col-xs-12 center-block">
	                                    <button type="button" class="btn center-block btn-success btn-sign-up">Save Settings</button>
	                                </div>
	                            </div>
	                        {!! Form::close() !!}
	                    </div>
	    
	                  </div><!-- /.reg-container -->

	                </div>
	            </div>
	        </div>
	    </div>
	</div>

	<script>
        $(document).ready(function(){

            // $('input[name="agree"]').on('change', function(){
            //     $(this).val( $(this).is(':checked') ? 1 : 0 );
            // });
			@if( Auth::check() )
	            $('.btn-sign-up').on('click', function(e){
	                var self = $(this);
	                var $form = self.parents('form');

	                e.preventDefault();
	                e.stopImmediatePropagation();
	                $form.find('.form-group').removeClass('has-error');

	                var btn = self.button('loading');
	                $form.ajaxSubmit({
	                    url: "{{ route('save-settings') }}",
	                    type: 'post',
	                    data: {
	                    	"userid": "{{ Auth::user()->user_id }}"
	                    },
	                    success: function(response) {

	                    	// console.log(response);
	                        toastr[response.status](response.message);
	                        btn.button('reset');

	                        if( response.status != 'success' ) {
	                            var errorTarget = $('input[name="'+ response.data.target +'"]');
	                            errorTarget.focus();
	                            errorTarget.parents('.form-group').addClass('has-error');
	                        }
	                    }
	                });
            	});
			@endif
        });
    </script>
  
@endsection