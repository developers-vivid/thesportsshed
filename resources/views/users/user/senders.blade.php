@if( count($messages) > 0 )
	<ul>
	  	@foreach($messages as $message)
	      	<li class="users{{ $message->has_unread_messages ? ' active' : '' }} show-messages" data-inbox-id="{{ $message->inbox_id }}">
	      		<a href="javascript:;">
	      			<?php $userDetails = \App\User::whereUserId( (Auth::user()->user_id == $message->receiver_id ? $message->sender_id : $message->receiver_id ) )->first(); ?>
		          	<div class="user-img-container">
		          		<img class="user-pic" src="{{ url('images/thumb-pic.png') }}" alt="avatar" title="avatar" />
		          	</div>
		          	<div class="user-name">{{ str_limit($userDetails->name, 15) }} <!-- <div class="time">{{ \Carbon\Carbon::parse($message->last_message_received)->format('h:i A') }}</div> -->
		              	<span class="user-email">{{-- $userDetails->email --}}</span>
		          	</div>
	      		</a>
	      	</li> 
	  	@endforeach
	</ul>
@else
    <div class="alert alert-danger text-center">
        No chat yet
    </div>
@endif