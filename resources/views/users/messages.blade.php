@extends('layouts.master')

@section('content')

    <div id="main-container" class="single-page messages">
    <div class="container">
        <div class="row main-row">
            <div class="col-lg-3 col-sm-3 col-xs-12 sidebar-container">
                <div class="message-heading"><h3>All Messages <i class="fa fa-angle-down" aria-hidden="true"></i></h3> <!-- <span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span> --></div>
                @if( in_array(env('APP_ENV'), ['local', 'development']) )
                    <div style="padding-left: 20px;">
                        <small>{{ Auth::user()->email }}</small>
                    </div>
                @endif
                <!-- <div class="message-search col-lg-12 col-md-12 col-xs-12">
                    {{ Form::open(['route' => 'search', 'class' => 'row form-group', 'id' => 'search-form-message', 'method' => 'get']) }}
                        <input class="col-lg-11 col-sm-11 col-xs-11 form-control text-field" type="text" name="keyword" value="{{ \Input::get('keyword') }}" placeholder="Search"><button class="col-lg-1 col-sm-1 col-xs-1 form-control submit-btn"><i class="fa fa-search"></i></button>
                    {{ Form::close() }}
                </div> -->
                <div class="users-container col-lg-12 col-md-12 col-xs-12">
                    <div class="alert alert-info text-center">Loading...</div>
                </div>
            </div>
            <div class="col-lg-9 col-sm-9 col-xs-12 chat-box-container hidden">
                <div class="message-area-container">
                    
                </div>
                
                <div class="textbox-area-container">
                   <textarea id="message-textarea" class="col-lg-10 col-sm-10 col-xs-12 textarea" rows="5" placeholder="Click to Reply"></textarea><button type="button" class="col-lg-2 col-sm-2 col-xs-12 btn-default btn-primary btn-send" data-loading-text="...">SEND</button>
                </div>
            </div>
        </div>
    </div>
  </div>
  
@endsection