{!! Form::open(['url' => 'post-login', 'class' => 'login form-horizontal']) !!}
    <div class="loginmodal-header">
        <span class="login-heading">Login  <span class="grey-txt">or</span> <a href="{{ url('/register') }}" class="signup-opt">Signup</a></span>
    </div>

    <div class="row">
        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 login-btns">
            <p>Use your Facebook or Google+ account login.</p>
            <div class="social-media-container">
                <a href="{{ url('auth/google') }}" class="google btn btn-sm btn-block" title="Login via Google"><i class="fa fa-google-plus"></i> <span>Sign in with google+</span></a>
                <a href="{{ url('auth/facebook') }}" class="facebook btn btn-sm btn-block" title="Login via Facebook"><i class="fa fa-facebook"></i> <span>Sign in with facebook</span></a>
            </div>    
        </div>
        <div class="form-sep hidden-md hidden-sm hidden-xs">
            <span>or</span>
        </div>
        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="fields-container">
                {!! Form::hidden('redirect_url', $redirectUrl, ['id' => 'inputRedirectUrl']) !!}
                
                <div class="input-group">
                    <span class="input-group-addon icon-user-custom" id="email-addon"></span>
                    <span class="input-group-addon border-custom">|</span>
                    <input type="email" name="email" class="form-control" id="inputEmailAddress" placeholder="Email Address" autofocus='true' aria-describedby="email-addon">
                    <!-- <span class="input-icon"><img src="/images/user-icon.png" alt="Username" title="Username"></span>--><!-- <span class="sep col-lg-1 col-sm-1 col-xs-1">|</span> -->
                </div>
                <div class="input-group">
                    <span class="input-group-addon icon-password-custom" id="password-addon"></span>
                    <span class="input-group-addon border-custom">|</span>
                    <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Password" aria-describedby="password-addon">
                    <!-- <span class="input-icon"><img src="/images/lock-icon.png" alt="Password" title="Password"></span> --><!-- <span class="sep col-lg-1 col-sm-1 col-xs-1">|</span> -->
                </div>
                <!-- <div class="row form-group">
                    <span class="input-icon col-lg-2 col-sm-2 col-xs-2"><img src="/images/user-icon.png" alt="Username" title="Username"></span><span class="sep col-lg-1 col-sm-1 col-xs-1">|</span><span class="col-lg-9 col-sm-9 col-xs-12 input-wrap"><input type="email" class="form-control" id="inputEmailAddress" placeholder="Username"></span>
                </div>
                <div class="row form-group">
                    <span class="input-icon col-lg-2 col-sm-2 col-xs-2"><img src="/images/lock-icon.png" alt="Password" title="Password"></span><span class="sep col-lg-1 col-sm-1 col-xs-1">|</span><span class="col-lg-9 col-sm-9 col-xs-12 input-wrap"><input type="password" class="form-control" id="inputPassword" placeholder="Password"></span>
                </div> -->
                <div class="row form-group">
                    <a class="forgot-password" href="{{ route('user::forgotPassword') }}">Forgot Password?</a>
                </div>
            </div>
            <div class="login-container pull-right">
                <button type="submit" class="col-lg-12 col-sm-12 col-xs-12 btn btn-default btn-primary btn-login" data-is-cart="{{ $is_cart ? \Crypt::encrypt('yes') : \Crypt::encrypt('no') }}">Login</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}