@extends('layouts.master')

@section('title', 'Login')

@section('content')
  
  <?php $categories = \App\Helper::getCategories(); ?>
    <div id="main-container">
        <div class="container">
            <div class="row main-row">
                <div class="col-lg-3 col-sm-3 col-xs-12 sidebar-container">
                    @include('layouts.includes.gp_categories')
                    @include('layouts.filters.refined-filter', ['action' => url('/search')])
                </div>
                <div class="col-lg-9 col-sm-9 col-xs-12 items-container">
                    @include('layouts.filters.gear-filter')
                    <div class="col-lg-12">
                        <div class="alert alert-danger text-center">
                            <p>You need to log in to access this page.</p>
                            <p>Please click the login button above to log in.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .container -->
    </div><!-- #main-container -->

@endsection