@extends('layouts.master')

@section('content')
<div class="single-page forgot-password">
    <div class="container">
        <div class="row">
        	<div class="col-lg-12">
        		<div class="forgot-password-section">
                    {!! Form::open(['route' => "user::forgotPassSendMail", 'id' => 'password-reset', 'role' => 'form', 'method' => 'post']) !!}
            			<div class="header-section">
            				<h3>Find Your Account</h3>
            			</div>
            			<div class="form-group">
            				{!! Form::email('email', '', ['class' => 'form-control', 'id' => "email", 'placeholder' => 'Email Address']) !!}
            			</div>

            			<div class="form-group">
            				<button type="submit" class="btn button-forgot-password" data-loading-text="Processing...">Submit</button>
            			</div>
                    {!! Form::close() !!}
        		</div>
        	</div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){

            $('#password-reset').on('submit', function(e){
                e.preventDefault();
                e.stopImmediatePropagation();

                var self = $(this),
                    btn = self.find('.btn').button('loading');

                self.ajaxSubmit({
                    success: function(response){
                        showToastr( response.status, response.message );
                        btn.button('reset');

                        if( response.status == 'success' ) {
                            $('#email').val('');
                        }
                    }
                });
            });
        });
    </script>

    @include('layouts.includes.gp_bottom')
</div>
@endsection