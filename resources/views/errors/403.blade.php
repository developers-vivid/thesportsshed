@extends('layouts.admin.app')

@section('title', ($exception->getMessage() ? $exception->getMessage() : 'Not Authorize'))

@section('content')
    
    <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div class="alert alert-danger text-center">
                <h3>
                    <strong style="font-weight: bold;">403</strong>
                    <p>{!! $exception->getMessage() ? $exception->getMessage() : 'Not Authorize' !!}</p>
                </h3>
            </div>
          </div>
        </div>
    </div>

@endsection