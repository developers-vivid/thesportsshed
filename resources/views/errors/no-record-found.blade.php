@extends('layouts.master')

@section('title', (isset($title) ? $title : 'No record found'))

@section('content')
	
	<?php $categories = \App\Helper::getCategories(); ?>
    <div id="main-container">
        <div class="container">
            <div class="row main-row">
                <div class="col-lg-3 col-sm-3 col-xs-12 sidebar-container">
                    @include('layouts.includes.gp_categories')
                    @include('layouts.filters.refined-filter', ['action' => url('/search')])
                </div>
                <div class="col-lg-9 col-sm-9 col-xs-12 items-container">
                    @include('layouts.filters.gear-filter')
                    <div class="col-lg-12">
                        <div class="alert alert-danger text-center">
                            {!! isset($message) ? $message : 'No record found' !!}
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .container -->
    </div><!-- #main-container -->

@endsection