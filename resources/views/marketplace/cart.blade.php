@extends('layouts.master')

@section('title', \App\Helper::getCategoryBySlug( $category ) . (\Request::segment(3) ? ' &raquo; '. \App\Helper::getCategoryBySlug( \Request::segment(3) ) : ''))

@section('content')

    <?php $categories = \App\Helper::getCategories(); ?>
    <div id="main-container" class="single-page gears">
        <div class="container">
            <div class="row main-row">
                <div class="col-lg-3 col-sm-3 col-xs-12 sidebar-container">
                    @include('layouts.includes.gp_categories')
                    @include('layouts.filters.refined-filter')
                </div>
                <div class="col-lg-9 col-sm-9 col-xs-12 items-container">
                    @include('layouts.filters.gear-filter')
                    <div class="row product-gear{{ (\Session::has('searchview') && \Session::get('searchview') == 'list') ? ' list' : ' grid' }}">
                        <div class="col-lg-12">
                            <h3 class="title-area">
                                {{ \App\Helper::getCategoryBySlug( \Request::segment(2) ) }} 
                                {{ \Request::segment(3) ? ' &raquo; '. \App\Helper::getCategoryBySlug( \Request::segment(3) ) : '' }} 
                            </h3>
                        </div>
                        <div class="{{ (\Session::has('searchview') && \Session::get('searchview') == 'listviewlayout') ? ' list' : ' gridviewlayout' }}" >
                            @if( count($gears) > 0 )
                                @foreach( $gears as $gear )
                                     @if( \Session::has('searchview') && strtolower(\Session::get('searchview')) == 'list' )
                                    <div class="col-xs-12 product-item" data-my-order="1">
                                        @include('layouts.gears.list')
                                    </div>
                                        @else
                                    <div class="col-lg-4 col-sm-4 col-xs-12 product-item" data-my-order="1">
                                            @include('layouts.gears.grid')
                                        @endif
                                    </div>
                                    
                                @endforeach
                                {!! $gears->appends([
                                    'location'  => Input::get('location'),
                                    'brand'     => Input::get('brand'),
                                    'price_min' => Input::get('price_min'),
                                    'price_max' => Input::get('price_max'),
                                    'type'      => Input::get('type'),
                                    'sort'      => Input::get('sort')
                                ])->links() !!}
                            @else
                                <div class="col-lg-12">
                                    <div class="alert alert-danger text-center">
                                        No items found
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .container -->
    </div><!-- #main-container -->

@endsection
