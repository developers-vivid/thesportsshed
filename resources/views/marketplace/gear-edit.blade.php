@extends('layouts.master')

@section('title', (count((array)$gear) > 0 ? $gear->product_title : 'Gear not found'))

@section('content')

<div class="single-page edit-gears">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="panel panel-default">
                   <div class="row panel-heading">
                        <div class="col-lg-11 col-md-10 col-xs-9 heading-title"><h2>Edit Gear</h2></div>
                        <div class="col-lg-1 col-md-2 col-xs-3 logo-thumb"><img src="/images/GearPlanet_Logo.png" class="img-responsive" alt="Forsublease logo" title="Forsublease logo"></div>
                    </div>
                        @if( $gear->isOwner )
                            {!! Form::open(['id' => 'create-gear-form', 'class' => 'form-horizontal', 'role' => 'form', 'file' => true]) !!}
                                <div class="panel-body">
                                    {!! Form::hidden('gear_id', $gear->product_id, ['class' => 'form-control']) !!}
                                    <div class="row">
                                        <div class="col-lg-7 col-md-7 col-xs-12 colm1">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <!-- Category -->
                                                    <div class="row form-group">
                                                        <label class="col-lg-12 col-md-12 col-xs-12 control-label">Category</label>
                                                        <div class="col-lg-12 col-md-12 col-xs-12 select-chosen">
                                                            <select name="category" id="category" class="form-control chosen">
                                                                <option value="">Find Category</option>
                                                                @if( count($categories) > 0 )
                                                                    @foreach($categories as $category)
                                                                        <option value="{{ $category->category_id }}" {{ $category->category_id == $gear->category_id ? 'selected' : '' }}>{{ $category->name }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <!-- Sub-Category -->
                                                    <div class="row form-group">
                                                        <label class="col-lg-12 col-md-12 col-xs-12 control-label">Sub Category</label>
                                                        <div class="col-lg-12 col-md-12 col-xs-12 select-chosen">
                                                            <?php $subCategory = \App\Categories::whereParent($gear->category_id)->get(); ?>
                                                            <select name="sub_category" id="sub-category" class="form-control chosen">
                                                                <option value="">Find Sub-Category</option>
                                                                @if( count($subCategory) > 0 )
                                                                    @foreach($subCategory as $subCat)
                                                                        <option value="{{ $subCat->category_id }}" {{ $subCat->category_id == $gear->sub_category_id ? 'selected' : '' }}>{{ $subCat->name }}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <!-- Type -->
                                                    <div class="row form-group">
                                                        <label class="col-lg-12 col-md-12 col-xs-12 control-label">Type {{ $gear->product_type }}</label>
                                                        <div class="col-lg-12 col-md-12 col-xs-12 select-chosen">
                                                            {{ Form::select('type', ['new' => 'New', 'pre-owned' => 'Pre Owned'], $gear->product_type, ['id' => 'type', 'class' => 'form-control chosen', 'placeholder' => 'Type']) }}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <!-- Condition -->
                                                    <div class="row form-group">
                                                        <label class="col-lg-12 col-md-12 col-xs-12 control-label">Condition</label>
                                                        <div class="col-lg-12 col-md-12 col-xs-12 select-chosen">
                                                            <select name="condition" id="condition" class="form-control chosen">
                                                                <option value="">Condition</option>
                                                                    @if( count($conditions = \App\ProductsCondition::all()) > 0 )
                                                                      @foreach($conditions as $condition)
                                                                        <option value="{{ $condition->condition_id }}" {{ $condition->condition_id == $gear->condition_id ? 'selected' : '' }}>{{ $condition->name }}</option>
                                                                      @endforeach
                                                                    @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   

                                            <div class="row">
                                                <div class="col-lg-2 col-md-2 col-xs-12">
                                                    <!-- Brand -->
                                                    <div class="row form-group">
                                                        <label class="col-lg-12 col-md-12 col-xs-12 control-label">Brand</label>
                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                            {!! Form::text('brand', $gear->product_brand, ['class' => 'form-control' ]) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-xs-12">
                                                    <!-- Model -->
                                                    <div class="row form-group">
                                                        <label class="col-lg-12 col-md-12 col-xs-12 control-label">Model</label>
                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                            {!! Form::text('model', $gear->product_model, ['class' => 'form-control' ]) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-2 col-xs-12">
                                                    <!-- Year -->
                                                    <div class="row form-group">
                                                        <label class="col-lg-12 col-md-12 col-xs-12 control-label">Year</label>
                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                            {!! Form::text('year', $gear->product_year, ['class' => 'form-control' ]) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-2 col-xs-12">
                                                    <!-- Colour -->
                                                    <div class="row form-group">
                                                        <label class="col-lg-12 col-md-12 col-xs-12 control-label">Colour</label>
                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                            {!! Form::text('colour', $gear->product_colour, ['class' => 'form-control' ]) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-xs-12">
                                                    <!-- Where was it made? -->
                                                    <div class="row form-group">
                                                        <label class="col-lg-12 col-md-12 col-xs-12 control-label">Where was it made?</label>
                                                        <div class="col-lg-12 col-md-12 col-xs-12 select-chosen">
                                                            {!! Form::select('made_in', \App\Helper::getCountries( true ), $gear->product_made_in, ['id' => 'country', 'class' => 'form-control chosen']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <!-- Listing title -->
                                                    <div class="row form-group">
                                                        <label class="col-lg-12 col-md-12 col-xs-12 control-label">Listing Title</label>
                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                            {!! Form::text('listing_title', $gear->product_title, ['class' => 'form-control' ]) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-xs-12">
                                                    <!-- Price -->
                                                    <div class="row form-group">
                                                        <label class="col-lg-12 col-md-12 col-xs-12 control-label">Price</label>
                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                            {!! Form::text('price', $gear->product_price, ['class' => 'form-control' ]) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-xs-12">
                                                    <!-- Sale Price -->
                                                    <div class="row form-group">
                                                        <label class="col-lg-12 col-md-12 col-xs-12 control-label">Sale Price</label>
                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                            {!! Form::text('sale_price', $gear->product_sale_price, ['class' => 'form-control' ]) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   

                                            {{--
                                            <div class="row">
                                                <div class="col-lg-5 col-lg-offset-6 col-md-6 col-xs-12 colmn">
                                                    <!-- Accept offers -->
                                                    <div class="form-group">
                                                        <div class="col-md-8">
                                                            {!! Form::checkbox('accept_offers', null, ($gear->accept_offers == 'yes' ? true : false), ['class' => 'accept_offers']) !!}<label class="control-label">Accept offers</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            --}}

                                            <!-- Description -->
                                            <div class="row form-group">
                                                <label class="col-lg-12 col-md-12 col-xs-12 control-label">Description</label>
                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                    {!! Form::textarea('description', $gear->product_description, ['class' => 'form-control' ]) !!}
                                                </div>
                                            </div>  

                                            <!-- Shopping Policy -->
                                            <div class="row form-group">
                                                <label class="col-lg-12 col-md-12 col-xs-12 control-label">Returns Policy, Other Information</label>
                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                    {!! Form::textarea('shopping_policy', $gear->shopping_policy, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>

                                            <!-- Billing Details -->
                                            <div class="row form-group">
                                                <label class="col-lg-12 col-md-12 col-xs-12 control-label">Billing Details</label>
                                            </div> 
                                            <div class="row form-group">
                                                <label class="col-lg-12 col-md-12 col-xs-12 control-label">PayPal Email <small class="text-muted pull-right">This is where you will received the payment</small></label>
                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                    {!! Form::email('paypal_email', $gear->paypal_email, ['class' => 'form-control', 'placeholder' => 'paypal@email.com']) !!}
                                                </div>
                                            </div> 

                                            <!-- Where are you shipping from? -->
                                            <div class="row form-group">
                                                <label class="col-lg-12 col-md-12 col-xs-12 control-label">Where are you shipping from?</label>
                                            </div> 

                                            <div class="row form-group">
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <!-- City -->
                                                    <div class="row form-group">
                                                        <label class="col-lg-12 col-md-12 col-xs-12 control-label">City</label>
                                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                                            {!! Form::text('city', $gear->shipping_city, ['class' => 'form-control' ]) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-12">
                                                    <!-- State -->
                                                    <div class="row form-group">
                                                        <label class="col-lg-12 col-md-12 col-xs-12 control-label">State</label>
                                                        <div class="col-lg-12 col-md-12 col-xs-12 select-chosen">
                                                            {!! Form::select('state', \App\Helper::getStates( true ), $gear->shipping_state, ['id' => 'state', 'class' => 'form-control chosen']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 

                                            <!-- Shopping Policy -->
                                            <div class="row form-group">
                                                <label class="col-lg-12 col-md-12 col-xs-12 control-label">Shipping Policy</label>
                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                    {!! Form::textarea('shipping_policy', $gear->shipping_policy, ['class' => 'form-control' ]) !!}
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-lg-6 col-md-6 col-xs-6">
                                                        <!-- Local pickup -->
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <label class="control-label">Shipping Cost</label>
                                                                {!! Form::number('shipping_costs', $gear->shipping_cost, ['class' => 'form-control', 'placeholder' => 'AUD' ]) !!}
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-lg-6 col-md-6 col-xs-6">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">Shipping Methods</label>
                                                    <!-- Shipping -->
                                                    <div class="col-md-6">
                                                        {!! Form::checkbox('shipping', null, false, ['class' => 'shipping']) !!}<label class="control-label">Shipping</label>
                                                    </div>
                                                    <!-- Local pickup -->
                                                    <div class="col-md-6">
                                                        {!! Form::checkbox('local_pickup', null, false, ['class' => 'local_pickup']) !!}<label class="control-label">Local Pickup</label>
                                                    </div>
                                                </div>
                                               
                                            </div>

                                            <!-- Shipping to -->
                                            <!-- <div class="row form-group">
                                                <label class="col-lg-12 col-md-12 col-xs-12 control-label">Shipping to</label>
                                            </div> -->

                                            <!-- Shipping Heading -->
                                            <!-- <div class="row form-group shipping-heading">
                                                <div class="col-lg-6 col-md-6 col-xs-6">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">Location</label>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-xs-6">
                                                    <label class="col-lg-12 col-md-12 col-xs-12 control-label">Shipping Cost*</label>
                                                </div>
                                            </div> --> 

                                            <!-- Shipping Field -->
                                            <!-- <div class="row form-group multi-field-wrapper">
                                                <?php
                                                    $exceptState = [];
                                                    $shipLocJson = [];
                                                    if( count($gear->shippingCost) > 0 ) {
                                                        foreach($gear->shippingCost as $shipCost) {
                                                            if( ! in_array($shipCost->location, ['Metro Areas', 'Regional Areas']) )
                                                                $exceptState[] = strtolower($shipCost->location);

                                                            $shipLocJson[] = array(
                                                                "location" => $shipCost->location,
                                                                "cost" => $shipCost->cost
                                                            );
                                                        }
                                                    }
                                                ?>
                                                
                                                {!! Form::hidden('shipping_cost', json_encode($shipLocJson), ['class' => 'form-control', 'id' => 'shipping-cost']) !!}
                                                <div class="col-lg-12 col-md-12 col-xs-12 multi-fields">
                                                    @if( count($gear->shippingCost) > 0 )
                                                        @foreach($gear->shippingCost as $cost)
                                                            <div class="col-lg-12 col-md-12 col-xs-12 shipping-to {{ in_array($cost->location, ['Metro Areas', 'Regional Areas']) ? 'multi-field' : 'multi-field-add' }}">
                                                                <label class="col-lg-6 col-md-6 col-xs-6 control-label">{{ $cost->location }}</label>
                                                                <div class="{{ in_array($cost->location, ['Metro Areas', 'Regional Areas']) ? 'col-lg-6 col-md-6 col-xs-6' : 'col-lg-5 col-md-5 col-xs-5' }} currency-field">
                                                                    <i class="fa fa-usd" aria-hidden="true"></i>
                                                                    {!! Form::number('metro_areas_cost', $cost->cost, ['class' => 'form-control shipping-costs', 'placeholder' => 'AUD' ]) !!}
                                                                </div>
                                                                @if( ! in_array($cost->location, ['Metro Areas', 'Regional Areas']) )
                                                                    <div class="col-lg-1 col-md-1 col-xs-1 optionsbtn">
                                                                        <button type="button" class="remove-field fa fa-times"></button>
                                                                    </div>
                                                                @endif
                                                                <div class="col-lg-2 optionsbtn"></div>
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <div class="col-lg-12 col-md-12 col-xs-12 shipping-to multi-field">
                                                           <label class="col-lg-6 col-md-6 col-xs-6 control-label">Metro Areas</label>
                                                            <div class="col-lg-6 col-md-6 col-xs-6 currency-field">
                                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                                                {!! Form::number('metro_areas_cost', Input::get('metro_areas_cost'), ['class' => 'form-control shipping-costs', 'placeholder' => 'AUD' ]) !!}
                                                            </div>
                                                            <div class="col-lg-2 optionsbtn"></div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12 col-xs-12 shipping-to multi-field">
                                                           <label class="col-lg-6 col-md-6 col-xs-6 control-label">Regional Areas</label>
                                                            <div class="col-lg-6 col-md-6 col-xs-6 currency-field">
                                                                <i class="fa fa-usd" aria-hidden="true"></i>
                                                                {!! Form::number('regional_areas_cost', Input::get('regional_areas_cost'), ['class' => 'form-control shipping-costs', 'placeholder' => 'AUD' ]) !!}
                                                            </div>
                                                        </div>
                                                    @endif

                                                    <div class="col-lg-12 col-md-12 col-xs-12 multi-field-option hidden">
                                                       <div class="col-lg-6 col-md-6 col-xs-6 select-chosen">
                                                            {!! Form::select('location', \App\Helper::getStates( true, $exceptState ), null, ['id' => 'location', 'class' => 'form-control chosen']) !!}
                                                        </div>
                                                        <div class="col-lg-5 col-md-5 col-xs-5 currency-field">
                                                            <i class="fa fa-usd" aria-hidden="true"></i>
                                                            {!! Form::number('location_cost', Input::get('location_cost'), ['class' => 'form-control input-field', 'placeholder' => 'AUD' ]) !!}
                                                        </div>
                                                        <button type="button" class="remove-field">SAVE</button>
                                                        <div class='col-lg-1 col-md-1 col-xs-1 optionsbtn'><button type="button" class="save-field fa fa-floppy-o"></button></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-xs-12 add-container"><div class="col-lg-12 col-md-12 col-xs-12"><button type="button" class="add-field fa fa-plus-circle"></button> Add shipping Locations</div></div>
                                            </div> --> 
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-xs-12"> 
                                            <div class="row form-group">
                                                <div class="col-lg-12 col-md-12 col-xs-12 sell-thumb-image">
                                                    <img src="{{ url('images/sell-thumb-image.jpg') }}" class="img-responsive" alt="" title="" />
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                    <ul class="sell-content-list">
                                                        <li><i class="fa fa-check-circle" aria-hidden="true"></i>Reach an <span>Australia-wide</span> audience of buyers</li>
                                                        <li><i class="fa fa-check-circle" aria-hidden="true"></i>Only a <span>3.5% final value</span> fee if your item sells (payment processing fees may apply depending on payment type)</li>
                                                        <li><i class="fa fa-check-circle" aria-hidden="true"></i>Great photos help sell items! check out our <span>Guide to Great Item Photos</span></li>
                                                        <li><i class="fa fa-check-circle" aria-hidden="true"></i>Unsure how to pack your item for shipping? We’ve created a <span>Guide to Packing terms</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body upload">
                                    <div class="row section-form upload-container">
                                        <div class="col-lg-6 col-md-6 col-xs-12 clearfix">
                                            <div class="form-group">
                                                <label class="col-lg-12 col-md-12 col-xs-12 control-label multiple-uploads">Upload Photos <span class="doc-limits">(10 docs max)</span><span class="capacity-limits">(png, jpg max of 2mb)</span></label>
                                                <div class="col-lg-12 col-md-12 col-xs-12">
                                                    <div id="update-photos" class="dropzone"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-xs-12">
                                            <div class="photo-container">
                                                @include('layouts.gears.single-photo', ['photo_id' => $gear->product_primary_photo, 'photo_filename' => $gear->photo_filename])
                                                @if( count((array)$gear->photos) > 0)
                                                    @foreach($gear->photos as $photo)
                                                        @if( $photo->photo_id != $gear->product_primary_photo )
                                                            @include('layouts.gears.single-photo', ['photo_id' => $photo->photo_id, 'photo_filename' => $photo->photo_filename])
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </div>
                                            <div class="newly-add col-md-2 col-sm-6 col-xs-12">
                                                <div id="imagePreviews" class="col-lg-12 col-md-12 col-xs-12 dropzone"></div>
                                            </div>
                                        </div>
                                    </div>

                                     <!-- Edit -->
                                    <div class="form-group">
                                        <div class="col-lg-12 col-md-12 col-xs-12 center-block">
                                            <button type="button" class="btn btn-success" id="btn-update-gear" data-loading-text="Processing... Please wait.">Update</button>
                                        </div>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        @else
                            <div class="alert alert-danger text-center">
                                {{ trans('messages.gear.not_owner') }}
                            </div>
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('body').on('click', '.delete-img', function(){
            var self = $(this);
            var photoId = self.data('photo-id');

            bootbox.dialog({
                message: "<h3 class='text-center'>Do you really want to remove this photo?</h3>",
                title: "Delete Photo",
                buttons: {
                    success: {
                        label: "Continue",
                        className: "btn-success",
                        callback: function() {
                            $.ajax({
                                "url" : "{{ route('mygears::remove-photo') }}",
                                "type" : 'post',
                                "data" : {
                                    "id" : photoId
                                }
                            }).done(function(response){
                                toastr[response.status](response.message);

                                if( response.status == 'success' ){
                                    self.parents('.img-preview-cont').fadeOut(300, function(){
                                        $(this).remove();
                                        bootbox.hideAll();
                                    });
                                }
                            });

                            return false;
                        }
                    },
                    danger: {
                        label: "Cancel",
                        className: "btn-danger"
                    }
                }
            });
        });

        $('body').on('click', '.add-primary-photo', function(){
            var self = $(this);
            var photoId = self.data('photo-id');
            $('.photo-container').find('.primary-img-con').removeClass('primary-img-con');
            $('.photo-container').find('.primary-photo').removeClass('primary-photo');
            
            $(this).addClass('primary-photo');
            $(this).siblings('.delete-img').addClass('primary-photo');
            $(this).parent().addClass('primary-img-con');

            $.ajax({
                "url" : "{{ route('mygears::primary-photo') }}",
                "type" : 'post',
                "data" : {
                    "id" : photoId
                }
            }).done(function(response){
                toastr[response.status](response.message);
            });

        });
    });
</script>

@endsection
