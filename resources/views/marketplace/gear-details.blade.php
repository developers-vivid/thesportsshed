@extends('layouts.master')

@section('title', (count((array)$gear) > 0 ? $gear->product_title : 'Gear not found'))

@section('content')

    <div id="main-container" class="single-page gear-details">
        <div class="container">
            <div class="row main-row">
                @if( count((array)$gear) > 0 && !empty($gear->photos) )
                    
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <h3 class="col-lg-12 item-title">{{ $gear->product_title }} 
                            <span>Listed {{ \Carbon\Carbon::createFromTimeStamp(strtotime($gear->created_at))->diffForHumans() }} by <span class="contact-name">{{ $gear->first_name .' '. $gear->last_name }}</span></span></h3>
                        </div>

                        <div class="row image-container maingear-details pikachoose">
                            <div class="col-lg-9 col-md-9">
                                @if( count($gear->photos) > 0 )
                                    <div class="image-carousel img-wrap">
                                    @foreach($gear->photos as $photo)

                                            <div class="carousel image-big">
                                                <div class="image-carousel-container img-zoom" data-fancybox-group="thumb" href="{{ url('images', ['gears', $photo->photo_filename]) }}">
                                                    <img class="img-responsive" src="{{ url('images', ['gears', $photo->photo_filename]) }}" alt="" title"" class="img-responsive"/>
                                                </div>
                                            </div>
                                    @endforeach
                                    </div>
                                @else
                                    <div class="alert alert-danger text-center">
                                        No other images found
                                    </div>
                                @endif
                            </div>



                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 sidebar-slider">
                                @if( count($gear->photos) > 0 )
                                    <div class="image-slider-container">
                                        <div class="buttons-slick">
                                            <div class="prev-slick"></div>
                                            <div class="next-slick"></div>
                                        </div>
                                        <div class="image-slider">
                                        @foreach($gear->photos as $photo)
                                                
                                                <div class="image-slider-container">
                                                    <div class="image-slider-individual" style="background-image: url('{{ url('images', ['gears', 'thumbs', $photo->photo_filename]) }}');"></div>
                                                </div>    
                                               
                                        @endforeach
                                        </div>
                                    </div>
                                @else
                                    <div class="alert alert-danger text-center">
                                        No other images found
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row section-top">
                            <div class="col-lg-9 col-md-8 col-sm-7 col-xs-12 product-details">
                                <div class="tab">
                                    <ul class="tabs">
                                        <li><a href="#">Description</a></li>
                                        <li><a href="#">Shop Policies</a></li>
                                        <li><a href="#">Shipping Policy</a></li>
                                    </ul> <!-- / tabs -->

                                    <div class="tab_content">
                                        <div class="tabs_item">
                                            <p>{!! nl2br($gear->product_description) !!}</p>
                                            <div class="product-specs">
                                                <h4>Product Specs</h4>
                                                <ul>
                                                    <li><b>Condition:</b> <span>{{ $gear->product_condition }}</span></li>
                                                    <li><b>Make:</b> <span>{{ $gear->product_brand ? $gear->product_brand : '-' }}</span></li>
                                                    <li><b>Model:</b> <span>{{ $gear->product_model ? $gear->product_model : '-' }}</span></li>
                                                    <li><b>Colour:</b> <span>{{ $gear->product_colour ? $gear->product_colour : '-' }}</span></li>
                                                    <li><b>Category:</b> <span>{{ $gear->product_category }}</li>
                                                    <li><b>Sub Category:</b> <span>{{ $gear->sub_category_id != "" ? \App\Categories::whereCategoryId($gear->sub_category_id)->value('name') : '-' }}</li>
                                                    <li><b>Year:</b> <span>{{ $gear->product_year ? $gear->product_year : '-' }}</span></li>
                                                    <li><b>Made In:</b> <span>{{ $gear->product_made_in ? ucwords($gear->product_made_in) : '-' }}</span></li>
                                                </ul>
                                            </div>
                                        </div> <!-- / tabs_item -->
                                        <div class="tabs_item">
                                            <p>{!! nl2br($gear->shopping_policy) !!}</p>
                                        </div> <!-- / tabs_item -->

                                        <div class="tabs_item">
                                            <p>{!! nl2br($gear->shipping_policy) !!}</p>
                                        </div> <!-- / tabs_item -->

                                    </div> <!-- / tab_content -->
                                </div> <!-- / tab -->
                            </div>

                            <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12 sidebar-container">
                                <div class="col-lg-12 col-sm-12 col-xs-12 clearfix options-container">
                                    @if( is_numeric($gear->product_sale_price) && $gear->product_sale_price > 0 )
                                    <div class="price">
                                        <div class="product-price"><span class="cross-out">${{ $gear->product_price }}</span> <span>AUD</span></div>
                                        <div class="sale-price">${{ $gear->product_sale_price }} <span>AUD</span></div>
                                    </div>
                                    @else
                                    <div class="price">
                                        <div class="product-price"><br/></div>
                                        <div class="sale-price">${{ $gear->product_price }} <span>AUD</span></div>
                                    </div>
                                    @endif
                                    @if( ! Auth::check() || ! $gear->isOwner )
                                        <button type="button" data-product-id="{{ \Crypt::encrypt($gear->product_id) }}" class="{{ config('gp_conf.isBuyNow') ? 'buy-now' : 'add-to-cart' }} make-an-offer-modal make-an-offer btn btn-primary buy-now">Buy Now</button>
                                        <button type="submit" class="contact-seller btn btn-success {{ Auth::check() ? 'btn-contact-seller' : 'login-modal' }}" data-loading-text="Processing..." data-seller-id="{{ $gear->created_by }}">{!! Auth::check() ? 'Contact Seller' : '<small>Please login to contact the seller</small>' !!}</button>
                                    @endif
                                </div>
                                <div class="col-lg-12 col-ms-12 col-xs-12 clearfix seller-container">
                                    <span class="seller-name">{{ $gear->first_name .' '. $gear->last_name }}</span>
                                    <span class="seller-address">
                                        {{ $gear->company_street }}  
                                        {{ $gear->company_city }}  
                                        {{ $gear->company_state }}  
                                        {{ $gear->company_post_code }}
                                    </span>
                                    <!-- <span class="seller-details">Member Since: {{ \Carbon\Carbon::parse($gear->date_registered)->toFormattedDateString() }}</span> -->
                                    <span class="seller-details">Member Since: {{ \Carbon\Carbon::parse($gear->date_registered)->format('d/n/y') }}</span>
                                </div>
                                <div class="col-lg-12 col-ms-12 col-xs-12 clearfix share-container">
                                    <?php
                                        $redirectUrl = route('gears::marketplace', [$gear->product_blurb]);
                                        $shareFbUrl = "app_id=". env('FACEBOOK_CLIENT_ID') ."&".
                                            "link=". $redirectUrl .'&'.
                                            "picture=". url('images', ['gears', $gear->photo_filename]) .'&'.
                                            "name=". rawurlencode( $gear->product_title ) .'&'.
                                            "caption=". rawurlencode( $gear->product_blurb ) .'&'.
                                            "display=page" .'&'.
                                            "description=". rawurlencode( htmlspecialchars_decode($gear->product_description) ) .'&'.
                                            "message=". rawurlencode( htmlspecialchars_decode($gear->product_description) ) .'&'.
                                            "redirect_uri=". urlencode( $redirectUrl );
                                        $shareTwitterUrl = "https://www.twitter.com/share?text=". urlencode($gear->product_title) ."&url=". urlencode( route('gears::marketplace', [$gear->product_blurb]) ) ."&via=GearPlanet";
                                    ?>
                                    <span>Share on <a href="http://www.facebook.com/dialog/feed?<?php echo $shareFbUrl; ?>" target="_blank"><img src="{{ url('images/fb-icon.png') }}" alt="{{ $gear->product_title }}" title="Share via Facebook"/></a><a href="{{ $shareTwitterUrl }}" target="_blank"><img src="{{ url('images/twitter-icon.png') }}" alt="{{ $gear->product_title }}" title="Share via Twitter" /></a></span>
                                </div>
                                
                            </div>
                            
                            <div class="col-lg-12 col-ms-12 col-xs-12 clearfix image-part related-item">
                                <h2>Related Items</h2>
                                @if( count($relatedItems) > 0 )
                                    @foreach($relatedItems as $item)
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 related-box">
                                            <a class="sidebar-thumbnail" href="{{ route('gears::marketplace', [$item->product_blurb]) }}" title="{{ $item->product_title }}">
                                                <div class="image-container" style="background-image: url('{{ url('images', ['gears', 'thumbs', $item->photo_filename]) }}')"></div>
                                                <span>{{ str_limit($item->product_title, 20, '...') }}</span>
                                            </a>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="alert alert-danger text-center">
                                        No related gears found
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    
                @else
                    <div class="col-lg-12">
                        <div class="alert alert-danger text-center">
                            Gear not found test
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <script>
    $(document).ready(function(){

        function setZoom(){
            $('.img-zoom').trigger('zoom.destroy');
            
            $("img.zoomImg").remove();

            $('.img-zoom').zoom({
                magnify: 2.5,
            });
        }

        function setZoomMobile(){
            $('.img-zoom').fancybox({
                closeBtn  : true,
                arrows    : true,
                cyclic : true,
                padding: [10, 10, 10, 10],
                fixed: false,
            });
        }
        $('.image-carousel').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.image-slider',
                fade: true,
                adaptiveHeight: true,
        });

        
        $('.image-slider').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.image-carousel',
            dots: false,
            focusOnSelect: true,
            vertical: true,
            centerMode: false,
            adaptiveHeight: true,

            nextArrow: $('.prev-slick'),
            prevArrow: $('.next-slick'),

            responsive: [
                {
                    breakpoint: 991,
                    settings:{
                        slidesToShow: 3,
                        vertical: false,
                        centerMode: true,
                    },
                },
                {
                    breakpoint: 550,
                    settings:{
                        slidesToShow: 3,
                        vertical: false,
                        centerMode: true,
                    }
                }
            ],
            
        });

        

        // $('.prev-slick').on('click', function(e){
        //     $('.image-slider').slick('slickNext');
        // });

        // $('.next-slick').on('click', function(e){
        //     $('.image-slider').slick('slickPrev');
        // });
        
        var isMobile = false; //initiate as false
        // device detection
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

        if( !isMobile ){
            
            setZoom();
             console.log('di man ni mobile tawon ni');
        } else{
            setZoomMobile();
            console.log('mobile man tawon ni');
        }
     });
    </script>
@endsection
