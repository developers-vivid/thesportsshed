@extends('layouts.master')

@section('title', "Australia’s Music Marketplace")

@section('content')
    <?php $categories = \App\Helper::getCategories(); ?>
    <div id="main-container" class="single-page gears">
        <div class="container">
            <div class="row main-row">
                Market Place home
            </div>
        </div><!-- .container -->
    </div><!-- #main-container -->
@endsection
