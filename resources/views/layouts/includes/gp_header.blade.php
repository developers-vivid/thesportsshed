<style>
    @media only screen and (max-width: 992px) and (min-width: 768px)  {
    .customNav1{
	margin-top:-10px !important;
    }
	.customNav1container{
	font-size:15px !important;
	}
	}
    @media only screen and (max-width: 768px) and (min-width: 320px)  {
    .customSubleaseLogo{
	position:relative;
	left:20px;
    }
	}	
</style>
<header id="main-header" class="fixed" style="z-index:100 !important;">
	<div class="header">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-sm-3 col-xs-12 logo-container">
					<a href="{{ route('home') }}">
						<img src="{{ url('images/gp-logo.png') }}" class="img-responsive customSubleaseLogo" alt="forsublease Logo" title="Forsublease logo" />
					</a>
				</div>
				<div class="col-lg-9 col-sm-9 col-xs-12 header-info-container">
					<div id="search-header-container" class="col-lg-6 col-sm-6 col-xs-12">
						{{ Form::open(['route' => 'search', 'class' => 'row form-group', 'id' => 'search-form-header', 'method' => 'get']) }}
						  	<input class="col-lg-9 col-lg-offset-2 col-sm-11 col-xs-12 form-control text-field" type="text" name="keyword" value="{{ \Input::get('keyword') }}" placeholder="Find Office"><button class="col-lg-1 col-sm-1 col-xs-12 form-control submit-btn"><i class="fa fa-search"></i></button>
						{{ Form::close() }}
					</div> 
					<div class="col-lg-6 col-sm-6 col-xs-12 options-container">
						<!-- <span class="links-item sell btn btn-danger"><a href="{{ route('gears::sell') }}">+SELL</a></span>	 -->
							@if( !Auth::check() )
								<div class="headerlinks">
									<div class="links-item checkout no-border no-mr mycartlink">
										<a href="{{ route('cart::my-cart') }}">
											<?php $cart = \App\Cart::whereToken(\Session::get('_token'))->count(); ?>
											<i class="fa fa-shopping-cart" aria-hidden="true"></i> Checkout<span class="cart-count">{{ $cart > 0 ? ' ('. $cart .')' : '' }}</span>
										</a>
									</div>
								</div>
							@else
								<div class="headerlinks margin-top-header">
								</div>
							@endif
							
						
						<div class="nav-container-circles">
							@if( Auth::check() )
								<?php $totalUnreadMessages = \App\UsersMessages::getTotalUsersUnReadMessages( Auth::user()->user_id ); ?>
								<!-- <span class="links-item login btn btn-success"><a href="{{ url('/sign-out') }}">SIGNOUT</a></span> -->
									<div class="container-circles">
										<a href="{{ route('messages::messages') }}"><img src="{{ url('images/mail-icon.png') }}" width="37px" height="37px;"></a>
										<span class="messages-notify{{ $totalUnreadMessages > 0 ? '' : ' hidden' }}" id="total-unread-messages">{{ $totalUnreadMessages }}</span>
									</div>
									<div class="container-circles">
										
										<img class="user-circle" src="{{ url('images/person-icon.png') }}" width="37px" height="37px;">
									
										<div class="sub-container">
											<ul>
												@if( config('gp_conf.allseller') || (Auth::check() && Auth::user()->seller == 'yes') )
													<li><a href="{{ route('mygears::ads') }}">My Ads</a></li>
												@endif
												<li>
													<a href="{{ route('cart::my-cart') }}">
														<?php $cart = \App\Cart::whereToken(\Session::get('_token'))->count(); ?>
														My Cart<span class="cart-count">{{ $cart > 0 ? ' ('. $cart .')' : '' }}</span>
													</a>
												</li>
												<li><a href="{{ route('messages::messages') }}">Messages{{ $totalUnreadMessages > 0 ? ' ('. $totalUnreadMessages .')' : '' }}</a></li>
												<li><a href="{{ route('user-profile') }}">Account Settings</a></li>
												<li><a href="{{ url('/sign-out') }}">Sign Out</a></li>
											</ul>
										</div>
									</div>
							@endif
								<span class="links-item sell btn btn-danger"><a href="{{ route('gears::sell') }}">+Rent Out</a></span>
								
							@if( ! Auth::check() )
								<span class="links-item join no-border btn btn-warning"><a href="{{ url('/register') }}">Join</a></span>
								<span class="links-item login btn btn-info"><a href="javascript:;" class="login-modal" data-is-cart="{{ Route::currentRouteName() == 'cart::checkout' ? true : false }}" data-redirect-url="{{ Route::currentRouteName() == 'cart::my-cart' ? route(Route::currentRouteName()) : '' }}">Login</a></span>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div><!-- .container -->
	</div>
</header><!-- header -->
@if( ! Auth::check() )
<div id="margin-top" class="margin-top-out"></div>
@else
<div id="margin-top"></div>
@endif

<nav id="main-nav" class="customNav1" style="background-color: #6ed5d4;">
<!-- navbar navbar-default removed from class for a while -->
	<div class="container">
		<div class="row">
			<div class="navbar-header">

	            <!-- Collapsed Hamburger -->
	            <style type="text/css">
	            /*button.btn-custom-ni{
	            	display: none;
					position: relative;
					float: right;
					margin-right: 10px;
					padding: 4px 6px;
					margin-top: 8px;
					margin-bottom: 8px;
					background-color: transparent;
					background-image: none;
					border: 1px solid transparent;
					border-radius: 4px;
					outline: none;
	            }*/
	            a.mmenu{
	            	display: none;
					position: relative;
					float: right;
					margin-right: 10px;
					padding: 4px 6px;
					margin-top: 8px;
					margin-bottom: 8px;
					background-color: transparent;
					background-image: none;
					border: 1px solid transparent;
					border-radius: 4px;
					outline: none;
					color: #6ed5d4;
	            }
	            .drawer--right.drawer-open .drawer-hamburger {
	                right: 28.25rem;
	            }

	            nav#main-nav ul li a {
					    background: none;
					    color: #ffffff;
					    font-size: 16px;
					    line-height: 16px;
					    padding: 0;
					}

					a.mmenu {
					    color: #ffffff;
					}
	            </style>
	           <!--  <button type="button" class="drawer-toggle btn-custom-ni">
	            	<span class="fa fa-bars" style="font-size: 22px;"></span>
	            </button> -->

	            <a href="#menu" class="mmenu"><span class="fa fa-bars" style="font-size: 22px;"></span></a>

				<nav id="menu">
					<div class="mmenu-con">
						<ul>
							<div class="search-bar">
								{{ Form::open(['route' => 'search', 'class' => 'form-group', 'id' => 'search-form-header', 'method' => 'get']) }}
								  	<input class="form-control text-field drawer-search" type="text" name="keyword" value="{{ \Input::get('keyword') }}" placeholder="Find Office"><button class="form-control submit-btn"><i class="fa fa-search"></i></button>
								{{ Form::close() }}
							</div>
						    @if( count($categories = \App\Helper::getCategories()) > 0 )
						        @foreach($categories as $category)
						            <?php
						                $currentMenu = (Route::currentRouteName() == 'gears::gearmarketplace' && \Request::segment(2) == $category->slug);
						                $catCntr = App\Products::getTotalProducts($category->category_id);
						            ?>
						            <li class='drawer-dropdown' {!! $currentMenu ? "class='active drawer-dropdown'" : "" !!}>
						                {!! link_to_route(
						                	'gears::gearmarketplace',
						                	($category->name .' '. ($catCntr > 0 ? '('. $catCntr .')' : '')),
						                	[$category->slug, null, 'location' => \Input::get('location'), 'brand' => \Input::get('brand'), 'price_min' => \Input::get('price_min'), 'price_max' => \Input::get('price_max'), 'type' => \Input::get('type'), 'sort' => \Input::get('sort')],
						                	[ 'class' => 'drawer-dropdown-menu-item' ]
						                	
						                ) !!}
						                @if( count($category->children) > 0 )
							                <!-- <a href="#" class="btn-float-left" data-toggle="dropdown" aria-expanded="false">
							                	<span class="drawer-caret"></span>
							                </a> -->
						                    <ul class="drawer-dropdown-menu">
						                        @foreach($category->children as $sc)
						                            <?php $subCatCntr = App\Products::getTotalProducts($category->category_id, $sc->category_id); ?>
						                            <li class="menu-item{{ $currentMenu && \Request::segment(3) == $sc->slug ? ' active' : '' }}">
						                                {!! link_to_route('gears::gearmarketplace', ($sc->name .' '. ($subCatCntr > 0 ? '('. $subCatCntr .')' : '')), [$category->slug, $sc->slug, 'location' => \Input::get('location'), 'brand' => \Input::get('brand'), 'price_min' => \Input::get('price_min'), 'price_max' => \Input::get('price_max'), 'type' => \Input::get('type'), 'sort' => \Input::get('sort')], ['class' => 'drawer-dropdown-menu-item']) !!}
						                                <!-- <a href="{{ route('gears::gearmarketplace', [$category->slug, $sc->slug]) }}">{{ $sc->name }} {{ $subCatCntr > 0 ? '('. $subCatCntr .')' : '' }}</a> -->
						                            </li>
						                         @endforeach
						                    </ul>
						                    
						                @endif
						            </li>
						        @endforeach
						    @endif
					    	<li class="nav-logo">
					    		<a href="{{ route('home') }}"><img src="{{ url('images/gearplanet-logo-mobile.png') }}" class="img-responsive" alt="Forsublease logo" title="Forsublease logo" /></a>
					    	</li> 
					    	<li class="nav-email">
					    		<span>Email: <a href="mailto:admin@gearplanet.com.au">admin@forsublease.com.au</a></span>
					    	</li>
					    	<li class="twitter social-media">
					    		<a href="#" target="_blank"><img src="{{ url('images/twitter-circle.png') }}" class="img-responsive" alt="Forsublease logo" title="Forsublease logo" /></a>
					    	</li>
							<li class="facebook social-media">
								<a href="#" target="_blank"><img src="{{ url('images/facebook-circle.png') }}" class="img-responsive" alt="Forsublease logo" title="Forsublease logo" /></a>
							</li>
							<li class="instagram social-media">
								<a href="#" target="_blank"><img src="{{ url('images/instagram-circle.png') }}" class="img-responsive" alt="Forsublease logo" title="Forsublease logo" /></a>
							</li>
						</ul>
					</div>
				</nav>
	            
        	</div>
			
			<!-- header menu list -->
			<!--
        	<div class="collapse navbar-collapse" id="app-navbar-collapse">
        		<ul class="nav navbar-nav">
					@if( count($categories = \App\Helper::getCategories()) > 0 )
						@foreach($categories as $category)
								@if( strtolower($category->menu))
				    			<li {{ strtolower(\Request::segment(2)) == $category->slug ? 'class=active' : '' }} >
				    				<a class="customNav1container" href="{{ route('gears::gearmarketplace', [$category->slug]) }}">{{ $category->menu }}</a>
				    			</li>
				    		@endif
				  		@endforeach
				  	@endif
			  	</ul>
        	</div>
			-->
			
		</div>
	</div><!-- .container -->
</nav><!-- nav -->

@if(Request::url() === url('/gears/sell'))
	<div id="banner-container">
        <div class="img-container"><img src="{{ url('images/SellItem_Banner.jpg') }}" class="img-responsive" alt="" title="" /></div>
    </div><!-- #banner-container --> 
@elseif (Request::url() === url('/mygears/ads'))
	<div id="banner-container">
        <div class="img-container"><img src="{{ url('images/MyAdds_Banner.jpg') }}" class="img-responsive" alt="" title="" /></div>
    </div><!-- #banner-container -->
@elseif (Request::url() === url('/register'))
	<div id="banner-container">
        <div class="col-lg-12 col-md-12 col-xs-12 banner-item">
            <div class="heading-container">
                        <h2>YOUR OFFICE JUST A CLICK AWAY</h2>
                        </div>
            <div class="img-container" style="background: url({{ url('images/homepage_banner1.jpg') }}) 0 0 no-repeat; background-size: cover; background-position: center;"></div>
        </div>
    </div><!-- #banner-container -->
@endif
