@if(Request::url() != route('home'))
<div id="main-bottom" class="customBottom">
	<div class="container customDiv1">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-xs-12 bottom-container" >
				<h3>Join us at Forsublease</h3>
				<ul class="list-unstyled footer-li">
					<li>Free to join list items</li>
					<li>Great deals on for lease offices</li>
					<li>Join a growing community</li>
				</ul>
			</div>
		</div>
	</div><!-- .container -->
@endif


<style>
    @media only screen and (max-width: 360px) and (min-width: 321px)  {
    .customPageIconsTextFooter1{
    position:relative;
	left:90px;
    }
    .customPageIconsTextFooter2{
    position:relative;
	left:100px;
	margin-top:20px;
    }	
	}
    @media only screen and (max-width: 320px) and (min-width: 300px)  {
    .customPageIconsTextFooter1{
    position:relative;
	left:80px;
    }
    .customPageIconsTextFooter2{
    position:relative;
	left:90px;
	margin-top:20px;
    }	
	}	
    @media only screen and (max-width: 299px) and (min-width: 150px)  {
    .customPageIconsTextFooter1{
    position:relative;
	left:55px;
    }
    .customPageIconsTextFooter2{
    position:relative;
	left:65px;
	margin-top:20px;
    }	
	}	
</style>


<footer id="main-footer">
	<div class="container " style="
    padding-left: 10%;font-size:20px
">
		
			
			<div class="col-lg-6 col-md-6 col-xs-12">
				<div>
					<h2 class="customPageIconsTextFooter1">ABOUT</h2>
					<ul>
						<li><a href="{{ route('pages::aboutUs') }}">About Us</a></li>
						<li><a href="{{ route('pages::whyUseGP') }}">Why Choose Forsublease</a></li>
						<li><a href="{{ route('pages::blogs') }}">Blog</a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-xs-12">
				<div>
					<h2 class="customPageIconsTextFooter2">INFO</h2>
					<ul>
						<li><a href="{{ route('pages::faqs') }}">FAQ</a></li>
						<li><a href="{{ route('pages::termsAndCondition') }}">Terms & Conditions</a></li>
						<li><a href="{{ route('pages::policy') }}">Policy</a></li>
					</ul>
				</div>
				
			</div>
			<!-- <div class="col-lg-4 col-md-4 col-xs-12">
				<div>
					<h2>HOW TO</h2>
					<ul>
						<li><a href="{{ route('pages::gearPhotography') }}">Forsublease.com.au</a></li>
						
					</ul>
				</div>
			</div> -->
		
	</div><!-- .container -->
</footer><!-- footer -->

<div id="branding-container">
	<div class="container">
		<div class="row">
			<div class="branding-content">
				<p class="col-lg-4 col-sm-4 col-xs-12" style="margin-top:11px;">Copyright &copy; 2017 <b><a href="#">Forsublease</a></b>. All Rights Reserved.</p>

				
					<p class="col-lg-4 col-sm-4 col-xs-12 social-white-icon" style="margin-top:-7px;">
							<!-- <a href="https://www.twitter.com/gearplanetaus" target="_blank"><i class="fa fa-twitter twitter"></i></a>
							<a href="https://www.facebook.com/gearplanetaus" target="_blank"><i class="fa fa-facebook facebook"></i></a>
							<a href="https://www.instagram.com/gearplanet" target="_blank"><i class="fa fa-instagram instagram"></i></a> -->
							<a href="#" target="_blank"><img src="{{ url('images/icons/facebook-white.png') }}" class="" alt="" title="" /></a>
							<a href="#" target="_blank"><img src="{{ url('images/icons/twitter-white.png') }}" class="" alt="" title="" /></a>
							<a href="#" target="_blank"><img src="{{ url('images/icons/googleplus-white.png') }}" class="" alt="" title="" /></a>
							<a href="#" target="_blank"><img src="{{ url('images/icons/linkdln-white.png') }}" class="" alt="" title=""/></a>

					</p>

				

				<p class="col-lg-4 col-sm-4 col-xs-12">Website Design Perth <a href="#" target="_blank"><img src="{{ url('images/core-logo.png') }}" class="img-responsive" alt="" title="" style="max-width: 75px"/></a></p> 
			</div>
		</div>
	</div><!-- /.container -->
</div><!-- /#branding-container -->
</div><!-- footer -->