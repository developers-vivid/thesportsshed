
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Forsublease - API {{ (isset($method) ? ucfirst(str_replace("-", " ", $method)) : 'References') }}</title>
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/api.css') }}">
    <script src="{{ elixir('js/master.js') }}" type="text/javascript"></script>
  </head>

  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <!-- Collapsed Hamburger -->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ route('apiDocHome') }}">Forsublease</a>
        </div>
        <div class="navbar-collapse collapse header-menu" id="app-navbar-collapse" aria-expanded="true">
          <ul class="nav navbar-nav mobile-menu">
            @foreach( config('gp_conf.menus') as $index => $menu )
              <li role="presentation" class="dropdown{{ in_array($method, $menu) ? ' active open' : '' }}" id="{{ $index }}">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">
                  {{ ucwords(str_replace('_', ' ', $index)) }} <span class="caret"></span> 
                </a> 
                <ul class="dropdown-menu"> 
                  @foreach( $menu as $i => $m )
                    <li {!! isset($method) && $method != '' && $method == $m ? 'class="active"' : '' !!}><a href="{{ route('apiDocHome', [$m]) }}">{{ ucwords(str_replace('-', ' ', $m)) }}</a></li> 
                  @endforeach
                </ul> 
              </li>
            @endforeach
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            @foreach( config('gp_conf.menus') as $index => $menu )
              <div class="panel panel-default{{ in_array($method, $menu) ? ' current' : '' }}">
                <div class="panel-heading menu" role="tab" id="{{ $index }}">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ 'collapse-'.$index }}" aria-expanded="true" aria-controls="{{ 'collapse-'.$index }}">
                      {{ ucwords(str_replace('_', ' ', $index)) }}
                    </a>
                  </h4>
                </div>
                <div id="{{ 'collapse-'.$index }}" class="panel-collapse collapse{{ in_array($method, $menu) ? ' in' : '' }}" role="tabpanel" aria-labelledby="{{ $index }}">
                  <div class="panel-body" style="padding: 0;">
                    <ul class="nav" style="margin-bottom: 0px;">
                      @foreach( $menu as $i => $m )
                        <li {!! isset($method) && $method != '' && $method == $m ? 'class="active"' : '' !!}>
                          <a href="{{ route('apiDocHome', [$m]) }}">{{ ucwords(str_replace('-', ' ', $m)) }}</a>
                        </li>              
                      @endforeach
                    </ul>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
        @yield('content')
      </div>
    </div>
    <script src="{{ elixir('js/api.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        // $('.mobile-menu > li.active a').click();
        // $('body').find('.sidebar').animate({
        //   scrollTop: $('.nav-sidebar li.active').offset().top - 50
        // }, 800);
      });
    </script>
  </body>
</html>
