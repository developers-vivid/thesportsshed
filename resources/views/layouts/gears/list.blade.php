<div class="row listview">
    <div class="col-lg-4 col-sm-4 col-xs-12 img-container">
        <a href="{{ route('gears::marketplace', [$gear->product_blurb]) }}" title="">
            <img src="{{ url('images', ['gears', $gear->gear_photo]) }}" class="img-responsive" alt="" title="" />
        </a>
    </div>
    <div class="col-lg-8 col-sm-8 col-xs-12 content-container">
        <h3 class="col-lg-12 col-sm-12 col-xs-12 item-title clearfix">
            <a href="{{ route('gears::marketplace', [$gear->product_blurb]) }}" title="{{ $gear->product_title }}">{{ str_limit($gear->product_title, 30, '...') }}</a>
            <span class="date">Date 
                @if( $gear->status == 'sold' && !is_null($gear->sold_at) )
                    {{ 'Ended: '. \Carbon\Carbon::parse($gear->sold_at)->format('F j, Y') }}
                
                @elseif( $gear->status == 'cancelled' && !is_null($gear->cancelled_at) )
                    {{ 'Cancelled: '. \Carbon\Carbon::parse($gear->cancelled_at)->format('F j, Y') }}

                @else
                    {{ 'Listed: '. \Carbon\Carbon::parse($gear->created_at)->format('F j, Y') }}

                @endif
            </span>
            <!-- <span>Listed {{ \Carbon\Carbon::createFromTimeStamp(strtotime($gear->created_at))->diffForHumans() }} by <span class="contact-name">{{ $gear->owner }}</span></span> -->
        </h3>
        <p class="col-lg-12 col-ms-12 col-xs-12 item-desc clearfix">
            {!! strip_tags(str_limit(htmlspecialchars_decode($gear->product_description), 200), '<br>') !!}
        </p>
        @if( is_numeric($gear->product_sale_price) && $gear->product_sale_price > 0 )
        <div class="col-lg-12 col-sm-12 col-xs-12 price clearfix">
            <div class="product-price"><span class="cross-out">${{ $gear->product_price }}</span> <span>AUD</span></div>
            <div class="sale-price">${{ $gear->product_sale_price }} <span>AUD</span></div>
        </div>
       
        @else
        <span class="col-lg-12 col-sm-12 col-xs-12 price clearfix">${{ $gear->product_price }} <span>AUD</span></span>
        @endif
        
        <div class="col-lg-12 col-sm-12 col-xs-12 buttons-container">
            @if( Route::currentRouteName() != 'mygears::ads' )
                @if( config('gp_conf.isBuyNow') )
                    <a href="javascript:;" data-product-id="{{ \Crypt::encrypt($gear->product_id) }}" class="buy-now btn btn-success" title="{{ $gear->product_title }}">Buy now</a>
                @else
                    <a href="javascript:;" data-product-id="{{ \Crypt::encrypt($gear->product_id) }}" class="add-to-cart btn btn-success" title="{{ $gear->product_title }}">Add to cart</a>
                @endif
            @endif
            @if( Route::currentRouteName() == 'mygears::ads' )
                <a href="{{ route('mygears::edit', [$gear->product_id]) }}" class="edit-btn btn btn-success" title="{{ $gear->product_title }}">Edit</a>
                @if( (\Input::has('status') && strtolower(\Input::get('status')) == 'cancelled') || ($gear->status == 'cancelled' || $gear->status == 'sold') )
                    <a href="{{ route('mygears::activateAd', [$gear->product_id]) }}" class="edit-btn btn btn-primary btn-spacing" title="">Activate Ad</a>
                @else
                    <a href="{{ route('mygears::cancelAd', [$gear->product_id]) }}" class="edit-btn btn btn-warning btn-spacing" title="">Cancel Ad</a>
                @endif
                @if( config('gp_conf.can.delete_ad') )
                    <a href="{{ route('mygears::deleteAd', [$gear->product_id]) }}" class="edit-btn btn btn-danger btn-spacing" title="">Delete Ad</a>
                @endif
            @endif

            <!--Display will show if the list is under Sold -->
            <!-- <a href="" class="edit-btn btn-danger btn" title="{{ $gear->product_title }}">Relist</a> -->
           
        </div>
    </div>
</div>