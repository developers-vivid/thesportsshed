<style>
	.buy-now{
	border-style:none !important;
	}
    @media only screen and (max-width: 1199px) and (min-width: 993px)  {
	.buy-now{
	padding-right:50px;
	}
	}
    @media only screen and (max-width: 992px) and (min-width: 768px)  {
	.buy-now{
	padding-right:45px;
	}
	}	
    @media only screen and (max-width: 767px) and (min-width: 481px)  {
	.buy-now{
	padding-right:15px;
	}
	}
    @media only screen and (max-width: 480px) and (min-width: 321px)  {
	.buy-now{
	padding-right:15px;
	}
	}	
    @media only screen and (max-width: 320px) {
	.buy-now{
	padding-right:10px;
	}
	}		
</style>
<div class="gridview">
    <a href="{{ route('gears::marketplace', [$gear->product_blurb]) }}" class="gear-img" title="">
        <img src="{{ url('images', ['gears', \App\Helper::getGearPrimaryPhoto($gear->product_id)]) }}" class="img-responsive" alt="" title="" />
    </a>
	<!--
    <h3>
        <a href="{{ route('gears::marketplace', [$gear->product_blurb]) }}" title="">
            {{ $gear->product_title }}
        </a>
    </h3>
	-->
   <!--  <div style="background-color:#6ed5d4 "> -->
    <div class="col-md-12 col-lg-12 col-xs-12" style="background-color:#6ed5d4">
	<div class="row" style="padding-top:10px; padding-bottom:5px;">
    <div class="price col-sm-8" style="font-size:12px; font-weight:bold;">
        <div class="product-price"><b>{{ $gear->product_title }}</b></div>
    </div>
	<div class="col-sm-4">
    @if( config('gp_conf.isBuyNow') )
        @if( ! Auth::check() || ($gear->created_by != Auth::user()->user_id) )
            <a style="" href="javascript:;" data-product-id="{{ \Crypt::encrypt($gear->product_id) }}" class="buy-now btn btn-white{{ is_numeric($gear->product_sale_price) && $gear->product_sale_price > 0 ? ' ' : '' }}" title="{{ $gear->product_title }}">
                Rent
            </a>
        @endif
    @else
        <a href="javascript:;" data-product-id="{{ \Crypt::encrypt($gear->product_id) }}" class="add-to-cart btn btn-white" title="{{ $gear->product_title }}">
            Add to cart
        </a>
    @endif
	</div>
	</div>
	</div>
	<!--
    <div class="col-md-12 col-lg-12 col-xs-12" style="background-color:#6ed5d4">
    <div class="price">
    @if( is_numeric($gear->product_sale_price) && $gear->product_sale_price > 0 )
        <div class="product-price hot-sale">
            <span class="cross-out">${{ $gear->product_price }}</span> <span>AUD</span>
        </div>

        <div class="sale-price">${{ $gear->product_sale_price }} <span>AUD</span></div>
        
    @else
        <div class="product-price">${{ $gear->product_price }} <span>AUD</span></div>
    @endif
    </div>
    @if( config('gp_conf.isBuyNow') )
        @if( ! Auth::check() || ($gear->created_by != Auth::user()->user_id) )
            <a href="javascript:;" data-product-id="{{ \Crypt::encrypt($gear->product_id) }}" class="buy-now btn btn-white{{ is_numeric($gear->product_sale_price) && $gear->product_sale_price > 0 ? ' button-adjust' : '' }}" title="{{ $gear->product_title }}">
                Rent
            </a>
        @endif
    @else
        <a href="javascript:;" data-product-id="{{ \Crypt::encrypt($gear->product_id) }}" class="add-to-cart btn btn-white" title="{{ $gear->product_title }}">
            Add to cart
        </a>
    @endif
	</div>
	-->
</div>