<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Forsublease API - @yield('title', 'Home')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ url('css/api/bootstrap.min.css') }}" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="{{ url('css/api/ie10-viewport-bug-workaround.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ url('css/api/jumbotron-narrow.css') }}" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="{{ url('js/api/ie-emulation-modes-warning.js') }}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="{{ elixir('js/master.js') }}"></script>
    <style type="text/css">
      .string { color: green; }
      .number { color: darkorange; }
      .boolean { color: blue; }
      .null { color: magenta; }
      .key { color: red; }

     
    </style>
  </head>

  <body>

    <div class="container">
      <div class="header clearfix">
        <h3 class="text-muted">
          <a href="{{ route('home') }}" title="">
            Forsublease
          </a>
        </h3>
      </div>

      <div class="row marketing">
        <pre>Endpoint URL : {{ url('/').'/api/v1/web-service' }}@yield('token', '')</pre>
        <hr/>
        @yield('content')
      </div>

      <footer class="footer">
        <p>&copy; 2017 Forsublease, Inc.</p>
      </footer>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{ url('js/api/ie10-viewport-bug-workaround.js') }}"></script>
  </body>
</html>
