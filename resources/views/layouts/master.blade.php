<!DOCTYPE html>
<html>
<head>
	<title>forsublease | @yield('title', "Your office just a click away")</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="icon" href="/favicon.png" type="image/x-icon" />

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="{{ elixir('css/plugins.css') }}">
	<link rel="stylesheet" href="{{ elixir('css/app.css') }}">

	<script src="//use.edgefonts.net/bebas-neue:n4:all.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC" rel="stylesheet">	
	<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC" rel="stylesheet">	
	<script type="text/javascript" src="{{ elixir('js/master.js') }}"></script>
	<script type="text/javascript">
		var baseUrl = "{{ url('/') }}";
		@if( Auth::check() )
			var pshMsg = {
				"isMessaging"       : '{{ Route::currentRouteName() == "messages::messages" ? true : false }}',
				"isGearInfo"        : '{{ Route::currentRouteName() == "gears::marketplace" ? true : false }}',
				"id"                : "{{ config('gp_conf.pusher_id') }}",
				"msg_chnl"          : "{{ config('gp_conf.pusher_msg_counter_id') }}",
				"msg_cntr_url"      : "{{ route('messages::message-counter') }}",
				"inbx_msg"          : "{{ route('messages::inbox-message') }}",
				"snd_msg"           : "{{ route('messages::send-message') }}",
				"recvr_msg"         : "{{ route('messages::received-message') }}",
				"cntct_seller"      : "{{ route('messages::contact-seller') }}",
				"snd_msg_to_seller" : "{{ route('messages::send-msg-to-seller') }}",
				"recvr_inbox"       : "{{ route('messages::inbox-senders') }}",
			};
			var pshr = {
				"mode" : "{{ env('APP_ENV') }}",
				"msgreceiver" : "{{ Auth::user()->user_id }}",
				"msgevnt" : "{{ config('gp_conf.pusher.new_message.'. strtolower(env('APP_ENV'))) }}",
				"msgcntr" : "{{ config('gp_conf.pusher.msg_counter.'. strtolower(env('APP_ENV'))) }}",
			};
		@endif
	</script>

<style>
html, body {
height: 100%;
overflow-x: hidden !important;
width: 100%;
position: fixed;
}

/*div#main-bottom .customBottom {
   background-position: 0px -550px;
   background-size:100% 100%;
}*/
.customDiv1{
    display:none;
}
</style>	
	
	<style>
.banner-item .heading-container h2 {
    font-size: 60px;
    margin: 20px 0 5px;
	font-family: bebas-neue;
	font-weight: 100;
	font-style: normal;
	letter-spacing: 4px;	
}

	.heading-title h3 {
    font-size: 35px;
    margin: 0 0 10px;
	font-family: bebas-neue;
	font-weight: 100;
	font-style: normal;
	letter-spacing: 4px;		
}


div#main-bottom {
    padding-top: 50px;
    padding-right: 0px;
    padding-bottom: 0px;
    padding-left: 0px;
}

footer#main-footer {
     background:transparent; 
    color: #fff;
    font: 14px Lato;
    height: auto;
    padding: 30px 0;
    width: 100%;
}

div#main-bottom h3 {
    font-size: 50px;
    margin-bottom: 30px;
}

div#main-bottom ul li {
    font-size: 30px;
    margin: 0;
	font-family: 'Alegreya Sans SC', sans-serif;
	letter-spacing: 1px;
}

#branding-container {
    background: transparent;
    padding-top: 100px;
    padding-right: 0px;
    padding-bottom: 100px;
    padding-left: 0px;
    text-align: center;
    width: 100%;
}

.branding-content p {
    color: #d6d6d6;
    margin: 0;
    text-align: center;
}




div#branding-container .branding-content  .social-white-icon a img{
   max-width: 40px;
   margin:10px;
}


.items-container .product-item div {
    color: #ffff;
    font-weight: 700;
}

.btn-white {
    color: #6ed5d4 !important;
    background-color: #ffffff;
    border-color: #4cae4c;
}

.header-info-container #search-header-container #search-form-header .submit-btn {
    background: #6ed5d4;
}




	div#main-bottom.customBottom {
    background-position: 0px -550px;
    }
    @media only screen and (max-width: 1366px) {
    div#main-bottom.customBottom {
    background-position: 0px -400px !important;
	background-attachment: fixed;	
    }    
    }		
    @media only screen and (max-width: 1250px) {
    div#main-bottom.customBottom {
    background-position: 0px -380px !important;
	background-attachment: fixed;		
    }    
    }	
    @media only screen and (max-width: 1200px) {
    div#main-bottom.customBottom {
    background-position: 0px -330px !important;
	background-attachment: fixed;		
    }    
    }		
    @media only screen and (max-width: 1150px) {
    div#main-bottom.customBottom {
    background-position: 0px -245px !important;
	background-attachment: fixed;		
    }    
    }		
    @media only screen and (max-width: 1024px) {
    div#main-bottom.customBottom {
    background-position: 0px -200px !important;
	background-attachment: fixed;		
    }    
    }	
    @media only screen and (max-width: 992px) {
    div#main-bottom.customBottom {
    background-position: 0px -70px !important;
	background-attachment: fixed;		
    }    
    }	
    @media only screen and (max-width: 800px) {
    div#main-bottom.customBottom {
    background-position: 0px 0px !important;
	background-attachment: fixed;	
    }    
    }	
    @media only screen and (max-width: 768px) {
    div#main-bottom.customBottom {
    background-position: 0px 50px !important;
    }    
    }	
    @media only screen and (max-width: 603px) {
    div#main-bottom.customBottom {
    background-position: 0px 0px !important;
    }    
    }	
    @media only screen and (max-width: 533px) {
    div#main-bottom.customBottom {
    background-position: 0px 0px !important;
    }    
    }  	
    @media only screen and (max-width: 480px) {
    div#main-bottom.customBottom {
    background-position: 0px 0px !important;
    }    
    }    
    @media only screen and (max-width: 320px) {
    div#main-bottom.customBottom{
    background-position: 0px 0px !important;
    }
    }
</style>

<!--Sell-->
<style>
	.panel-body{
		background-color: #ffffff !important;
	}
	.dz-default.dz-message{	
		background: #6ed5d4 !important;
	}
	input[type=checkbox]:checked, input[type=checkbox]:not(:checked){
		background: #6ed5d4 !important;
	}
	.chzn-container-single .chzn-single, .chzn-container-single .chzn-single span{
		background: #ffffff !important;
	}
	.chzn-container-single .chzn-single div b{
		background-color: #ffffff !important;
	}
	.colm1{
		border: 1px solid #ffffff;
	}
</style>
<!--Sell-->

<!--FAQ-->
<style>
	.icon-raise {
		background-image: url(/images/raise-icon.png);
		background-position: 50%;
		background-repeat: no-repeat;
		border-radius: 3px;
		padding: 30px;
		display: inline-block;
		vertical-align: top;
	}
</style>
<!--FAQ-->

<!-- register -->
<style>
    .panel-heading{
        margin-top: 30px;

    }
    .logo-thumb{
        float: right;
        display: inline;
    }
    .panel-body{
        background-color:rgba(110, 213, 212, 0.3);
    }
    .btn-sign-up{
        background-color:rgba(110, 213, 212, 1);
        border: none;
    }
    .btn-sign-up:hover{
        background-color:#3fc9c8;
        border: none;
    }
    .btn-sign-up:active{
        background-color:#3fc9c8;
        border: none;
    }
    .btn-sign-up:focus{
        background-color:#3fc9c8;
        border: none;
    }
</style>

</head>

<body style="background:#ffffff">
	<div id="main-wrapper">
		@include('layouts.includes.gp_header')
		
		<!-- BOF Body -->
		@yield('content')
		<!-- EOF Body -->

		@include('layouts.includes.gp_footer')
	</div><!-- #main-wrapper -->
	<div class="drawer drawer--right">
	  <header role="banner">
		<!-- <button type="button" class="drawer-toggle drawer-hamburger">
			<span class="sr-only">toggle navigation</span>
			<span class="drawer-hamburger-icon"></span>
		</button> -->
		<style type="text/css">
		.drawer-nav .dropdown-backdrop {
		    z-index: -999!important;
		}
		.search-bar{
			background-color: #1fb5ce;
			width: 100%;
			position: relative;
			padding: 20px 5%;
		}
		.search-bar .text-field{
			height: 43px;
			width: 85%;
			border-radius: 4px 0 0 4px;
		}
		.search-bar .submit-btn{
			position: absolute;
			top: 20px;
			width: 40px;
			height: 43px;
			right: 14px;
			border-radius: 0 4px 4px 0;
		}
		</style>
	    <nav class="drawer-nav" role="navigation">
			<ul class="drawer-menu">
				<div class="search-bar">
					{{ Form::open(['route' => 'search', 'class' => 'form-group', 'id' => 'search-form-header', 'method' => 'get']) }}
					  	<input class="form-control text-field drawer-search" type="text" name="keyword" value="{{ \Input::get('keyword') }}" placeholder="Find Office"><button class="form-control submit-btn"><i class="fa fa-search"></i></button>
					{{ Form::close() }}
				</div>
			    @if( count($categories = \App\Helper::getCategories()) > 0 )
			        @foreach($categories as $category)
			            <?php
			                $currentMenu = (Route::currentRouteName() == 'gears::gearmarketplace' && \Request::segment(2) == $category->slug);
			                $catCntr = App\Products::getTotalProducts($category->category_id);
			            ?>
			            <li class='drawer-dropdown' {!! $currentMenu ? "class='active drawer-dropdown'" : "" !!}>
			                {!! link_to_route(
			                	'gears::gearmarketplace',
			                	($category->name .' '. ($catCntr > 0 ? '('. $catCntr .')' : '')),
			                	[$category->slug, null, 'location' => \Input::get('location'), 'brand' => \Input::get('brand'), 'price_min' => \Input::get('price_min'), 'price_max' => \Input::get('price_max'), 'type' => \Input::get('type'), 'sort' => \Input::get('sort')],
			                	[ 'class' => 'drawer-dropdown-menu-item' ]
			                	
			                ) !!}
			                @if( count($category->children) > 0 )
				                <a href="#" class="btn-float-left" data-toggle="dropdown" aria-expanded="false">
				                	<span class="drawer-caret"></span>
				                </a>
			                    <ul class="drawer-dropdown-menu">
			                        @foreach($category->children as $sc)
			                            <?php $subCatCntr = App\Products::getTotalProducts($category->category_id, $sc->category_id); ?>
			                            <li class="menu-item{{ $currentMenu && \Request::segment(3) == $sc->slug ? ' active' : '' }}">
			                                {!! link_to_route('gears::gearmarketplace', ($sc->name .' '. ($subCatCntr > 0 ? '('. $subCatCntr .')' : '')), [$category->slug, $sc->slug, 'location' => \Input::get('location'), 'brand' => \Input::get('brand'), 'price_min' => \Input::get('price_min'), 'price_max' => \Input::get('price_max'), 'type' => \Input::get('type'), 'sort' => \Input::get('sort')], ['class' => 'drawer-dropdown-menu-item']) !!}
			                                <!-- <a href="{{ route('gears::gearmarketplace', [$category->slug, $sc->slug]) }}">{{ $sc->name }} {{ $subCatCntr > 0 ? '('. $subCatCntr .')' : '' }}</a> -->
			                            </li>
			                         @endforeach
			                    </ul>
			                    
			                @endif
			            </li>
			        @endforeach
			    @endif
			    <div class="nav-footer">
			    	<div class="nav-logo">
			    		<img src="{{ url('images/gearplanet-logo-mobile.png') }}" class="img-responsive" alt="Forsublease logo" title="Forsublease logo" />
			    	</div>
			    	<div class="email">
			    		<li>Email: <a href="mailto:admin@gearplanet.com.au">admin@forsublease.com.au</a></li>
			    	</div>

			    	<div class="social-media">
			    		<ul>
			    			<li class="twitter"><a href="#" target="_blank"><img src="{{ url('images/twitter-circle.png') }}" class="img-responsive" alt="twitter logo" title="Forsublease logo" /></a></li>
							<li class="facebook"><a href="#" target="_blank"><img src="{{ url('images/facebook-circle.png') }}" class="img-responsive" alt="facebook logo" title="Forsublease logo" /></a></li>
							<li class="instagram"><a href="#" target="_blank"><img src="{{ url('images/instagram-circle.png') }}" class="img-responsive" alt="instagram logo" title="Forsublease logo" /></a></li>
			    		</ul>
			    	</div>
			    </div>
			</ul>
	    </nav>
	  </header>
	</div>

	<script type="text/javascript" src="{{ elixir('js/plugins.js') }}"></script>
	<script type="text/javascript" src="{{ elixir('js/app.js') }}"></script>
	@if( Auth::check() )
		<script type="text/javascript" src="//js.pusher.com/3.1/pusher.min.js"></script>
		<script type="text/javascript" src="/js/pusher-messages.js"></script>
	@endif
	
	<script>
	    $(document).ready(function(){
	      	@if( session()->has('alert') )
	        	toastr["{{ session()->get('alert')->status ? session()->get('alert')->status : 'info' }}"]("{{ session()->get('alert')->message }}");
	      	@endif
	    });
  	</script>
</body>
</html>
