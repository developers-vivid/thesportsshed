<div class="{{ $msg->receiver_id == Auth::user()->user_id ? 'receiver' : 'sender' }}-container clearfix">
    <div class="message-container">
        <p>{!! htmlspecialchars_decode($msg->message) !!}<!--  - <small class="text-muted">{{ $msg->sender_name }} - {{ $msg->sender_email }}</small> --></p>
        <span class="created-at">
        	<small class="text-muted" title="{{ \Carbon\Carbon::parse($msg->created_at)->format('M j, Y - h:i A') }}">{{ \Carbon\Carbon::parse($msg->created_at)->format('h:i A') }}</small>
        </span>
    </div>
</div>