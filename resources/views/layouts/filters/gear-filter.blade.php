<style>
.customHeading{
	font-size:24px; 
	margin-left:-10px; 
	font-family: "Myriad Pro", Georgia, Serif;
	font-weight: normal;
}

/* Arrow */
.customSelect2::after {
  content: '\2164';
  color:#ffffff;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  padding: 0px 1em;
  background: #636363;
  pointer-events: none;
  border: 2px solid #636363 !important;    
  border-bottom-right-radius: 4px; 
  border-top-right-radius: 4px; 
}

.borderSelect1{
	  border: 2px solid #636363 !important;
	  border-radius:7px;
}

/* Transition */
.customSelect2:hover::after {
  color: #ffffff;
}
.customSelect2::after {
  -webkit-transition: .25s all ease;
  -o-transition: .25s all ease;
  transition: .25s all ease;
}
</style>
<div style="padding:10px;">
<h1 class="customHeading">Recent Listings on Forsublease</h1>
</div>
<div class="row options-container">
<style>
.customLinks li{padding:0px !important;}
.customLinks ul{padding:13px !important;}
@media only screen and (max-width: 480px) {
.customLinks{
	width:110% !important;
}
}
@media only screen and (max-width: 320px) {
.customLinks{
	width:100% !important;
}
}
</style>
<div class="col-lg-8 col-sm-7 col-xs-6 product-container customLinks">
{!! $gears->links() !!}
</div>
	<!--
    <div class="col-lg-8 col-sm-7 col-xs-6 new-pre-owned-container">
        <ul class="refined-filter-type-ul">
            <li {!! \Input::get('type') == '' || strtolower(\Input::get('type')) == 'all' ? "class='selected'" : "" !!}>
                <a href="javascript:;" class="refined-filter-type" data-value="all">All</a>
            </li>
            <li {!! \Input::has('type') && strtolower(\Input::get('type')) == 'new' ? "class='selected'" : "" !!}>
                <a href="javascript:;" class="refined-filter-type" data-value="new">New</a>
            </li>
            <li {!! \Input::has('type') && strtolower(\Input::get('type')) == 'pre-owned' ? "class='selected'" : "" !!}>
                <a href="javascript:;" class="refined-filter-type" data-value="pre-owned">Pre-Owned</a>
            </li>
        </ul>
    </div>
	-->
    <div class="{{ (! isset($hideResultsLayouts) || ! $hideResultsLayouts) ? 'show-layouts' : 'hide-layouts' }} col-lg-4 col-sm-5 col-xs-6 most-recent-view-container">
        <div class="col-lg-8 col-sm-8 col-xs-8 most-recent ">
			<div class="borderSelect1">
            <div class="custom-select customSelect2">
                {!! Form::select('sort', config('gp_conf.sort'), Input::get('sort'), ['class' => 'form-control refined-filter-sort'] ) !!}
            </div>
			</div>
        </div>
        @if( ! isset($hideResultsLayouts) || ! $hideResultsLayouts )
            <div class="col-lg-4 col-sm-4 col-xs-4 view-type-container">
                <button type="button" class="change-search-view {{ (\Session::has('searchview') && \Session::get('searchview') == 'list') ? ' selected' : '' }}" data-view="list" title="">
                    <i class="fa fa-th-list"></i>
                </button>
                <button type="button" class="change-search-view {{ (\Session::get('searchview') == '' || \Session::get('searchview') == 'grid') ? ' selected' : '' }}" data-view="grid" title="">
                    <i class="fa fa-th"></i>
                </button>
            </div>
        @endif
    </div>
</div>