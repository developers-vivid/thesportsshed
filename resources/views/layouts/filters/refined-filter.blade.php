<style>
/* Arrow */
.customSelect1::after {
  content: '\2164';
  color:#ffffff;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  padding: 7px 1em;
  background: #636363;
  pointer-events: none;
  border-bottom-right-radius: 6px; 
  border-top-right-radius: 6px; 
}
/* Transition */
.customSelect1:hover::after {
  color: #ffffff;
}
.customSelect1::after {
  -webkit-transition: .25s all ease;
  -o-transition: .25s all ease;
  transition: .25s all ease;
}
</style>
<form action="{{ isset($action) ? $action : '' }}" method="get" id="refined-filter-form" accept-charset="utf-8" style="background-color:#ebebeb; border-radius: 10px !important;  margin-top:20px;">
    <div class="product-search-container">
       
        <div class="clicker">
            <h3>Refined results</h3>
        </div>
        <div class="hide-results" style="width:80%; margin:0 auto;">
            @if( Input::has('keyword') && Input::get('keyword') != '' )
                {!! Form::hidden('keyword', (Input::has('keyword') ? Input::get('keyword') : ''), ['class' => 'form-control keyword']) !!}
            @endif
            <div class="location-container">
                <h4>Location</h4>
                <div class="select-chosen customSelect1">
                    <select class="form-control chosen refined-filter-brands" name="location">
                        <option value="all" {{ strtolower(Input::get('location')) == 'all' ? 'selected' : '' }}>All Locations</option>
                        <option value="" {{ Input::get('location') == '' || strtolower(Input::get('location')) == '' ? 'selected' : '' }}>Select Location</option>
                        <option value="nsw" {{ Input::has('location') && strtolower(Input::get('location')) == 'nsw' ? 'selected' : '' }}>NSW</option>
                        <option value="qld" {{ Input::has('location') && strtolower(Input::get('location')) == 'qld' ? 'selected' : '' }}>QLD</option>
                        <option value="wa" {{ Input::has('location') && strtolower(Input::get('location')) == 'wa' ? 'selected' : '' }}>WA</option>
                        <option value="sa" {{ Input::has('location') && strtolower(Input::get('location')) == 'sa' ? 'selected' : '' }}>SA</option>
                        <option value="tas" {{ Input::has('location') && strtolower(Input::get('location')) == 'tas' ? 'selected' : '' }}>TAS</option>
                        <option value="vic" {{ Input::has('location') && strtolower(Input::get('location')) == 'vic' ? 'selected' : '' }}>VIC</option>
                    </select>
                </div>
            </div>
            <div class="brand-container">
                <h4>Brand</h4>
                <div class="select-chosen customSelect1">
                    <select name="brand" id="brand" class="form-control chosen refined-filter-brands" name="brand">
                        <option value="all" {{ strtolower(Input::get('brand')) == 'all' ? 'selected' : '' }}>All Brands</option>
                        <option value="" {{ Input::get('brand') == '' || strtolower(Input::get('brand')) == '' ? 'selected' : '' }}>Select Brand</option>
                        @if( count($brands = \App\Products::getAllBrands()) > 0 )
                            @foreach( $brands as $brand )
                                <option value="{{ strtolower($brand->product_brand) }}"  {{ Input::has('brand') && strtolower(Input::get('brand')) == strtolower($brand->product_brand) ? 'selected' : '' }}>{{ ucfirst($brand->product_brand) }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            @if( isset($includeType) && $includeType )
                <div class="brand-container">
                    <h4>Type</h4>
                    <div class="select-chosen">
                        <select name="type" id="type" class="form-control chosen refined-filter-type" name="type">
                            <option value="all" {{ strtolower(Input::get('type')) == 'all' ? 'selected' : '' }}>All</option>
                            <option value="" {{ Input::get('type') == '' || strtolower(Input::get('type')) == 'all' ? 'selected' : '' }}>Select Type</option>
                            <option value="new" {{ Input::has('type') && strtolower(Input::get('type')) == 'new' ? 'selected' : '' }}>New</option>
                            <option value="pre-owned" {{ Input::has('type') && strtolower(Input::get('type')) == 'pre-owned' ? 'selected' : '' }}>Pre-Owned</option>
                        </select>
                    </div>
                </div>
            @endif

            @if( isset($includeSort) && $includeSort )
                <div class="brand-container">
                    <h4>Sort</h4>
                    <div class="select-chosen">
                        {!! Form::select('sort', config('gp_conf.sort'), Input::get('sort'), ['id' => 'sort', 'class' => 'form-control chosen refined-filter-sort'] ) !!}
                    </div>
                </div>
            @endif
            <div class="price-range-container">
                <h4>Price Range</h4>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12 form-group currency-field {{ $errors->has('min') ? ' has-error' : '' }}">
                        <i class="fa fa-usd" aria-hidden="true"></i>
                        {!! Form::number('price_min', Input::get('price_min'), ['class' => 'form-control', 'placeholder' => 'min' ]) !!}
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-12 form-group separator"><i class="fa fa-minus" aria-hidden="true"></i></div>
                    <div class="col-lg-4 col-md-4 col-xs-12 form-group currency-field second-currency-field{{ $errors->has('max') ? ' has-error' : '' }}">
                        <i class="fa fa-usd" aria-hidden="true"></i>
                        {!! Form::number('price_max', Input::get('price_max'), ['class' => 'form-control', 'placeholder' => 'max' ]) !!}
                    </div>
                    <div class="col-lg-3 col-md-3 col-xs-12 form-group pricebtn">
                        <button type="submit" id="btn-submit-price" class="form-control"><i class="fa fa-chevron-right"></i></button>
                    </div>
                </div>
            </div>

            @if( Route::currentRouteName() == 'mygears::ads' )
                {!! Form::hidden('status', (Input::has('status') ? Input::get('status') : 'all'), ['class' => 'form-control filter-status']) !!}
            @else
                {!! Form::hidden('type', (Input::has('type') ? Input::get('type') : ''), ['class' => 'form-control filter-type']) !!}
                {!! Form::hidden('sort', (Input::has('sort') ? Input::get('sort') : ''), ['class' => 'form-control sort-results']) !!}
            @endif
        </div>
        </div>
</form>