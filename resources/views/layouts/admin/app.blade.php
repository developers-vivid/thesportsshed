<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Forusblease - @yield('title', 'Admin Dashboard')</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="{{ elixir('css/admin-plugins.css') }}" rel="stylesheet">
    <link href="{{ elixir('css/admin.css') }}" rel="stylesheet">

    <style type="text/css">
        body {
          font-family: 'Lato';
          font-size: 13px;
        }

        .fa-btn {
          margin-right: 6px;
        }
    </style>
    <script src="{{ elixir('js/master.js') }}"></script>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ route('admin::dashboard') }}">
                    Forsublease
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                @if ( Auth::user())
                    <ul class="nav navbar-nav">
                        <li class="{{ Request::is('users') || Request::segment(1) == 'users' ? 'active' : '' }}"><a href="{{ route('admin::users') }}">Users</a></li>
                        <li class="{{ Request::is('categories') || Request::segment(1) == 'categories' ? 'active' : '' }}"><a href="{{ route('admin::categories') }}">Categories</a></li>
                        <li class=""><a href="{{ route('admin::blogs') }}">Blogs</a></li>
                    </ul>
                @endif

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if ( Auth::user())
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-user fa-fw"></i> {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li role="separator" class="divider"></li>
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')
    
    <script src="{{ elixir('js/admin-plugins.js') }}"></script>
    <script src="{{ elixir('js/admin-app.js') }}"></script>

    <script>
        $(document).ready(function(){
            @if( session()->has('alert') )
                toastr["{{ session()->get('alert')->status ? session()->get('alert')->status : 'info' }}"]("{{ session()->get('alert')->message }}");
            @endif

            if( $('body').find('[data-toggle="tooltip"]').length > 0 )
                $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</body>
</html>
