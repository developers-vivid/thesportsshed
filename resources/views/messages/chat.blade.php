@extends('layouts.master')

@section('content')

    <div id="main-container" class="single-page messages">
        <div class="container">
            <div class="row main-row">
                <div class="col-lg-12 col-sm-12 col-xs-12 chat-box-container">
                    <div class="message-area-container">
                        <div class="receiver-user">
                            <div class="receiver-user-pic-container">
                                <img class="receiver-user-pic" src="{{ url('images/thumb-pic.png') }}" alt="avatar" title="avatar" />
                            </div>
                            
                            <h3 class="receiver-user-name">{{ $userDetails->name }}<span><a href="mailto:{{ $userDetails->email }}">{{ $userDetails->email }}</a></span></h3>
                        </div>

                        <hr>
                        <div class="messages-container inbx-{{ $inboxDetails->inbox_id }}">
                            @if( count($messages) > 0 )
                                <!-- <div class="date">June 1</div> -->
                                @foreach($messages as $msg)
                                    @include('layouts.single-message')
                                @endforeach
                            @endif
                        </div>
                    </div>
                    
                    <div class="textbox-area-container">
                       <textarea id="message-textarea" class="col-lg-10 col-sm-10 col-xs-12 textarea" rows="5" placeholder="Click to Reply"></textarea><button type="button" class="col-lg-2 col-sm-2 col-xs-12 btn-default btn-primary btn-send" data-loading-text="...">SEND</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            chat.inboxId = "{{ $inboxDetails->inbox_id }}";
            chat.receiverId = "{{ $inboxDetails->receiver }}";
            chat.activeInbox = "{{ $inboxDetails->inbox_id }}";

            // do scrollBottom
            var container = document.getElementsByClassName("messages-container");
            if (container.length){
                var height = container[0].scrollHeight;
                $(".messages-container").animate({ scrollTop: height }, "fast");
            }
        });
    </script>
  
@endsection