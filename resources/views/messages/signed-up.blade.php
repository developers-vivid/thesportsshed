@extends('layouts.404.page_not_found_master')

@section('title', 'Successful Sign Up')

@section('content-404')
    
    <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-8 col-md-offset-2 col-xs-12 page-not-found-container">
            <div class="logo-container">
                <a href="{{ route('home') }}">
                   <img src="{{ url('images/splash/splash-logo.png') }}" class="img-responsive" alt="" title="" />
                    <h1 class="slogan">Your Office Just A Click Away</h1>
                </a>
            </div>
            <div class="alert alert-danger text-center">
                <br/>
                <h3>
                    <p><strong style="font-weight: bold;">{{ \Session::get('alert')->message }}</strong></p>
                </h3>
                <a href="{{ route('home') }}" class="homepage-link" >Go To Homepage</a>
                <br/><br/>
            </div>
          </div>
        </div>
    </div>

@endsection