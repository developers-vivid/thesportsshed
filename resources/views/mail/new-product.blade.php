@extends('mail.layout')

@section('mail-content')
	<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #dbdbdb;border-collapse:separate;box-shadow: 0px 0 10px #B5B5B5;border-radius: 10px; margin: 0 auto 0 auto;">
		<tr>
			<td style="background-color:#4ab0cf;padding: 0 0 0 0; text-align:center; font-size: 29px;color:#fff;padding:40px 10px;border-radius: 6px 6px 0 0;font-family:tahoma,sans-serif;">
				New Product Added
			</td>
		</tr>
		<tr>
			<td style="background-color:#fff;padding: 0 0 0 0; text-align:center;color:#7e7e7e;padding:45px 20px;border-radius: 0 0 6px 6px;font-family:tahoma,sans-serif;text-align:left;">
				<p style="font-size:17px;line-height: 23px;padding:0;margin:0 0 30px 0;">Hi {{ $ss->user_name }},</p>
				<p style="font-size:17px;line-height: 23px;padding:0;margin:0 0 30px 0;">Here are the list of new product added that matched your saved search.</p><br/>

				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #dbdbdb;border-collapse:separate;box-shadow: 0px 0 10px #B5B5B5;border-radius: 10px; margin: 0 auto 0 auto;">
					<tbody>
						<tr>
							<th>Product Photo</th>
							<td>:</td>
							<td>
								<img src="{{ url('/images/gears/thumbs/', [$product->product_id]) }}" alt="{{ $product->product_title }}">
							</td>
						</tr>
						<tr>
							<th>Product ID</th>
							<td>:</td>
							<td>
								<a href="{{ route('gears::marketplace', [$product->product_blurb]) }}" title="{{ $product->product_title }}">{{ $product->product_id }}</a>
							</td>
						</tr>
						<tr>
							<th>Product Title</th>
							<td>:</td>
							<td>
								<a href="{{ route('gears::marketplace', [$product->product_blurb]) }}" title="{{ $product->product_title }}">{{ $product->product_title }}</a>
							</td>
						</tr>
						<tr>
							<th>Product Price</th>
							<td>:</td>
							<td>{!! $product->product_sale_price != '0.00' ? '<span style="text-decoration: line-through;">'. $product->product_sale_price .'</span>' : '' !!} <span>{{ $product->product_price }}</span></td>
						</tr>
						<tr>
							<th>Date Created</th>
							<td>:</td>
							<td>{{ $product->created_at }}</td>
						</tr>
						<tr>
							<th>Product Owner</th>
							<td>:</td>
							<td>{{ $product->owner }} <{{ $product->owner_email }}></td>
						</tr>
					</tbody>
				</table>

				<p style="font-size:17px;line-height: 23px;padding:0;margin:0 0 30px 0;"><span style="font-style:italic;">Thanks</span>,<br/><span style="font-weight:600;">Forsublease Team</span></p>
			</td>
		</tr>
	</table>
@endsection