@extends('mail.layout')

@section('mail-content')
	<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: 0px solid #dbdbdb;border-collapse:separate;box-shadow: 0px 0 10px #B5B5B5;border-radius: 10px; margin: 0 auto 0 auto;">
		<tr>
			<tr>
				<td style="background-color:#4ab0cf;padding: 0 0 0 0; text-align:center; font-size: 25px;color:#fff;padding:40px 10px;border-radius: 6px 6px 0 0;font-family:tahoma,sans-serif;">
					You Have unread messages from {{ $senderDetails->first_name }}
				</td>
			</tr>
		</tr>
		<tr>
			<td style="background-color:#fff;text-align:center;color:#7e7e7e;padding:0 20px 45px 20px;border-radius: 0 0 6px 6px;font-family:tahoma,sans-serif;text-align:left;">
				<p style="font-size:17px;line-height: 23px;padding:0;margin:40px 0 25px 0;">{{ $senderDetails->name }}</p>

				<p style="font-size:17px;line-height: 23px;padding:0;margin:0 0 10px 0;">
					{!! htmlspecialchars_decode( $theMsg ) !!}
				</p>
				
				<p style="font-size:17px;line-height: 23px;padding:0;margin:0 0 30px 0;">Cheers,<br/><span style="font-weight:600;">{{ $senderDetails->name }}</span></p>
				<p><a style="text-decoration: none;border: 1px solid #4ab0cf;background-color: #4ab0cf;padding: 6px;color: #fff;" target="_blank" href="{{ route('messages::channelMessages', [$inboxDetails->channel_id]) }}">Click To Reply</a></p>
			</td>
		</tr>
	</table>
@endsection