<body style="margin: 0; padding: 0; background-color: #46adcc;">
	<table bgcolor="#46adcc" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 95px 0 80px 0; backround-color: red;">	
		<tr>
			<td style="padding: 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="622" style="border: 0px solid #dbdbdb; border-collapse: collapse;">
					
					<tr>
						<td bgcolor="#ffffff" style="padding:0 57px;background-color:#f6f6f6; border-radius: 6px 6px 0 0;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="padding: 0 0 0 0; text-align:center;padding: 33px 0 28px 0">
										<img src="{{ url('/images/gear-planet-logo.png') }}">
									</td>
								</tr>

								<tr>
									<td>
										@yield('mail-content')
									</td>
								</tr>
								<tr>
									<td style="text-align:center">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td>
													<p style="text-align:center;font-size:18px;padding:0 0 15px 0;margin:30px 0 0 0;color:#616161;font-weight:bold;font-family:tahoma,sans-serif;">Stay in touch</p>
												</td>
											</tr>
											<tr>
												<td>
													<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<tr>
															<td style="text-align:right;">
																<a href="https://www.twitter.com/gearplanetaus">
																	<img src="{{ url('/images/twitter.png') }}" style="padding: 0 5px 0 0;">
																</a>
															</td>
															<td style="text-align:center;width:29px;">
																<a href="https://www.facebook.com/gearplanetaus">
																	<img src="{{ url('/images/fb.png') }}">
																</a>
															</td>
															<td style="text-align:left;">
																<a href="https://www.instagram.com/gearplanet">
																	<img src="{{ url('/images/instagram.png') }}" style="padding: 0 0 0 5px;">
																</a>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style="padding:30px 0 25px 0;text-align:center;">
										<a href="http://www.sushidigital.com.au/">
											<img src="{{ url('/images/sushilogo.png') }}" style="margin: 0 auto;">
										</a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>