@extends('layouts.splash_master')
@section('content-splash')
    <div id="main-container" class="splash">
        <div class="container">
            <div class="row logo-container">
                <div class="col-lg-12">
                    <a href="{{ route('home') }}">
                        <img src="{{ url('images/splash/splash-logo.png') }}" class="img-responsive" alt="" title="" />
                        <h1 class="slogan">Australia’s Music Marketplace</h1>
                    </a>
                </div>
            </div>
            <div class="row content-container">
                <h2>Coming Soon</h2>
                <h3>Sign up below for more info</h3>
                {!! Form::open(['url' => route('newsletter::subscribe'), 'role' => 'form', 'id' => 'search-form']) !!}
                    {!! Form::text('email', old('email'), ['class' => 'form-control text-field subscribe-email', 'placeholder' => 'Your email']) !!}{!! Form::submit('Submit', ['class' => 'form-control submit-btn subscribe-btn']) !!}
                {!! Form::close() !!}
                <ul>
                    <li class="facebook"><a href="https://www.facebook.com/gearplanetaus" target="_blank"><i class="fa fa-facebook-square"></i> Share</a><span class="num-of-share">38</span></li>
                    <li class="twitter"><a href="https://www.twitter.com/" target="_blank"><i class="fa fa-twitter"></i> Tweet</a></li>
                    <li class="google-plus"><a href="https://www.plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a><span class="num-of-share">38</span></li>
                </ul>
            </div>
        </div><!-- .container -->
    </div><!-- #main-container -->
    <div id="branding-container" class="splash">
        <div class="container">
            <div class="row">
                <div class="branding-content">
                    <h1>Buy & Sell music gear Australia wide</h1>
                </div>
            </div>
        </div><!-- /.container -->
    </div><!-- /#branding-container -->
@endsection
