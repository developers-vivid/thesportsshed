@extends('layouts.master')

@section('title', 'Terms and Conditions')

@section('content')
	
<div id="main-container" class="single-page about-us">
        <div class="banner-container">
                <div class="col-lg-12 col-md-12 col-xs-12 banner-item">
                        <div class="heading-container">
                        <h2>YOUR OFFICE JUST A CLICK AWAY</h2>
                        </div>
                    <div class="img-container" style="background: url({{ url('images/homepage_banner1.jpg') }}) 0 0 no-repeat; background-size: cover; background-position: center;"></div>
                </div>
        </div>
      <div class="container">
          <div class="row">
              <div class="aboutus-content">
                <div class="header-title col-lg-12 col-sm-12 col-xs-12">
                  <h1>Terms and Conditions</h1>
                </div>
                <div class="section">
                  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                  <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
                  <p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere</p>
                </div>
              </div>
          </div>
      </div>
  </div>

@endsection
