@extends('layouts.master')

@section('title', "FAQS")

@section('content')
   	
    <div id="main-container" class="single-page faqs">
        <div class="col-lg-12 col-md-12 col-xs-12 banner-item">
            <div class="heading-container">
                <h2>YOUR OFFICE JUST A CLICK AWAY</h2>
            </div>
            <div class="img-container" style="background: url({{ url('images/homepage_banner1.jpg') }}) 0 0 no-repeat; background-size: cover; background-position: center;"></div>
        </div>
        <div class="container">
            <div class="row main-row">
                <div class="faqs-content clearfix" style="margin-top: 550px;">
                	<div class="col-lg-12 col-md-12 col-xs-12 header-title">
                		<h1>FAQ’s</h1>	
                	</div>
                	<div class="col-lg-6 col-sm-6 col-xs-12 clearfix">
                		<div class="section">
                            <!--<div class="icon-comment"></div>-->
                            <div class="icon-raise"></div>
		                	<div class="content">
		                		<h3 class="faq-title">What is Forsublease?</h3>
		                		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                <br/>
		                	</div>
                		</div>

                		<div class="section">
                			<!--<div class="icon-comment"></div>-->
                            <div class="icon-raise"></div>
		                	<div class="content">
		                		<h3 class="faq-title">Is there a Forsublease app?</h3>
		                		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
		                	</div>
                		</div>

                		<div class="section">
                			<!--<div class="icon-comment"></div>-->
                            <div class="icon-raise"></div>
		                	<div class="content">
		                		<h3 class="faq-title">Does it cost anything to sell on GearPlanet.com.au?</h3>
		                		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
		                	</div>
                		</div>

                        <div class="section">
                            <!--<div class="icon-comment"></div>-->
                            <div class="icon-raise"></div>
                            <div class="content">
                                <h3 class="faq-title">How do I pay for my items?</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>

                        <div class="section">
                            <!--<div class="icon-comment"></div>-->
                            <div class="icon-raise"></div>
                            <div class="content">
                                <h3 class="faq-title">How do I ship my item, and ensure my item arrives safely?</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>

                        <div class="section">
                            <!--<div class="icon-comment"></div>-->
                            <div class="icon-raise"></div>
                            <div class="content">
                                <h3 class="faq-title">I’m a store and/or want to list bulk items, how can I?</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>

	                </div>

	                <div class="col-lg-6 col-sm-6 col-xs-12 clearfix">
                	
                        <div class="section">
                            <!--<div class="icon-comment"></div>-->
                            <div class="icon-raise"></div>
                            <div class="content">
                                <h3 class="faq-title">Can I trade stuff on Forsublease?</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>

                        <div class="section">
                            <!--<div class="icon-comment"></div>-->
                            <div class="icon-raise"></div>
                            <div class="content">
                                <h3 class="faq-title">How am I covered a buyer/seller while using Forsublease?</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>

                        <div class="section">
                            <!--<div class="icon-comment"></div>-->
                            <div class="icon-raise"></div>
                            <div class="content">
                                <h3 class="faq-title">I’m not happy with something I bought on Forsublease, what do I do?</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>

                        <div class="section">
                            <!--<div class="icon-comment"></div>-->
                            <div class="icon-raise"></div>
                            <div class="content">
                                <h3 class="faq-title">I have a problem with one of my transactions, can Forsublease help?</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>

                    </div>
                    <!--<div class="col-lg-12">
                        <img src="{{ url('/images/Selling-page-bar-chart-gp.jpg') }}" alt="Bar Chart" title="Bar Chart" class="img-responsive img-faq" width="500">
                    </div>-->
	                
                </div>
            </div>
        </div>
    </div>

@endsection