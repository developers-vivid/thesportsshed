@extends('layouts.master')

@section('title', "Blogs")

@section('content')
   	
    <div id="main-container" class="single-page blogs">
        <div class="container">
            <div class="row main-row">
                <div class="blog" style="margin-top: 20px;">
                    @if( $blogs->count() > 0 )
                        @foreach($blogs as $blog)
                            <div class="blog-section col-lg-4 col-sm-6 col-xs-12" style="padding-bottom: 80px;">
                                <div class="image-container">
                                    <img src="{{ url('/images/blogs/thumbs/', [$blog->photo]) }}">
                                </div>
                                <div class="blog-header">
                                    <h3>{{ str_limit($blog->title, 50) }}</h3>
                                    <div class="blog-writer">
                                        <div class="writer inline-block">
                                            <p>By: <span>{{ $blog->author }}</span></p>
                                        </div>
                                        <div class="date inline-block">
                                            <p>{{ \Carbon\Carbon::parse($blog->created_at)->format('F j, Y') }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="blog-body">
                                    <p>{!! $blog->blog_content !!}</p>
                                    <div class="button-link">
                                        <a href="#">
                                            <div class="read-more">
                                                Read More
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="alert alert-danger text-center">
                            <strong>Error!</strong> No blog(s) yet
                        </div>
                    @endif

                </div>

            </div>
        </div>
    </div>
@endsection