@extends('layouts.master')

@section('title', "Why Forsublease")

@section('content')

<style>
    @media only screen and (max-width: 360px){
    .customPageIconsText{
    position:relative;
	left:20px;
    }
	}
    @media only screen and (max-width: 320px){
    .customPageIconsText{
    position:relative;
	left:15px;
    }
	}
</style>

    
    <div id="main-container" class="single-page why-use-gp">
        <div class="col-lg-12 col-md-12 col-xs-12 banner-item">
                <div class="heading-container">
                <h2 class="customPageIconsText2">YOUR OFFICE JUST A CLICK AWAY</h2>
                </div>
            <div class="img-container" style="background: url({{ url('images/homepage_banner1.jpg') }}) 0 0 no-repeat; background-size: cover; background-position: center;"></div>
        </div>
        <div class="container">
          <div class="row">
              <div class="gp-content">
              <div class="container" >
              <div id="test-sana" class="col-lg-12 col-sm-12 col-xs-12 heading-title text-center" style="margin-top: 3em;">
                  <h3 class="customPageIconsText">ADVERTISE YOUR AVAILABLE SPACE TODAY</h3>
              </div>
          </div>

          <div class="page-icons  text-center">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
              <div class="icon-img">
                <img src="{{ url('/images/icons/simple_registration.png') }}" alt="Buy and sell music gear!" title="Buy and sell music gear!"/>
              </div>
              <span class="icon-title">1. Simple Registration</span>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
              <div class="icon-img">
                <img src="{{ url('/images/icons/ads_space.png') }}" alt="Free to join list items" title="Free to join list items"/>
              </div>
              <span class="icon-title">2. Advertise your Space</span>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
              <div class="icon-img">
                <img src="{{ url('/images/icons/rec_enq.png') }}" alt="Great deals on new &amp; used gear" title="Great deals on new &amp; used gear"/>
              </div>
              <span class="icon-title">3. Receive enquiries!</span>
            </div>
            <div class="clearfix"></div>
          </div>

                    <div class="header-title col-lg-12" style="margin-top:100px; margin-bottom:100px;">
                      <h1>Why Forsublease?</h1>
                    </div>

                    <div class="section clearfix">

                      <div class="list col-lg-6 col-sm-6 col-xs-12">
                        <ul>
                          <li>
                            <h3>We are Australian owned and operated</h3>
                            <p>Thats right, we’re based in Mt Lawley, Perth and we care about local business and local musicians. It also means <i>we’re on your timezone</i>. We will reply to all emails promptly and talk to you in friendly laid back manner - no sales pitches or scripts.</p>
                          </li>
                          <li>
                            <h3>We’re Muso’s too!</h3>
                            <p>We have both toured and recorded professionally as musicians. What’s more, we suffer from GAS as much as just about anybody else I know. Why is this important? It means that we get you, we’re one of you and we’re here to help you. We want a site that works for us, which in turns means it will work for you.</p>
                          </li>
                         <li>
                           <h3>Forusblease is a small local business</h3>
                           <p>Thats right, its just me (Curtis) and my buddy Shaun. That’s it. We’re not a faceless corporation, and we won’t swamp your queries mysterious language and take 2 weeks to reply. If you call or email, you will reach one of us. We think its important to keep the website personal as it helps to keep the community vibe. Feel free to reach out to us - whether its to tell us about your awesome buy or whether you have an idea for how we can improve the site.</p>
                         </li>
                        </ul>
                      </div>

                      <div class="list col-lg-6 col-sm-6 col-xs-12">
                        <ul>
                         <li>
                           <h3>Safe Transactions</h3>
                           <p>We have fully integrated PayPal and eWay into the site. That means that you can place your transactions safely and (in the case of Paypal) have a great way to raise and settle any dispute you may have with a buyer/seller.</p>
                         </li>
                         <li>
                            <h3>Great Advice</h3>
                            <p>We’ve spent a lot of time writing blogs on the best way to package your items, the best services to use for delivery, the best way to photograph your items etc in a bid to make everything as easy and stress free as possible. You can find links to those articles <a href="{{ route('pages::blogs') }}">here</a>. If you need advice on something and cant find it in that section - shoot us an email, we’ll do our best to help.</p>
                         </li>
                         <li>
                            <h3>Low fees</h3>
                            <p>We only charge 3.5% per transaction - and that’s only IF your item sells. As a comparison - eBay charges at around 9.9% on all transactions. Thats HALF the cost for TWICE the service. Find out more about selling <a href="#">here</a>!</p>
                         </li>
                        </ul>
                      </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection