@extends('layouts.master')

@section('title', 'About Us')

@section('content')
	
<div id="main-container" class="single-page about-us">
            <div class="banner-container">
                <div class="col-lg-12 col-md-12 col-xs-12 banner-item">
                        <div class="heading-container">
                        <h2>YOUR OFFICE JUST A CLICK AWAY</h2>
                        </div>
                    <div class="img-container" style="background: url({{ url('images/homepage_banner1.jpg') }}) 0 0 no-repeat; background-size: cover; background-position: center;"></div>
                    </div>
                </div>
      <div class="container">
          <div class="row">
              <div class="aboutus-content">


                <div class="header-title col-lg-12 col-sm-12 col-xs-12">
                  <h1>About Forsublease</h1>
                </div>
                <div class="section">
                  <p>Here at Forsublease, our goal is simple. We want buying and selling our music space to be as awesome as possible. Forsublease is a two man operation run by Curtis McEntee & Shaun McIlroy and is based in Perth, Western Australia. Our story is a familiar one - we have both played in local bands and gigged around our local music scene. We both love music, both playing live and recording.</p>
                  <p>As for a lot of us, that in turn means we love buying gear. And selling gear. And then buying more gear - it’s a great, never ending cycle! With the re-sale of second hand music gear being so good in general, one can buy and trade without spending a lot of money at all, further fuelling the GAS.</p>
                  <p>The only thing is that we always found searching for ‘the good stuff ’ a bit laborious, and existing websites either want to charge you up to 10%+ of your item’s sale price, or are cluttered with non musical items. We never liked finding the piece of gear we had wanted only to realise that the seller wouldn’t ship to Australia, or having to have an odd stranger come to one of our homes for a local pickup.</p>
                  <p>So we decided to do something about it. We decided to create a slick, responsive website with a matching app for both iOS and Android that was built specifically for the buying and selling of musical equipment. We wanted the website to feature the same things as all of you; safe payment portals, personalised news feeds, optimised shipping options and awesome search filters. And we did it!</p>
                  <p>Welcome to  Forsublease - Australia’s Lease.</p>
                  <p><img src="{{ url('/images/about_us_logo.png') }}"> </p>

                  <br/>
                  <h3 class="text-center">Contact Us</h3>
                  <p>Questions, Comments or Want to Join our team? We'd love to hear from you!<br/>
                  Get in touch at contact@spaceplanet.com.au<br/>
                  We can also be found at The Rock Inn during the "regular work day"<BR/>
                  762 Beaufort St Mount Lawley 6050 Western Australia</p>
                </div>
              </div>
          </div>
      </div>
  </div>

@endsection
