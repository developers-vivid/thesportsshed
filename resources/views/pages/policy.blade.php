@extends('layouts.master')

@section('title', 'Policy')

@section('content')
	
<div id="main-container" class="single-page policy">
        <div class="banner-container">
                <div class="col-lg-12 col-md-12 col-xs-12 banner-item">
                        <div class="heading-container">
                        <h2>YOUR OFFICE JUST A CLICK AWAY</h2>
                        </div>
                    <div class="img-container" style="background: url({{ url('images/homepage_banner1.jpg') }}) 0 0 no-repeat; background-size: cover; background-position: center;"></div>
                </div>
        </div>
      <div class="container">
          <div class="row">
              <div class="aboutus-content" >

                <div class="header-title col-lg-12 col-sm-12 col-xs-12">
                  <h1>Policy</h1>
                </div>
                <div class="section col-lg-12 col-sm-12 col-xs-12">
                  <p><strong>Forsublease Terms and Conditions</strong></p>

                  <p>Please read these terms of use carefully before using the website and services offered by Forsublease.com ABN MOO (  X)0000( (Forsublease). These terms and conditions set out the legally binding agreement between Forsublease (us, we, our) and the user (user, you, your) in relation to your use of the website at<a href="http://www.Forsublease.com.au" alt="Forsublease" title="Forsublease">http://www.Forsublease.com.au</a> (the Site) and all services provided by Forsublease on the Site (Services).</p>

                  <p>By using the Site or Services in any manner, you agree to be bound by these terms and conditions and any additional terms and conditions and policies referred to here and/or available by hyperlink.<br/>
                  These terms and conditions apply to all users, including but not limited to sellers, customers and contributors of content, information and other materials or services on the Site. </p>

                  <p>1. Forsublease is a Venue</p>

                  <p>Forsublease acts as a venue to allow users who comply with Forsublease's policies to offer, sell and buy certain goods. Forsublease is not directly involved in the transaction between buyers and sellers and does not transfer legal ownership between users. </p>

                  <p>Forsublease has no control over the quality, safety, morality or legality of any aspect of the items listed, the truth or accuracy of the listings, the ability of sellers to sell items or the ability of buyers to pay for items. <br/>
                  Forsublease does not pre-screen users or the content or information provided by users. Forsublease cannot guarantee the true identity, age, and nationality of a user or that a buyer or seller will actually complete a transaction. <br/>
                  You agree that Forsublease is not responsible or liable for any content, for example, data, text, information, usernames, graphics, images, photographs, profiles, audio, video, items, and lin. posted by you, other users, or outside parties on Forsublease. You use the Forsublease service at your own risk. </p>

                  <p>2. Membership</p>

                  <p>Age: You must be 18 years or older to use the Site. All registration information you submit must be accurate and truthful. If you are under the age of 18, you may only use the Services under the supervision of a parent or legal guardian who is at least 18 years of age. In such cases, the adult is the user and is responsible for any and all activities.</p>

                  <p>Compliance: You agree to comply with all local laws regarding online conduct and acceptable content. You are responsible for all applicable taxes. You agree to comply with all Forusblease's policies, operating rules and procedures as updated from time to time.</p>

                  <p>Password: Keep your password secure. You are responsible for all activity, liability and damage resulting from your failure to keep your password secret. You agree to immediately notify Forusblease of any unauthorised use of your password or any breach of security. You agree not to provide your username and password information in combination to any other party other than Forusblease without Forusblease's express written permission.</p>

                  <p>Account Information: You must keep your account information up-to-date and accurate at all times, including a valid email address. To sell items on Forusblease you must provide and maintain valid payment information such as valid credit card information or a valid PayPal account.</p> 

                  <p>Account Transfer: You may not transfer or sell your Forusblease account or User ID to another party. If you are registering as a business entity, you must have the authority to bind the entity to this Agreement.</p> 

                  <p>Right to Refuse Service: Forusblease's services are not available to temporarily or indefinitely suspended Forusblease members. Forusblease may cancel unconfirmed or inactive accounts. Forusblease reserves the right to refuse service to anyone, for any reason, at any time and change its eligibility criteria at any time.</p> 

                  <p>Please use caution, common sense, and practice safe buying and selling when using Forusblease. Forusblease does not control the Content provided by users that is made available on Forusblease. You may find some Content to be offensive, harmful, inaccurate, or deceptive. There are also risks of dealing with underage persons or people acting under false pretence or dealing with international trade and foreign nationals.</p>

                  <p>3. Fees and Services (Fees Policy)</p>

                  <p>Joining and setting up a shop on Forsublease is f ree. Forsublease does not charge fees to list an item for sale. However, Forsublease charges a percentage of the sale price when the item sells. Unless otherwise stated, all fees are quoted in Australian Dollars (AUD). Fees will include final sale price of an item as well as shipping fees on an order. You are responsible for paying all fees and applicable taxes associated with using Forsublease. Forsublease keeps accepted payment information on file.<br/>
                  Sometimes Forsublease's fees may change. Forsublease will give you notice of changes to the Fees Policy and the fees for Forsublease's services by posting the changes on the Site. For new services, the fees for that service are effective at the launch of the service.<br/>
                  Forsublease may temporarily change the fees for promotional events (for example, free listing days), effective when Forsublease posts the temporary promotional event on the Site.</p>

                  <p>Individual payment providers may charge a fee to process a transaction in addition to the fees charged by Forsublease. Information about such fees may be available on your payment provider's website. </p>

                  <p>At the beginning of the month, each seller is emailed an invoice detailing the amount due. A seller must pay the amount due in full within 15 days of the date of the invoice.</p>

                  <p>Our fees are non-refundable and you must pay all unpaid fees on your account and any applicable penalties when they are due, including if: <br/>
                  a) Forusblease terminates a listing or your account; b) you close your account; c) the payment of your Forusblease fees cannot be completed for any reason.</p>

                  <p>If you do not pay by the due date, we may suspend or terminate your account or limit your access to the Services.  We may also pursue payment by other collection mechanisms (including retaining collection agencies and legal counsel).</p>

                  <p>In certain situations, including but not limited to a void or invalid transaction and returns, we may issue a credit for the applicable fees to a seller's billing statement.</p>

                  <p>If you have a question or wish to dispute a charge, contact Forsublease.</p>

                  <p>4. Listing and Selling Listing Description:</p>

                  <p>By listing an item on the Site you warrant that:<br/>
                      a) you and all aspects of the item comply with Forsublease's policies; b) you may legally sell the item; and c) you have accurately described your item and all terms of sale in your Forsublease shop.</p>

                  <p>Your listings must:</p>
                  <ul>
                    <li>only include text descriptions, graphics, pictures and other content relevant to the sale of that item that you have supplied.</li>
                    <li>not include links or reference to other selling venues, external listings, websites or selling channels, phone numbers or email addresses directly in listings, shop banners, or shop policies.</li>
                    <li>be listed in an appropriate category with appropriate tags.</li>
                    <li>accurately and completely describe the item/items for sale in that listing; and.</li>
                    <li>ensure each unique item must have its own listing.</li>
                  </ul>

                  <p>Shop Policies: If you are a seller, we recommend you create and outline shop policies for your Forsublease shop (Seller Policies). Seller Policies may include, for example. shipping, returns, payment and selling policies. Seller Policies must be reasonable and must comply with Forsublease's site-wide policies. Sellers must comply with Seller Policies and are responsible for enforcing their own Seller Policies.</p>

                  <p>Forsublease reserves the right to request that a seller modify a Seller Policy. </p>

                  <p>Binding Sale: All sales through Forsublease are binding. The seller must ship a completed order or otherwise complete the transaction with the buyer in a prompt manner, unless: </p>
                  <ol type="a" class="ol-l-alpha">
                    <li>the buyer fails to meet the terms of the seller's listing (such as payment method); or</li>
                    <li>the seller cannot authenticate the buyer's identity; or</li>
                    <li>other exceptional circumstances apply (at Forusblease’s discretion).</li>
                  </ol>

                  <p>The buyer must deliver appropriate payment for items purchased, unless there is an exceptional circumstance (as determined by Forsublease). By purchasing an item on Forsublease, you agree to the specific return and other policies outlined in the applicable Seller Policy. </p>

                  <p>Fee Avoidance: The price stated in each item listing description must be an accurate representation of the sale. Sellers may charge reasonable shipping and handling fees to cover the costs for packaging and mailing the items. You may not misrepresent the item's location. No sales may be carried out via private message as a means to avoid Forsublease's sales fee. Any other efforts to move sales initiated on Forsublease off-site to avoid fees is strictly prohibited and may result in account suspension or termination. </p>

                  <p>5. Prohibited and Restricted Items and Activities</p>

                  <p>You are responsible for your conduct and activities on Forsublease including all content you submit, post, and display on Forsublease.Your content and your use of Forusblease must not:</p>
                  <ol type="a" class="ol-l-alpha">
                    <li>be false, inaccurate or misleading;</li>
                    <li>be fraudulent or involve the sale of illegal, counterfeit or stolen items;</li> 
                    <li>infringe upon any third-party's copyright, patent, trademark, trade secret or other proprietary or intellectual property rights;</li> 
                    <li>violate this Agreement, any Site policy or community guidelines, or any applicable laws or regulations;</li> 
                    <li>contain items that have been identified as hazardous to consumers or are subject to a recall;</li> 
                    <li>include any defamatory, threatening, abusive, defamatory or obscene material;</li>
                    <li>impersonate or intimidate any person (including Forsublease staff or other users), or falsely misrepresent your affiliation with any person, for example, the use of similar email address, nicknames, or creation of false account(s);</li>
                    <li>distribute or send communications that contain spam;</li> 
                    <li>distribute viruses or any other technologies that may harm Forsublease, the Services or the interests or property of Forsublease or other users; </li>
                    <li>harvest or otherwise collect information about other users, including email addresses, without their consent;</li>
                    <li>copy, modify or distribute any other person’s content without their consent;</li>
                    <li>host images not part of a listing;</li> 
                    <li> modify, adapt or hack Forsublease or another website so as to falsely imply that it is associated with Forsublease;</li>
                    <li>use any robot spider, scraper or other automated means to access the Services and collect content for any purpose without our express written permission;</li>
                    <li>create or appear to create liability for Forsublease or cause Forsublease to lose (in whole or in part) the services of Forsublease's ISPs or other suppliers; </li>
                    <li>impose an unreasonable load on our infrastructure or interfere with the proper working of the Services;</li>
                    <li>copy, reproduce, modify, distribute or publicly display another user’s content without their consent;</li> 
                    <li>link directly or indirectly, reference or contain descriptions of goods or services that are prohibited under this Agreement, Forsublease's Privacy Policy or other documents posted on Forsublease; or </li>
                    <li> cause Forsublease to violate any applicable law, statute, ordinance or regulation these terms including by listing any item on Forsublease , through any transaction initiated or completed using Forsublease's Service or by paying to Forsublease any fee for the Services. </li>
                  </ol>
                  
                  <p>6. Content License:</p>

                  <p>Forsublease does not claim ownership rights in your Content. By using the Services, you grant Forsublease a non-exclusive, worldwide, perpetual, irrevocable, royalty-free, sublicensable right to use your Content or information you provide to Forsublease, exercise the copyright, publicity, and database rights (but no other rights) you have in the Content and allow Forsublease to store, re-format and display your Content on Forsublease in any way as Forsublease chooses. </p>

                  <p>Forsublease will only use personal information in accordance with Forsublease's Privacy Policy. Forsublease can also use any transactional information including sale date and final price for sales completed on Forsublease as it sees fit. </p>

                  <p>As part of a transaction, you may obtain personal information, including email address and shipping information, from another Forsublease user. You must not: </p>
                  <ol type="a" class="ol-l-alpha">
                    <li>use personal information about another user for a purpose other than completing the transaction without obtaining prior permission from the other user;</li>
                    <li>send unsolicited commercial messages;</li>
                    <li>add any Forsublease user to your email or physical mail list without that user's express permission.</li>
                  </ol>

                  <p>By posting Content on Forsublease, it is possible for an outside website or a third party to re-post that Content. You agree to hold Forsublease harmless for any dispute concerning this use. If you choose to display your own Forsublease-hosted image on another website, the image must provide a link back to its listing page on Forsublease. </p>

                  <p>Forsublease does not endorse and is not responsible or liable for any content, advertising, products, or other materials on or available from outside websites or resources linked to or referenced on the Site and accepts no liability for any damage or loss caused or alleged to be caused by or in connection with the use of or reliance on any content, goods or services available on or through any such websites or resources. </p>

                  <p>8. Resolution of Disputes and Release</p>

                  <p>In the event a dispute arises between you and Forsublease, please contact Forsublease. In any dispute arising from or relating to the subject matter of this Agreement, the parties agree to use best endeavours to resolve the dispute in good faith. Nothing in these terms and conditions prevents a party from seeking urgent or injunctive relief. </p>

                  <p>You agree that any cause of action arising out of or related to the Site (including, but not limited to, any services provided by Forsublease) or this Agreement must commence within one (1) year after the cause of action arose. </p>

                  <p>If you have a dispute with one or more users, or a third party, you release and indemnify Forsublease (and Forsublease's officers, directors, agents, subsidiaries, joint ventures and employees) from any and all claims, demands and damages (actual and consequential), arising out of or in any way connected with such disputes. </p>

                  <p>You acknowledge that, while Forsublease is not liable in any dispute between you and a third party, Forsublease, for the benefit of users and at its sole discretion, may try to help users resolve disputes. Forsublease does so in Forsublease's sole discretion. Forsublease has no obligation to resolve disputes between users or between users and third parties. To the extent that Forsublease attempts to resolve a dispute. Forsublease will do so in good faith based solely on Forsublease's policies. Forsublease will not make judgments regarding legal issues or claims. </p>

                  <p>9. Forsublease's Intellectual Property</p>

                  <p>Forsublease, and other Forsublease graphics, logos, designs, page headers, button icons, scripts, and service names are registered trademarks, trademarks or trade dress of Forsublease, Inc. in Australia and/or other countries. Forsublease's trademar. and trade dress may not be used, including as part of trademarks and/or as part of domain names or email addresses, in connection with any product or service in a, manner that is likely to cause confusion. </p>

                  <p>11. Breach</p>

                  <p>If we:</p>
                  <ol type="a" class="ol-l-alpha">
                    <li>suspect a user has breached or acted inconsistently with the terms or intent of this Agreement, the Privacy Policy, or other Forsublease policy documents and community guidelines;</li>
                    <li>suspect a user has engaged in improper or fraudulent activity in connection with Forsublease or actions that may cause legal liability or financial loss to Forsublease's users or to Forsublease, or</li>
                    <li>are unable to verify or authenticate any of a user’s personal information or content,</li>
                  </ol>

                  <p>without limiting any other remedies, Forsublease may, without notice, and without refunding any fees:</p>
                  <ol type="a" class="ol-l-alpha">
                    <li>delay or immediately remove Content;</li> 
                    <li>warn Forsublease's community of a user's actions;</li>
                    <li>issue a warning to a user;</li>
                    <li>temporarily or indefinitely suspend a user including a user's account privileges;</li> 
                    <li>terminate a user's account;</li>
                    <li>prohibit access to the Site;</li> 
                    <li>take technical and legal steps to keep a user off the Site; and</li> 
                    <li>refuse to provide services to a user.</li>
                  </ol>

                  <p>12. Privacy</p>

                  <p>By using the Services, you agree to the collection, transfer, storage and use of your personal information by Forusblease as further described in our Privacy Policy. Except as provided in our Privacy Policy, we will not sell or disclose your personal information to third parties without your consent.</p>

                  <p>13. Limitations of Liability</p>

                  <p>The Services are provided on an "as is" and "as available" basis. Forsublease will provide the services offered by the Site with due care and skill but does not guarantee they will be continuous, uninterrupted or fault free. To the extent any functionality in the Services is subject to delays beyond our control including, without limitation, delays or late, due to your physical location or your data service provider's network. </p>

                  <p>You agree not to hold us responsible for the actions and content posted by other users. We do not guarantee the accuracy of posting or user communications. We do not guarantee the accuracy of postings or user communications or the quality, safety or legality of what is offered. We do not accept liability for the posting of any unlawful, threatening, abusive, defamatory, obscene or indecent information, or any material or any kind that violates or infringes upon the rights on any other person including, without limitation conduct that would constitute or could encourage a criminal offence, give rise to civil liability or otherwise violate any applicable law. </p>

                  <p>To the extent legally permitted, Forsublease excludes all warranties, rights remedies and liability to you or a third party for personal injury or death, damage to real or tangible property, breach of contract, negligence or breach of any other law, including representations or warranties, express or implied, including those of merchantable quality, durability, fitness for a particular purpose and those arising by statute. To the extent legally permitted, Forsublease is not liable for any loss, whether of money (including profit), goodwill, reputation or any special, indirect or consequential damages arising out of your use of the Services, even if you advise us or we could reasonably foresee the possibility of any such damage occurring. </p>

                  <p>For any liability which cannot lawfully be excluded, but can be limited, our liability to you or any third party (whether in contract, tort, statute or otherwise) is limited to the greater of (a) the total fees you pay to us in the 12 months before the action giving rise to the liability and (b)AUD$100. </p>

                  <p>Notwithstanding anything else in these terms and conditions, our liability will be reduced to the extent the loss of damage is caused by you, your employees, agents or contractors. </p>

                  <p>15. Indemnity</p>
                  
                  <p>You indemnify and hold harmless Forsublease and its affiliates and their respective officers, directors, agents, subsidiaries, joint venturers and employees) from any and all claims, demands and damages (actual and consequential) arising out of or in any way connected with your use of the Services. </p>

                  <p>19. No Agency</p>

                  <p>You and Forsublease are independent contractors, and no agency, partnership, joint venture, employee-employer or franchiser-franchisee relationship is intended or created by this Agreement. </p>

                  <p><strong>20. General</strong></p> 

                  <p>Forsublease reserves the right to modify or terminate the Forsublease service for any reason, without notice, at any time. Forsublease reserves the right to alter these Terms and Conditions or other Site policies at any time, so please review the policies frequently. If Forsublease makes a material change Forsublease will notify you here, by email, by means of a notice on our home page, or other places Forsublease deems appropriate at its discretion. </p>

                  <p>This Agreement is governed by the laws of Western Australia. Each party submits to the exclusive jurisdiction of the courts of that place and the courts of appeal from them. If any provision or part of a provision of this Agreement is held to be invalid or unenforceable, that provision or part of a provision is to be regarded as having been deleted from this Agreement and this Agreement otherwise remains in full force and effect. If we do not enforce a particular provision of this Agreement, we are not waiving our right to do so later. </p>

                  <p>Effective Date: 9 October 2016</p>

                </div>
              </div>
          </div>
      </div>
  </div>

@endsection
