@extends('layouts.master')

@section('title', "Blogs")

@section('content')
   	
    <div id="main-container" class="single-page gear-photography">
        <div class="container">
            <div class="row main-row">  
                <div class="col-lg-12 col-md-12 col-xs-12 header-title">
                    <h1>Photography</h1>  
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12 gear-photography-container">
                    <div class="section col-lg-11 col-sm-12 col-xs-12">
                        <p><strong>Guide to Gear Photography</strong></p>

                        <p><i>That new/vintage ****** dripping in soft airy, silver light. In the photos it looks good enough to eat, right. There's a small figure between you, the hungry mother bear, and your synth-child. It's your bank account. Natural instincts kick in, old gear is hoarded, ads are placed. Thrashing, then murmurs. It's 2am. The bank account is... no more</i></p>

                        <p>It almost doesn't need to be said, but killer photos help to sell gear online about as much as anything. It's not the only thing that'll get us fired up to spend our dollars, but we've all had our attention caught by a saucy little someone - in the right lighting.</p>

                        <p>*Disclaimer - this is a musicians grade-guide to taking decent photos of your stuff, to help it sell good, or just improving the bad photo. So without further ado… Shaun’s especial..</p>

                        <p class="txt-align-c"><strong>Musician’s Guide to Taking Pretty Decent Photos of your Stuff to Help It Sell Good (or just Improving the Bad Photo)</strong></p>

                        <p>The thing that makes a great gear photo, is coincidentally, the same thing that gives us the ability to see photos, and well, <i>sustains all life on this planet the humans call Earth...</i> Yep, Light!</p>

                        <p>Using a flash and an SLR camera you can take yourself some A+ pretty decent photos. Just add: something reflecty (I’m using A3 paper) and 2 parts will power.</p>

                         <div class="row gridviewlayout">
                            <div class="col-lg-4 col-sm-6 col-xs-12 product-item">
                                <img src="{{ url('images/gear-photography/photography-2.jpg') }}" class="img-responsive" alt="" title="" />
                            </div>
                            <div class="col-lg-4 col-sm-6 col-xs-12 product-item">
                                <img src="{{ url('images/gear-photography/photography-4.jpg') }}" class="img-responsive" alt="" title="" />
                            </div>
                            <div class="col-lg-4 col-sm-6 col-xs-12 product-item">
                                <img src="{{ url('images/gear-photography/photography-6.jpg') }}" class="img-responsive" alt="" title="" />
                            </div>
                        </div>

                        <p>No flash? No worries! Simply use the power of everyone’s favourite sky orb, also known as the ALMIGHTY SUN, to provide your light, using the same principles as above.</p>

                        <p>No sun? Well, you’re not done yet. Try making your own "sun" "effect" using your sh*tty bedside lamp! Firmly place the lamp (or other light source nearby) to the thing you wish to turn into money, and proceed. Photos still garbage? (you are your own harshest critic) Then it’s worth waiting till the morning to get a decent photo, your GAS will thank you for it. </p>

                    </div>
                    <!--div class="row gridviewlayout">
                        <div class="col-lg-4 col-sm-6 col-xs-12 product-item">
                            <img src="{{ url('images/gear-photography/photography-1.jpg') }}" class="img-responsive" alt="" title="" />
                        </div>
                        <div class="col-lg-4 col-sm-6 col-xs-12 product-item">
                            <img src="{{ url('images/gear-photography/photography-2.jpg') }}" class="img-responsive" alt="" title="" />
                        </div>
                        <div class="col-lg-4 col-sm-6 col-xs-12 product-item">
                            <img src="{{ url('images/gear-photography/photography-3.jpg') }}" class="img-responsive" alt="" title="" />
                        </div>
                        <div class="col-lg-4 col-sm-6 col-xs-12 product-item">
                            <img src="{{ url('images/gear-photography/photography-4.jpg') }}" class="img-responsive" alt="" title="" />
                        </div>
                        <div class="col-lg-4 col-sm-6 col-xs-12 product-item">
                            <img src="{{ url('images/gear-photography/photography-5.jpg') }}" class="img-responsive" alt="" title="" />
                        </div>
                        <div class="col-lg-4 col-sm-6 col-xs-12 product-item">
                            <img src="{{ url('images/gear-photography/photography-6.jpg') }}" class="img-responsive" alt="" title="" />
                        </div>
                        <div class="col-lg-4 col-sm-6 col-xs-12 product-item">
                            <img src="{{ url('images/gear-photography/photography-7.jpg') }}" class="img-responsive" alt="" title="" />
                        </div>
                        <div class="col-lg-4 col-sm-6 col-xs-12 product-item">
                            <img src="{{ url('images/gear-photography/photography-8.jpg') }}" class="img-responsive" alt="" title="" />
                        </div>
                        <div class="col-lg-4 col-sm-6 col-xs-12 product-item">
                            <img src="{{ url('images/gear-photography/photography-9.jpg') }}" class="img-responsive" alt="" title="" />
                        </div>
                        <div class="col-lg-4 col-sm-6 col-xs-12 product-item">
                            <img src="{{ url('images/gear-photography/photography-10.jpg') }}" class="img-responsive" alt="" title="" />
                        </div>
                    </div-->
                </div>
            </div>
        </div>
    </div>
@endsection