@extends('layouts.api')

@section('title', 'Home')

@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 class="page-header">Home</h1>
        <h5>Welcome to What Jealous API Reference.</h5>
        <h5>For more info please contact <a href="mailto:richmund@sushidigital.com.au" title="Richmund Lofranco">richmund@sushidigital.com.au</a></h5>

        <div class="alert alert-info">
        	<h5>Notes:</h5>
        	<ul>
        		<li>If private, only brags from user you followed/following will display</li>
        	</ul>
        </div>
    </div>
@endsection