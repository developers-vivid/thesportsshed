@extends('layouts.api')

@section('title', 'Method not found')

@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 class="page-header">{{ ucfirst($method) }}</h1>
        <div class="alert alert-danger text-center">
            <strong>Error!</strong> {{ $message }}
        </div>
    </div>
@endsection