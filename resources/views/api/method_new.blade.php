@extends('layouts.api_new')

@section('title', $title)
@section('token', (isset($token) ? $token : ''))

@section( 'content' )
	<script src="{{ url('js/run_prettify.js') }}"></script>
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    	<h4 class="page-header">{{ ucwords(str_replace("-", " ", $method)) }}</h4>
    	@if( ! in_array($method, config('gp_conf.apiguest')) )
    		<div class="alert alert-danger text-center" style="padding: 0px;">
				<h5><strong>WARNING!!</strong><br/>This API requires <strong>token</strong>. This is to indentify the <strong>current user</strong>.</h5>
    		</div>
    	@endif

    	@if( isset($notes) )
			@if( $method == 'dashboard' )
				<div class="panel panel-warning">
				  	<div class="panel-heading"><strong>API Collections</strong></div>
				  	<div class="panel-body">
				  		<div style="max-height: 100px; overflow: auto;">
							@if( count(($files = array_values(array_diff(scandir( public_path('/apicollections') ), array('.', '..'))))) > 0 )
								<ul class="ul-tag">
									@for( $i = (count($files) - 1); $i >= 0; $i-- )
										<li>
											<a href="{{ url('/apicollections', [$files[$i]]) }}" title="">{{ $files[$i] }}</a>
										</li>
									@endfor
								</ul>
							@endif					  			
				  		</div>
				  	</div>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="panel panel-warning">
						  	<div class="panel-heading"><strong>Configuration</strong></div>
						  	<div class="panel-body">
								<pre class="configuration prettify" style="max-height: 200px; overflow: auto;">loading...</pre>
						  	</div>
						</div>	
					</div>
					<div class="col-md-6">
						<div class="panel panel-warning">
						  	<div class="panel-heading"><strong>Definition</strong> <small class="pull-right">[description] => [value]</small></div>
						  	<div class="panel-body">
						  		<pre class="notes prettify" style="max-height: 200px; overflow: auto;">loading...</pre>
						  	</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						@if( isset($pendingTasks) && count($pendingTasks) > 0 )
							<div class="panel panel-warning">
							  	<div class="panel-heading"><strong>Pending Tasks</strong></div>
							  	<div class="panel-body">
							  		<div style="max-height: 200px; overflow: auto;">
										@foreach($pendingTasks as $task)
											<code class="api"><strong>{{ $task }}</strong></code>
										@endforeach
									</div>
							  	</div>
							</div>
						@endif							
					</div>
					<div class="col-md-6">
						@if( isset($questions) && count($questions) > 0 )
							<div class="panel panel-warning">
							  	<div class="panel-heading"><strong>Questions?</strong></div>
							  	<div class="panel-body">
							  		<div style="max-height: 200px; overflow: auto;">
										@foreach($questions as $question)
											<code class="api">{!! $question !!}</code>
										@endforeach
									</div>
							  	</div>
							</div>
						@endif
					</div>
				</div>
				
			@else
				<div class="panel panel-warning heads-up">
				  	<div class="panel-heading"><strong>Heads Up!</strong></div>
				  	<div class="panel-body">
				  		<div style="max-height: 200px; overflow: auto;">
				  			<ul style="padding: 0; list-style: none; margin-bottom: 0px;">
				  				@if( isset($notes) && count($notes) > 0 )
									@foreach($notes as $i => $note)
										<li><code style="display: inline-block; margin-bottom: 3px;"><strong>{{ $i+=1 }}</strong>. {!! $note !!}</code></li>
									@endforeach
								@else
									<li><code style="display: inline-block; margin-bottom: 3px;">No important notes added</code></li>
								@endif
							</ul>
						</div>
				  	</div>
				  	<div class="panel-footer">
				  		@if( isset($url) && $url != '' )
				    		<code style="word-wrap: break-word;">
				    			url: <strong>{{ url('/') .'/'. ($method != 'settings' ? (env('API_PREFIX', 'api/v'.env('API_VERSION', 1.1)) .'/') : ''). $url }}</strong>
				    		</code>
				    	@endif
				  	</div>
				</div>
			@endif
		@endif

    	<div class="row">
    		@if( $method != 'dashboard' )
	    		<div class="col-md-6">
	    			<div class="panel panel-info">
					  	<div class="panel-heading"><strong>Request <small class="text-muted">(raw format)</small></strong></div>
					  	<div class="panel-body" style="max-height: 500px; overflow: auto;">
							<pre class="request prettify">loading...</pre>					  		
					  	</div>
					</div>
	    		</div>
	    		<div class="col-md-6">
	    			<div class="panel panel-info">
					  	<div class="panel-heading"><strong>Response <small class="text-muted">(json format)</small></strong></div>
					  	<div class="panel-body" style="max-height: 500px; overflow: auto;">
							<pre class="response prettify">loading...</pre>
					  	</div>
					</div>
	    		</div>
	    	@endif
    	</div>
    </div>

	<script>
		$(document).ready(function(){
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
			});

			$.ajax({
				"url": "{{ route(Route::currentRouteName(), [$method]) }}",
				"type": 'post'
			}).done(function(response){
				@if( $method != 'dashboard' )
					$('.request').html(syntaxHighlight(JSON.stringify(response.data.request, undefined, 4)));
					$('.response').html(syntaxHighlight(JSON.stringify(response.data.response, undefined, 4)));
				@else
					$('.configuration').html(syntaxHighlight(JSON.stringify(response.data.configuration, undefined, 4)));
					$('.notes').html(syntaxHighlight(JSON.stringify(response.data.notes, undefined, 4)));
				@endif
			});

			function syntaxHighlight(json) {
			    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
			    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
			        var cls = 'number';
			        if(/\S+@\S+\.\S+/.test(match)) {
			            cls = 'email';
			        } else if(/(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/.test(match)) {
			            cls = 'url';
			        } else if (/^"/.test(match)) {
			            if (/:$/.test(match)) {
			                cls = 'key';
			            } else {
			                cls = 'string';
			            }
			        } else if (/true|false/.test(match)) {
			        	if (/true/.test(match))
			            	cls = 'boolean positive';
			        	else
			            	cls = 'boolean negative';
			        } else if (/null/.test(match)) {
			            cls = 'null';
			        }
			        return '<span class="' + cls + '">' + match + '</span>';
		    	});
			}
		});
	</script>

@endsection