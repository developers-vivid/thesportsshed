@extends('layouts.api')

@section('title', 'Home')

@section( 'content' )
	<div class="panel panel-default">
		<div class="panel-heading"><strong>API List</strong></div>
		<div class="panel-body">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				@foreach( \App\Helper::getApiList() as $index => $api )
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="heading{{ str_slug($api['title']) }}">
					      	<h4 class="panel-title">
					        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ str_slug($api['title']) }}" aria-expanded="true" aria-controls="collapse{{ str_slug($api['title']) }}">
					          		<p style="font-size: 13px; font-weight: bold; margin-bottom: 0;">{{ $api['title'] }}</p>
					        	</a>
					      	</h4>
					    </div>
					    <div id="collapse{{ str_slug($api['title']) }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ str_slug($api['title']) }}">
					      	<div class="panel-body">
					        	<p>{{ $api['description'] }}</p>
					        	<hr/>
					        	<a href="{{ $api['url'] }}" class="btn btn-sm btn-success" title="">View &raquo;</a>
					      	</div>
					    </div>
					</div>
				@endforeach
			</div>
		</div>
	</div>

@endsection