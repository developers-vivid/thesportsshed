<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsShippingCost extends Model {
  protected $table = 'products_shipping_cost';
  protected $primaryKey = 'product_shipping_cost_id';
  public $incrementing = false;
  protected $softDelete = true;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'product_shipping_cost_id', 'product_id', 'location', 'cost',
  ];

  protected $dates = [
  	'created_at', 'updated_at',
  ];

  protected $hidden = [
  ];
}
