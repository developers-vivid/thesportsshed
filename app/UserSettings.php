<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSettings extends Model{

	protected $table = 'users_settings';
	protected $primaryKey = 'settings_id';
	public $incrementing = false;
	protected $softDelete = true;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'settings_id', 'user_id', 'email_message_notification',
	];

	protected $dates = [
		'created_at', 'updated_at',
	];

	protected $hidden = [
	];
}
