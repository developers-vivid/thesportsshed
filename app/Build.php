<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Build extends Model {
	protected $table = 'builds';
	protected $primaryKey = 'build_id';
	public $incrementing = false;
	protected $softDelete = true;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'build_id',
		'version',
		'device',
	];

	protected $dates = [
		'created_at',
		'updated_at',
	];

	protected $hidden = [
	];
}
