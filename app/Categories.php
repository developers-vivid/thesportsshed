<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
 	protected $table = 'categories';
  protected $primaryKey = 'category_id';
  public $incrementing = false;
  protected $softDelete = true;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'category_id', 'name', 'slug', 'description', 'parent'
  ];

  protected $dates = [
  	'deleted_at', 'created_at', 'updated_at',
  ];

  protected $hidden = [
  	
  ];
}
