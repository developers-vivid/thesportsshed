<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $gate->define('update-ad', function ($user, $ad) {
            return $user->user_id == $ad->created_by;
        });

        // to activate ad
        $gate->define('activate-ad', function ($user, $ad) {
            return ($user->isAdmin() || $user->isSuperAdmin()) || $user->user_id == $ad->created_by;
        });

        $gate->define('cancel-ad', function ($user, $ad) {
            return ($user->isAdmin() || $user->isSuperAdmin()) || $user->user_id == $ad->created_by;
        });

        $gate->define('delete-ad', function ($user, $ad) {
            return (($user->isAdmin() || $user->isSuperAdmin()) || $user->user_id == $ad->created_by) && config('gp_conf.can.delete_ad');
        });

        // who can update build only. SuperAdmin only
        $gate->define('update-build', function($user) {
            return $user->isSuperAdmin();
        });

    }
}
