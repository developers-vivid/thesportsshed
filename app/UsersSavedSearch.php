<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;
use App\Helper as Hlpr;

class UsersSavedSearch extends Model {
	protected $table = 'users_saved_search';
	protected $primaryKey = 'search_id';
	public $incrementing = false;
	protected $softDelete = true;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'search_id', 'keyword', 'url', 'created_by',
	];

	protected $dates = [
		'created_at', 'updated_at'
	];

	protected $hidden = [
	];

	/**
	 * [getSavedSearch description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-11-07T10:56:15+0800]
	 * @param  [type] $userId [description]
	 * @return [type]         [description]
	 */
	public static function getSavedSearch( $userId = null ){
		return DB::table('users_saved_search as uss')
				->select('uss.search_id', Hlpr::nullToStr('uss.keyword'), 'uss.url', 'uss.created_at',
					'user.name as user_name', 'user.email as user_email'
				)
				->join('users as user', 'uss.created_by', '=', 'user.user_id')
				->when((!is_null($userId)), function($query) use($userId){
					return $query->where('uss.created_by', $userId);
				})
				->orderBy('uss.created_at', 'desc')
				->get();
	}

}
