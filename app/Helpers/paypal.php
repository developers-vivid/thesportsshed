<?php

namespace App;

use Carbon\Carbon;

class PayPal {

	protected $paypalHeaders;
    protected $paypalRequestEnvelop;

	/**
	 * [__construct description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-09T08:20:20+0800]
	 * @param  [type] $filepath [description]
	 * @param  string $mode     [description]
	 */
	public function __construct(){
		$this->paypalHeaders = array(
            "X-PAYPAL-SECURITY-USERID: ". env('PAYPAL_USERID', 'richmund-facilitator_api1.sushidigital.com.au'),
            "X-PAYPAL-SECURITY-PASSWORD: ". env('PAYPAL_PASSWORD', 'DWL92ZBZLYTVS325'),
            "X-PAYPAL-SECURITY-SIGNATURE: ". env('PAYPAL_SIGNATURE', 'AFcWxV21C7fd0v3bYYYRCpSSRl31AhwR1UotWjj8kmOL8L9DGpmEnWtR'),
            "X-PAYPAL-REQUEST-DATA-FORMAT: JSON",
            "X-PAYPAL-RESPONSE-DATA-FORMAT: JSON",
            "X-PAYPAL-APPLICATION-ID: ". env('PAYPAL_APPLICATION_ID', 'APP-80W284485P519543T')
        );

        $this->paypalRequestEnvelop = [
            "errorLanguage" => "en_US"
        ];
	}

	/**
     * URL use to redirect to PayPal
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-18T14:36:47+0800]
     * @param  string $payKey [description]
     * @return [type]         [description]
     */
	public function getRedirectToPayPalUrl($payKey = '', $sandbox = true){
		if( $sandbox )
            return 'https://www.sandbox.paypal.com/webapps/adaptivepayment/flow/pay?expType=light&payKey='. $payKey;
        else
            return 'https://www.paypal.com/webscr?cmd=_ap-payment&paykey='. $payKey;
	}

	/**
	 * [getPaypalResponseStatusValue description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-09T08:41:19+0800]
	 * @param  [type] $status [description]
	 * @return [type]         [description]
	 */
	public function getPaypalResponseStatusValue( $status ){
		$orderStatus = 'failed';
                
        if( strtolower($status) == 'completed' )
        	$orderStatus = config('gp_conf.orders.status.completed');

        else if( strtolower($status) == 'created' )
        	$orderStatus = config('gp_conf.orders.status.onHold');

       	else if( strtolower($status) == 'expired' )
       		$orderStatus = config('gp_conf.orders.status.cancelled');

       	return $orderStatus;
	}

	/**
	 * Get paypal adaptive payments payment details url
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-09T08:23:29+0800]
	 * @param  string  $payKey        [description]
	 * @param  array   $paypalHeaders [description]
	 * @param  boolean $sandbox       [description]
	 * @return [type]                 [description]
	 */
	public function getPaymentDetails( $payKey = '', $sandbox = true ) {
		$ch = curl_init();

	    curl_setopt($ch, CURLOPT_URL, "https://svcs". ($sandbox ? '.sandbox' : '') .".paypal.com/AdaptivePayments/PaymentDetails");
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array('payKey' => $payKey, "requestEnvelope" => $this->paypalRequestEnvelop)));
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->paypalHeaders);

	    $result = curl_exec( $ch );

	    if (curl_errno($ch)) { 
		   dd(curl_error($ch));
		} 

	    $response = json_decode( $result );
	    curl_close ($ch);

	    return $response;
	}
}