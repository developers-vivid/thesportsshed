<?php

namespace App;

use Auth;
use Session;
use DB;
use Log;

use Carbon\Carbon;

use App\User;
use App\UsersProfile;
use App\UsersRoles;
use App\UsersCompany;

class UserObj {
	protected static $userId;
	protected $_user;
	protected $_userProfile;

	public function __construct(){
		self::$userId = Auth::user()->user_id;
	}

	public static function isActive(){
		return self::$userId;
		// return self::_user->status == 'active' ? true : false;
	}
}