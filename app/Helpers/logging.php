<?php

namespace App;

use Log;
use Carbon\Carbon;

class Logging {

	/**
	 * [messageInfo description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-30T13:35:37+0800]
	 * @param  [type] $message [description]
	 * @param  array  $data    [description]
	 * @return [type]          [description]
	 */
	public static function messageInfo( $message, $data = array() ) {
		$filePath = fopen(storage_path('logs/message.log'), 'a+');
		self::writeFile( $filePath, $message, $data );
	}

	/**
	 * [messageError description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-30T13:35:33+0800]
	 * @param  [type] $message [description]
	 * @param  array  $data    [description]
	 * @return [type]          [description]
	 */
	public static function messageError( $message, $data = array() ) {
		$filePath = fopen(storage_path('logs/message.log'), 'a+');
		self::writeFile( $filePath, $message, $data, "ERROR" );
	}

	/**
	 * [writeFile description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-30T13:35:46+0800]
	 * @param  [type] $theFilePath [description]
	 * @param  [type] $message     [description]
	 * @param  array  $data        [description]
	 * @param  string $logType     [description]
	 * @return [type]              [description]
	 */
	protected static function writeFile( $theFilePath, $message, $data = array(), $logType = 'INFO' ) {
		$theMessage = "[". Carbon::now()->toDatetimeString() ."] ".config('app.env') . ".{$logType}: ". $message .PHP_EOL;
		
		if( count($data) > 0 )
			$theMessage .= json_encode( $data ) .PHP_EOL.PHP_EOL;

		$writed = fwrite( $theFilePath, $theMessage );
		if( ! $writed )
			Log::error("Cannot write to file (". $theFilePath .")");
		
		fclose( $theFilePath );
	}
}