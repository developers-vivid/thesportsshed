<?php

namespace App;

use Session;
use DB;
use Carbon\Carbon;
use Mail;

use App\GpLog as Log;

class Helper {

	/**
	 * To get Unique ID
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-02-05T10:16:23+0800]
	 * @return [type] [description]
	 */
	public static function getUID(){
		return uniqid();
	}

	/**
	 * [getAllValidationError description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-10-28T10:23:23+0800]
	 * @param  [type]  $error    [description]
	 * @param  boolean $asString [description]
	 * @return [type]            [description]
	 */
	public static function getAllValidationError( $error, $asString = true ) {
		$errBag = "";

		if( count($error) > 0 ) {
			$errBag .= "<ul>";

			if( !$asString )
				$errBag = [];
				

			foreach($error->toArray() as $err) {
				for($i = 0; $i < count($err); $i++)
					if( !$asString )
						$errBag[] = substr($err[$i], 0, -1);
					else
						$errBag .= "<li>". substr($err[$i], 0, -1) ."</li>";
			}

			if( $asString )
				$errBag .= "</ul>";
		}

		return $errBag;
	}

	/**
	 * [setArrayFromUrl description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-09-01T15:38:23+0800]
	 * @param  [type] $url [description]
	 */
	public static function setArrayFromUrl( $url ) {
	    $remove_http = str_replace('http://', '', $url);
	    $split_url = explode('?', $remove_http);
	    $get_page_name = explode('/', $split_url[0]);
	    $page_name = $get_page_name[1];

	    $split_parameters = explode('&', $split_url[1]);

	    for($i = 0; $i < count($split_parameters); $i++) {
	        $final_split = explode('=', $split_parameters[$i]);
	        $split_complete[$page_name][$final_split[0]] = $final_split[1];
	    }

	    return ($split_complete && is_array($split_complete)) ? (object)$split_complete : $split_complete;
	}

	/**
	 * [getMimeTypeFromBase64 description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-17T10:45:41+0800]
	 * @param  [type] $base64 [description]
	 * @return [type]         [description]
	 */
	public static function getMimeTypeFromBase64( $base64 ){
		$img  = explode(',', $base64);
		$ini  = substr($img[0], 11);
		$type = explode(';', $ini);
		return (count($type) && isset($type[0])) ? $type[0] : null;
	}

	/**
	 * Update order cancelled/created/completed
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-13T12:01:43+0800]
	 * @param  [type]  $orderId     [description]
	 * @param  boolean $isCompleted [description]
	 * @return [type]               [description]
	 */
	public static function updateOrder( $orderId, $isCompleted = false, $response = null ){
		$dateOrdered = Carbon::now();
		$orderLog = new Log('order');

        $updateOrder = [
            "status" => config('gp_conf.orders.status.'. ($isCompleted ? 'completed' : 'cancelled')), 
            "token" => null, 
            "updated_at" => $dateOrdered
        ];

        if( !is_null($response) && strtolower($response->status) == "completed" ) {
            $updateOrder["date_completed"] = $dateOrdered;
            $updateOrder["paypal_payer_email"] = $response->sender->email;
            $updateOrder["paypal_payer_id"] = $response->sender->accountId;
        }
        else if( !is_null($response) && in_array(strtolower($response->status), ["failed", "expired"]) ) {
            $updateOrder["date_cancelled"] = $dateOrdered;
        }
        else if( !is_null($response) && strtolower($response->status) == "refunded" ) {
            $updateOrder["date_refunded"] = $dateOrdered;
        }
        else {
            $updateOrder["date_cancelled"] = $dateOrdered;        	
        }

        // Update Order
        \App\Orders::where(['order_id' => $orderId])->update($updateOrder);

        $orderHistory = 'Order has been '. ($isCompleted ? 'completed' : 'cancelled');
        $orderLog->write( $orderHistory . '. ['. $orderId .']' );

		// update order history
        \App\OrdersHistory::create([
            "order_history_id" => self::getUID(),
            "order_id"         => $orderId,
            "description"      => $orderHistory,
            "created_at"       => $dateOrdered
        ]);

        $orderDetails = \App\Orders::getOrderDetails( $orderId );
        $orderMessage = trans('messages.checkout.'. ($isCompleted ? 'completed' : 'cancelled'));

        $orderedProducts = \App\OrdersProducts::getOrderedProducts($orderId);
        foreach( $orderedProducts as $prod ) {
        	\App\Products::where(['product_id' => (string)$prod->product_id])->update([
                "status" => ($isCompleted ? 'sold' : 'active'),
                "sold_at" => ($isCompleted ? $dateOrdered : null),
                "updated_at" => $dateOrdered
            ]);
        	$orderLog->write('Product ['. $prod->product_id .'] has been '. ($isCompleted ? 'sold' : 'active') .'.');

            // send order summary
            try{
                if( ! config('gp_conf.isLocal') && $isCompleted ) {
                	$customerName = ($orderDetails->customer_first_name .' '. $orderDetails->customer_last_name);
                	
                	// to seller
                    Mail::send('mail.checkout-summary', ['orderDetails' => $orderDetails, 'orderMessage' => 'You have received an order from '. $customerName .'. Their order is as follows: '], function ($m) use ($prod) {
                        $m->from('gearplanet@info.com.au', 'Gear Planet');
                        $m->to($prod->owner_email, $prod->owner_name)->subject('Order Summary');
                        // $m->bcc('richmund@sushidigital.com.au', 'Richmund at Sushi');
                    });
                    $orderLog->write('Sent email to seller email:['. $prod->owner_email .'], ownerName:['. $prod->owner_name .'], orderId['. $orderId .'].');

                    // to buyer
                    Mail::send('mail.checkout-summary', ['orderDetails' => $orderDetails, 'orderMessage' => 'Your order has been received and is now being processed. Your order details are shown below for your reference:'], function ($m) use ($prod, $customerName, $orderDetails) {
                        $m->from('gearplanet@info.com.au', 'Gear Planet');
                        $m->to($orderDetails->customer_email, $customerName)->subject('Order Summary');
                        // $m->bcc('richmund@sushidigital.com.au', 'Richmund at Sushi');
                    });
                    $orderLog->write('Sent email to buyer email:['. $prod->owner_email .'], ownerName:['. $prod->owner_name .'], orderId['. $orderId .'].');
                }
            } catch (Exception $e){
              $orderLog->error('Unable to send order summary to ['. $prod->owner_email .']['. $prod->owner_name .']');
            }
        }

        return view('cart.checkout-summary', compact('orderDetails', 'orderMessage'));
	}

	/**
	 * [updateOrderShippingInfo description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-09T10:46:31+0800]
	 * @param  [type] $address1 [description]
	 * @param  [type] $address2 [description]
	 * @param  [type] $city     [description]
	 * @param  [type] $state    [description]
	 * @param  [type] $postCode [description]
	 * @param  [type] $userId   [description]
	 * @param  [type] $orderId  [description]
	 * @return [type]           [description]
	 */
	public static function updateOrderShippingInfo( $address1, $address2, $city, $state, $postCode, $userId, $orderId ){
		$shippingInfoData = [
            "address_1"  => $address1,
			"address_2"  => $address2,
			"city"       => $city,
			"state"      => $state,
			"post_code"  => $postCode,
			"updated_at" => Carbon::now()
		];

		$shippingAddress = \App\UsersShippingAddress::where(['user_id' => $userId])->update($shippingInfoData);

        if( ! $shippingAddress ){
            unset($shippingInfoData['updated_at']);

			$shippingInfoData['shipping_id'] = self::getUID();
			$shippingInfoData['user_id']     = $userId;
			$shippingInfoData['created_at']  = Carbon::now();

            $shippingAddress = \App\UsersShippingAddress::create($shippingInfoData);
        }

        // create order shipping info
        if( ! is_null($orderId) ) {
	        $orderShippingInfo = OrdersShippingInfo::create([
	        	"order_shipping_id" => self::getUID(),
				"order_id"   => $orderId,
				"address_1"  => $address1,
				"address_2"  => $address2,
				"city"       => $city,
				"state"      => $state,
				"post_code"  => $postCode,
				"created_at" => Carbon::now()
	        ]);        	
        }

        return ! is_null($orderId) ? $orderShippingInfo : $shippingInfoData;
	}

	/**
	 * Parse url to str
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-08T15:47:19+0800]
	 * @param  [type] $url [description]
	 * @return [type]      [description]
	 */
	public static function parseUrlToStr( $url ){
	    $remove_http = str_replace('http://', '', $url);
	    $split_url = explode('?', $remove_http);
	    $get_page_name = explode('/', $split_url[0]);
	    $page_name = $get_page_name[1];

	    $split_parameters = explode('&', $split_url[1]);

	    for($i = 0; $i < count($split_parameters); $i++) {
	        $final_split = explode('=', $split_parameters[$i]);
	        $split_complete[$page_name][$final_split[0]] = $final_split[1];
	    }

	    return $split_complete;
	}

	/**
	 * [removeGearPhoto description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-08T16:00:40+0800]
	 * @return [type] [description]
	 */
	public static function removeGearPhoto( $photoId ) {
		$photoDetails = ProductsPhotos::wherePhotoId( $photoId )->first();

		if( Products::whereProductId($photoDetails->product_id)->whereProductPrimaryPhoto($photoDetails->photo_id)->count() == 0 ) {
			$thumbPath = public_path('/images/gears/thumbs/'. $photoDetails->photo_filename);
	        if( file_exists($thumbPath) )
	            @unlink($thumbPath);

	        // remove from thumbs
	        $origPath = public_path('/images/gears/'. $photoDetails->photo_filename);
	        if( file_exists($origPath) )
	            @unlink($origPath);

	        return ProductsPhotos::wherePhotoId( $photoId )->delete();			
		}

		return null;
	}

	/**
	 * Append all url params from search page
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-08T16:00:23+0800]
	 * @param  [type] $url [description]
	 * @return [type]      [description]
	 */
	public static function appendAllUrlParams( $url ){
		$urlArr = self::parseUrlToStr( $url );
		$urlArr = $urlArr['search'];

		$keyword   = (isset($urlArr['keyword']) && $urlArr['keyword'] != '') ? $urlArr['keyword'] : '';
		$location  = (isset($urlArr['location']) && $urlArr['location'] != '') ? ' + '.$urlArr['location'] : '';
		$brand     = (isset($urlArr['brand']) && $urlArr['brand'] != '') ? ' + '.$urlArr['brand'] : '';
		$price_min = (isset($urlArr['price_min']) && $urlArr['price_min'] != '') ? ' + '.$urlArr['price_min'] : '';
		$price_max = (isset($urlArr['price_max']) && $urlArr['price_max'] != '') ? ' + '.$urlArr['price_max'] : '';
		$type      = (isset($urlArr['type']) && $urlArr['type'] != '') ? ' + '.$urlArr['type'] : '';
		$sort      = (isset($urlArr['sort']) && $urlArr['sort'] != '') ? urldecode($urlArr['sort']) : '';

		$sortName = "";
            if( $sort == "product_price|asc" ) 
                $sortName = "Price Low to High";
            else if( $sort == "product_price|desc" )
                $sortName = "Price Hight to Low";
            else
                $sortName = "Most Recent First";

		return $keyword .' '. $location .' '. $brand .' '. $price_min .' '. $price_max .' '. $type .' + '. $sortName;
	}

	/**
	 * To splice array
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-24T11:36:09+0800]
	 * @param  [type] $arr  [description]
	 * @param  [type] $skip [description]
	 * @return [type]       [description]
	 */
	public static function spliceArr( $arr, $skip ) {
		$result = new \stdClass();

		$result->total = count($arr);
		$result->total_pages = ceil(count($arr) / (int)config('gp_conf.pagination'));
		$result->result = array_slice( $arr, ( ($skip - 1) * (int)config('gp_conf.pagination') ), (int)config('gp_conf.pagination'));

		return $result;
	}

	/**
	 * Change arr to string
	 * For api purposes only
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-27T12:38:16+0800]
	 * @param  [type] $input [description]
	 * @return [type]        [description]
	 */
	public static function arrToString( $input ){
		$output = implode(', ', array_map(
		    function ($v, $k) {
		        if(is_array($v)){
		            return $k.'[]='.implode('&'.$k.'[]=', $v);
		        }else{
		            return $k.'='.$v;
		        }
		    }, 
		    (array)$input, 
		    array_keys((array)$input)
		));

		return $output;
	}

	/**
	 * To get countries
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T15:36:49+0800]
	 * @param  boolean $withEmptyArr [description]
	 * @return [type]                [description]
	 */
	public static function getCountries( $withEmptyArr = false ){
		$countries = ["Australia", "Belgium", "Canada", "China", "Denmark", "France", "Germany", "India", "Indonesia", "Italy", "Japan", "Korea", "Mexico", "Panama", "Poland", "Russia", "Spain", "Sweden", "Switzerland", "Taiwan", "Thailand", "United Kingdom", "United States", "Vietnam", "Others"];
		$countryArr = [];

		if( $withEmptyArr )
			$countryArr[''] = "Select Country";

		for($i = 0; $i < count($countries); $i++){
			$countryArr[strtolower($countries[$i])] = $countries[$i];
		}
		return $countryArr;
	}

	/**
	 * [getBrands description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-29T10:07:46+0800]
	 * @param  boolean $withEmpty [description]
	 * @return [type]             [description]
	 */
	public static function getBrands( $withEmpty = true ) {
		$brands = \App\Products::getAllBrands();
		$theBrands = [];

		if( $withEmpty )
			$theBrands[''] = "- Select -";

		if( count($brands) > 0 ) {
			foreach( $brands as $brand ) {
				if( ! is_null( $brand->product_brand ) )
					$theBrands[strtolower($brand->product_brand)] = $brand->product_brand;
			}
		}

		return $theBrands;
	}

	/**
	 * To get states
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T15:36:56+0800]
	 * @param  boolean $withEmptyArr [description]
	 * @param  array   $except       [description]
	 * @return [type]                [description]
	 */
	public static function getStates( $withEmptyArr = false, $except = [] ){
		$st = ["NSW", "QLD", "WA", "SA", "TAS", "VIC"];
		$states = [];

		if( $withEmptyArr )
			$states[''] = "Select State";

		for($i = 0; $i < count($st); $i++){
			if( count($except) > 0 && in_array(strtolower($st[$i]), $except) )
				continue;
			else
				$states[strtolower($st[$i])] = $st[$i];
		}
		return $states;
	}

	/**
	 * To get first index of an array via manual
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T15:37:04+0800]
	 * @param  [type] $errorMessageBag [description]
	 * @return [type]                  [description]
	 */
	public static function getFirstArrayOfErrorMessageBag( $errorMessageBag ){
		reset($errorMessageBag);
    	return key($errorMessageBag);
	}

	public static function getArrayOfErrorMessageBag( $errorMessageBag ){
    	return array_keys($errorMessageBag);
	}

	public static function nullToStr($field, $alias = null){
		$fieldArr = explode(".", $field);
		return DB::raw('IFNULL('. env('DB_PREFIX', 'gp_') . $field .', "") as '. (!is_null($alias) ? $alias : $fieldArr[1]));
	}

	/**
	 * To get category name via category slug
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-04T10:15:49+0800]
	 * @param  [type] $slug [description]
	 * @return [type]       [description]
	 */
	public static function getCategoryBySlug( $slug ){
		return \App\Categories::whereSlug($slug)->value('name');
	}

	/**
	 * Get gear/product primary photo
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-30T14:23:03+0800]
	 * @param  [type] $gearID [description]
	 * @return [type]         [description]
	 */
	public static function getGearPrimaryPhoto( $gearID, $field = '' ){
		$productPhoto = \App\Products::whereProductId($gearID)->first();
		if( count($productPhoto) > 0 )
			return \App\ProductsPhotos::wherePhotoId($productPhoto->product_primary_photo)->value( ($field != '' ? $field : 'photo_filename') );
		else
			return 'no-image.png';
	}

	/**
	 * To get all categories
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-14T13:36:10+0800]
	 * @return [type] [description]
	 */
	public static function getCategories(){
		$categories = \App\Categories::whereNull('deleted_at')->whereParent('-')->orderBy('order', 'asc')->get();
		if( count($categories) > 0 ) {
			foreach($categories as $cat) {
				$cat->total = \App\Products::getTotalProducts($cat->category_id);
				$cat->children = self::getSubCategories($cat->category_id);
			}
		}
		return $categories;
	}

	/**
	 * [getSubCategories description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-30T13:39:19+0800]
	 * @param  [type] $catId [description]
	 * @return [type]        [description]
	 */
	public static function getSubCategories( $catId ) {
		$subCats = \App\Categories::whereParent($catId)->whereNull('deleted_at')->get();
		if( count($subCats) > 0 ) {
			foreach($subCats as $sCat)
				$sCat->total = \App\Products::getTotalProducts($catId, $sCat->category_id);
		}
		return $subCats;
	}

	/**
	 * To get all product conditions
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-16T13:43:09+0800]
	 * @return [type] [description]
	 */
	public static function getProductConditions(){
		return \App\ProductsCondition::all();
	}
	
	/**
	 * Handle response
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-11T09:03:03+0800]
	 * @param  string  $message        [description]
	 * @param  boolean $error          [description]
	 * @param  array   $data           [description]
	 * @param  boolean $doSessionFlash [description]
	 * @return [type]                  [description]
	 */
	public static function handleResponse( $message = 'Invalid Request', $error = true, $data = array(), $doSessionFlash = false, $apiName = '' ){
		$responseData = array(
			"status"  => ($error ? 'error' : 'success'),
			"message" => $message,
			"data"    => $data
		);

		if( !is_null($apiName) && $apiName != '' )
			$responseData['api'] = $apiName;

		if( $doSessionFlash )
			Session::flash('alert', $responseData);

		return response()->json($responseData);
	}

	/**
	 * [getRandomHex description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-09-07T12:04:11+0800]
	 * @return [type] [description]
	 */
	public static function getRandomHex() {
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			// 32 bits for "time_low"
			mt_rand(0, 0xffff), mt_rand(0, 0xffff),

			// 16 bits for "time_mid"
			mt_rand(0, 0xffff),

			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			mt_rand(0, 0x0fff) | 0x4000,

			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			mt_rand(0, 0x3fff) | 0x8000,

			// 48 bits for "node"
			mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
		);
	}

}