<?php

namespace App;

use Auth;
use Session;
use DB;
use Log;

use Carbon\Carbon;

use App\User;
use App\UsersProfile;
use App\UsersRoles;
use App\UsersCompany;

class Users {

	public static function getGearOwner($gearID){
		$query = DB::table('products as prod')
					->select('user.email', 'user.name')
					->where('prod.product_id', $gearID)
					->leftJoin('users as user', 'prod.created_by', '=', 'user.user_id')
					->get();

		return $query[0];
	}

	/**
	 * Is Administrator
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-10T14:03:03+0800]
	 * @return boolean [description]
	 */
	public static function isAdmin(){
		return (Auth::check() && UsersRoles::whereRoleId(Auth::user()->role_id)->whereName('Administrator')->count()) > 0 ? true : false;
	}

	/**
	 * Is Super Administrator
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-10T14:03:10+0800]
	 * @return boolean [description]
	 */
	public static function isSuperadmin(){
		return (Auth::check() && UsersRoles::whereRoleId(Auth::user()->role_id)->whereName('Super Admin')->count()) > 0 ? true : false;
	}

	/**
	 * Is User
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-10T14:03:18+0800]
	 * @return boolean [description]
	 */
	public static function isUser(){
		return (Auth::check() && UsersRoles::whereRoleId(Auth::user()->role_id)->whereName('User')->count()) > 0 ? true : false;
	}

	/**
	 * Handles creating user
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-09T09:42:38+0800]
	 * @param  [type] $userData [description]
	 * @return [type]           [description]
	 */
	public static function createUser( $userData, $provider = null, $logoFileName = '', $seller = true ){
		$userData = (object)$userData;
		$isSocial = !is_null($provider);
		
		$user = User::create([
			"user_id"    => \App\Helper::getUID(),
			"name"       => ($isSocial ? $userData->name : $userData->first_name .' '. $userData->last_name),
			"email"      => $userData->email,
			"password"   => ($isSocial ? bcrypt($userData->email.$provider) : bcrypt($userData->password)),
			"login_type" => ($isSocial ? $provider : 'local' ),
			"api_token"  => substr(str_shuffle(config('gp_conf.alpha_num')), 0, 60),
			"status"     => 'active',
			"role_id"    => '56aebc46f2827',	// as user by default
			"seller"     => 'yes'
		]);

		$userSetting = new \App\Settings( $user->user_id );
        $userSetting->setMessageNotification( 'on' )->save();

		// create user profile
		$userProfile = [];
		$userProfile['profile_id'] = \App\Helper::getUID();
		$userProfile['user_id'] = $user->user_id;

		$userProfile['first_name'] = ($isSocial ? $userData->name : $userData->first_name);

		if( ! $isSocial ) {
			$userProfile['last_name'] = $userData->last_name;
			
			if( isset($userData->telephone) && $userData->telephone != '' )
				$userProfile['telephone'] = $userData->telephone;

			if( isset($userData->mobile) && $userData->mobile != '' )
				$userProfile['mobile']    = $userData->mobile;
		}

		$userProfile['created_at'] = Carbon::now();
		$theUserProfile = UsersProfile::create($userProfile);

		// create user company
		if( ! $isSocial ){

			// create the crop logo image
			if( $logoFileName != '' ) {
				$logoStoragePath = storage_path() .config('gp_conf.ds'). 'company_logos' .config('gp_conf.ds');
	  		$image = new ImageResize($logoStoragePath.$logoFileName);

	  		$image->resizeToHeight(48);
	  		$image->save( $logoStoragePath. 'thumbs' .config('gp_conf.ds'). $logoFileName);
			}


			UsersCompany::create([
				"company_id" => \App\Helper::getUID(),
				"user_id"    => $user->user_id,
				"name"       => (isset($userData->company_name) ? $userData->company_name : 'no company'),
				"abn"        => (isset($userData->company_abn) ? $userData->company_abn : ''),
				"street"     => (isset($userData->company_street) ? $userData->company_street : ''),
				"city"       => (isset($userData->company_city) ? $userData->company_city : ''),
				"state"      => (isset($userData->company_state) ? $userData->company_state : ''),
				"post_code"  => (isset($userData->company_postcode) ? $userData->company_postcode : ''),
				"phone"      => (isset($userData->company_phone) ? $userData->company_phone : ''),
				"logo"       => $logoFileName,
				"created_at" => Carbon::now(),
			]);
		}
		else {
			UsersCompany::create([
				"company_id" => \App\Helper::getUID(),
				"user_id"    => $user->user_id,
				"created_at" => Carbon::now()
			]);
		}

		if( ! $isSocial )
			\App\Helper::handleResponse("You have successfully registered.", false, [], true);

		return $user ? $user->user_id : 0;
	}

}