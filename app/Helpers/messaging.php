<?php

namespace App;

use Carbon\Carbon;
use DB;
use Log;
use Mail;
// use Vinkla\Pusher\PusherManager as PushMngr;

use App\User;
use App\UsersInbox;
use App\UsersMessages;
use App\Helper as Hlpr;

class Messaging {

	// protected $pusher;

	/**
	 * Messaging
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T10:26:50+0800]
	 * @param  PusherManager $pusher [description]
	 */
	public function __construct(){
	// public function __construct( PushMngr $pusher ){
		// $this->pusher = $pusher;
	}

	/**
	 * To contact seller
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T13:36:08+0800]
	 * @param  [type] $pusher     [description]
	 * @param  [type] $senderID   [description]
	 * @param  [type] $sellerID   [description]
	 * @param  [type] $senderName [description]
	 * @param  [type] $socketID   [description]
	 * @param  [type] $message    [description]
	 * @return [type]             [description]
	 */
	public static function contactSeller( $pusher, $senderID, $sellerID, $senderName, $socketID, $message, $apiName = '' ) {
		$dateNow = Carbon::now();
        $userMessages = UsersMessages::getInboxDetails($senderID, $sellerID);
        // DB::beginTransaction();
        
		if( is_null( $userMessages ) ){
			// create inbox
			$inbox = UsersInbox::create([
				"inbox_id" => Hlpr::getUID(),
				"channel_id" => substr(str_shuffle(config('gp_conf.alpha_num')), 0, 20),
				"created_at" => $dateNow
			]);
		}

		$message = UsersMessages::create([
			"message_id"  => Hlpr::getUID(),
			"inbox_id"    => (! is_null( $userMessages ) ? $userMessages->inbox_id : $inbox->inbox_id),
			"sender_id"   => $senderID,
			"receiver_id" => $sellerID,
			"message"     => htmlspecialchars( $message, ENT_QUOTES ),
			"created_at"  => $dateNow
		]);

		// get inbox details
		$inboxDetails = UsersInbox::whereInboxId(($userMessages ? $userMessages->inbox_id : $inbox->inbox_id))->first();

		// send push message
        $pshrSent = $pusher->trigger(
        	config('gp_conf.pusher_msg_counter_id'), 
        	config('gp_conf.pusher.msg_counter.'. strtolower(env('APP_ENV'))), 
        	json_encode(["user_name" => $senderName, "receiver" => $sellerID, "inbox" => $inboxDetails->inbox_id, "message" => $message->message_id, 'datetime' => $dateNow]), 
        	$socketID
        );

		// the message
		$response = $pusher->trigger(
			$inboxDetails->channel_id, 
			config('gp_conf.pusher.new_message.'. strtolower(env('APP_ENV'))), 
			json_encode(["message_id" => $message->message_id, "receiver" => $sellerID, 'inbox' => $inboxDetails->inbox_id, 'datetime' => $message->created_at]), 
			$socketID
		);

		UsersInbox::where(['inbox_id' => $inboxDetails->inbox_id])->update(['last_message_received' => $dateNow, 'updated_at' => Carbon::now()]);

		if( $pshrSent && $response ) {
			// DB::commit();
			self::setEmailNotification( $message->receiver_id, $message->sender_id, htmlspecialchars( $message, ENT_QUOTES ), $message->inbox_id );
			
			\App\Logging::messageInfo( sprintf('contactSeller: Sent a message to (%s) from (%s) in inbox (%s) with a message id (%s)', $sellerID, $senderID, $message->inbox_id, $message->message_id) );
			return Hlpr::handleResponse( trans('messages.api.messages.sent'), false, ['message' => $message->message_id], false, $apiName );
		}
		else {
			// DB::rollBack();
			\App\Logging::messageError( sprintf('contactSeller: Error in sending message to (%s) from (%s) in inbox (%s)', $sellerID, $senderID, (! is_null( $userMessages ) ? $userMessages->inbox_id : $inbox->inbox_id)) );
			return Hlpr::handleResponse( trans('messages.api.messages.not_sent'), true, [], false, $apiName );
		}		
	}

	/**
	 * Messaging
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T13:36:14+0800]
	 * @param  [type] $pusher     [description]
	 * @param  [type] $inboxID    [description]
	 * @param  [type] $senderID   [description]
	 * @param  [type] $senderName [description]
	 * @param  [type] $socketID   [description]
	 * @param  [type] $message    [description]
	 * @return [type]             [description]
	 */
	public static function sendMessage( $pusher, $inboxID, $senderID, $sellerID, $senderName, $socketID, $message, $isApp = false, $apiName = '' ){
		$inboxDetails = UsersMessages::getUserInbox( $inboxID, $sellerID, $isApp );
		// DB::beginTransaction();
			
		$msg = UsersMessages::create([
			"message_id"  => Hlpr::getUID(),
			"inbox_id"    => $inboxID,
			"sender_id"   => $senderID,
			"receiver_id" => $sellerID,
			"message"     => htmlspecialchars( $message, ENT_QUOTES ),
			"created_at"  => Carbon::now()
		]);

		// the message
		$response = $pusher->trigger(
			$inboxDetails->channel_id, 
			config('gp_conf.pusher.new_message.'. strtolower(env('APP_ENV'))), 
			json_encode(["message_id" => $msg->message_id, 'receiver' => $sellerID, 'inbox' => $inboxID, 'datetime' => $msg->created_at]), 
			$socketID);

		// message counter
		$pshrSent = $pusher->trigger( 
			config('gp_conf.pusher_msg_counter_id'), 
			config('gp_conf.pusher.msg_counter.'. strtolower(env('APP_ENV'))), 
			json_encode(["user_name" => $senderName, 'receiver' => $sellerID, "socket" => $socketID, "inbox" => $inboxID, 'datetime' => $msg->created_at]), 
			$socketID);

		$theMsg = UsersMessages::getInboxMessages( $inboxID, $msg->message_id );

		if( $response && $pshrSent ) {
			// DB::commit();
			$theResponse = ['response' => $response];

			if( $isApp )
				$theResponse['message'] = $theMsg[0];
			else
				$theResponse['message'] = view('layouts.single-message', ['msg' => $theMsg[0]])->render();

			// send email notification
			self::setEmailNotification( $msg->receiver_id, $msg->sender_id, htmlspecialchars( $message, ENT_QUOTES ), $msg->inbox_id );

			\App\Logging::messageInfo( sprintf('sendMessage: Sent a message to (%s) from (%s) in inbox (%s) with a message id (%s)', $sellerID, $senderID, $message->inbox_id, $message->message_id) );
			return Hlpr::handleResponse( trans('messages.api.messages.sent'), false, $theResponse, false, $apiName );
		}
		else {
			// DB::rollBack();
			\App\Logging::messageError( sprintf('sendMessage: Error sending a message to (%s) from (%s) in inbox (%s) with a message id (%s)', $sellerID, $senderID, $inboxID) );
			return Hlpr::handleResponse( trans('messages.api.messages.not_sent'), true, [], false, $apiName );
		}
	}

	/**
	 * [setEmailNotification description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-09-07T16:01:31+0800]
	 * @param  [type] $receiverId [description]
	 * @param  [type] $senderId   [description]
	 * @param  [type] $message    [description]
	 * @param  [type] $inboxId    [description]
	 */
	public static function setEmailNotification( $receiverId, $senderId, $message, $inboxId ) {
		$receiverDetails = User::getFullDetails( $receiverId );
		$senderDetails   = User::getFullDetails( $senderId );
		$inboxDetails    = UsersInbox::whereInboxId( $inboxId )->first();

		try{
            if( ! config('gp_conf.isLocal') && strtolower($receiverDetails->email_message_notification) == "on" ) {
            	$msgNotif = [
					'receiverDetails' => $receiverDetails, 
					'senderDetails'   => $senderDetails, 
					'theMsg'          => $message, 
					'inboxDetails'    => $inboxDetails
				];

				Mail::send('mail.message-notification', $msgNotif, function ($m) use ($receiverDetails) {
		            $m->from('gearplanet@info.com.au', 'Gear Planet');
		            $m->to($receiverDetails->email, $receiverDetails->name)->subject('New Message Received');
		            // $m->bcc('richmund@sushidigital.com.au', 'Richmund at Sushi');
		        });

		        \App\Logging::messageInfo( sprintf('Sent an message notification email to (%s), from (%s) using inbox (%s)', $receiverId, $senderId, $inboxId), $msgNotif );
		    }
		} catch (Exception $e){
			\App\Logging::messageInfo( sprintf("Unable to send order summary to (%s), (%s)", $prod->owner_email, $prod->owner_name) );
        }
	}

}