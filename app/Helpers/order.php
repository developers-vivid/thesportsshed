<?php

namespace App;

use PayPal\Types\AP\Receiver;

use Carbon\Carbon;
use Auth;

use \App\Helper as Hlpr;

use App\Cart;
use App\Orders;
use App\OrdersProducts;
use App\OrdersHistory;
use App\OrdersShippingInfo;

class Order {

	public $order;
	protected $token;
	protected $orderCreated;
	protected $cart;

	/**
	 * File log constructor
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T13:29:19+0800]
	 * @param  [type] $filepath [description]
	 * @param  string $mode     [description]
	 */
	public function __construct( $userToken ){
		$this->token = $userToken;
	}

	/**
	 * [create description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-09T09:59:20+0800]
	 * @return [type] [description]
	 */
	public function create( $notes = null ){
		$this->orderCreated = Carbon::now();

		$this->cart = new \stdClass();
		$this->cart->items = Cart::whereToken($this->token)->get();

		if( count($this->cart->items) > 0 ) {
			$this->order = Orders::create([
				"order_id"    => Hlpr::getUID(),
				"customer_id" => Auth::user()->user_id,
				"invoice_id"  => substr(str_shuffle(config('gp_conf.alpha_num')), 0, 30),
				"token"       => substr(str_shuffle(config('gp_conf.alpha_num')), 0, 60),
				"total"       => Cart::getTotalCartPrice( $this->token ),
				"notes"       => $notes,
				"created_at"  => $this->orderCreated
			]);
		}

		return $this;
	}

	/**
	 * [putItems description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-09T09:59:26+0800]
	 * @return [type] [description]
	 */
	public function putItems(){
		$this->order->products = new \stdClass();
		foreach($this->cart->items as $item){
			$orderedProducts = OrdersProducts::create([
				"order_product_id" => Hlpr::getUID(),
				"order_id"         => $this->order->order_id,
				"product_id"       => $item->product_id,
				"price"            => $item->price,
				"shipping_cost"    => $item->shipping_cost,
				"qty"              => $item->qty,
				"created_at"       => $this->orderCreated
			]);

			if( $orderedProducts )
				$this->order->products->list[] = $orderedProducts;
		}

		$this->order->ordered_products = new \stdClass();
		$this->order->ordered_products = \App\OrdersProducts::getUserOrders($this->order->order_id);

		return $this;
	}

	/**
	 * [removeItemsFromCart description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-09T09:59:29+0800]
	 * @return [type] [description]
	 */
	public function removeItemsFromCart(){
		Cart::where(['token' => $this->token])->delete();
		return $this;
	}

	/**
	 * [putShipping description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-09T09:59:33+0800]
	 * @param  [type] $shippingInfo [description]
	 * @return [type]               [description]
	 */
	public function putShipping( $shippingInfo ){
		$this->order->shipping = OrdersShippingInfo::create([
			"order_shipping_id" => Hlpr::getUID(),
			"order_id"          => $this->order->order_id,
			"address_1"         => $shippingInfo->address_1,
			"address_2"         => $shippingInfo->address_2,
			"city"              => $shippingInfo->city,
			"state"             => $shippingInfo->state,
			"post_code"         => $shippingInfo->post_code,
			"created_at"        => Carbon::now(),
		]);

		return $this;
	}

	/**
	 * [makeHistory description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-09T09:59:35+0800]
	 * @param  [type] $description [description]
	 * @return [type]              [description]
	 */
	public function makeHistory( $description ){
		$this->order->history = new \stdClass();
		$this->order->history->list[] = OrdersHistory::create([
			"order_history_id" => Hlpr::getUID(),
			"order_id"         => $this->order->order_id,
			"description"      => $description,
			"created_at"       => Carbon::now()
		]);

		return $this;
	}

	/**
	 * [getRecievers description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-09T09:59:40+0800]
	 * @return [type] [description]
	 */
	public function getRecievers(){
		$this->order->receivers = new \stdClass();

		$receivers = array();
		$gpShares = 0;
		$indx = 0;

		if( count($this->order->ordered_products) > 0 ){
			foreach($this->order->ordered_products as $item){
				$thePrice = ($item->total + $item->shipping_cost);
				$deduction = $thePrice * config('gp_conf.prcnt_share');

				if( config('gp_conf.isBuyNow') )
					$gpShares = $deduction;
				else
					$gpShares+=$thePrice;

				$receivers[$indx] = new Receiver();
	            // $receivers[$indx]->amount = floatval(str_replace(",", "", number_format(($thePrice - $deduction), 2)));
	            $receivers[$indx]->amount = floatval(str_replace(",", "", number_format($thePrice, 2)));
	            $receivers[$indx]->email = $item->paypal_email;
	            $receivers[$indx]->primary = (config('gp_conf.isBuyNow') ? 'true' : 'false');
	            $receivers[$indx]->invoiceId = $this->order->invoice_id;

	            $indx++;

	            // update and add commission
	            OrdersProducts::where(['order_id' => $this->order->order_id, 'product_id' => $item->product_id])->update(['commission' => $gpShares, 'updated_at' => Carbon::now()]);
			}

			$receivers[$indx] = new Receiver();
            $receivers[$indx]->amount = floatval(str_replace(",", "", number_format( $gpShares, 2 )));
            $receivers[$indx]->email = "gearplanetbuyer002@gmail.com";
            $receivers[$indx]->primary = (config('gp_conf.isBuyNow') ? 'false' : 'true');
            $receivers[$indx]->invoiceId = $this->order->invoice_id;
		}

		$this->order->receivers = $receivers;

		return $this;
	}

}