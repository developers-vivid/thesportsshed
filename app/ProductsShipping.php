<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsShipping extends Model {
  protected $table = 'products_shipping';
  protected $primaryKey = 'product_shipping_id';
  public $incrementing = false;
  protected $softDelete = true;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'product_shipping_id', 'product_id', 'country', 'city', 'state', 'policy', 'method',
  ];

  protected $dates = [
  	'created_at', 'updated_at',
  ];

  protected $hidden = [
  ];
}
