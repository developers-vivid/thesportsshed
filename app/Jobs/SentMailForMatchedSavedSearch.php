<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;
use Carbon\Carbon;

use App\Helper as Hlpr;
use App\GpLog as Log;

use App\Products;
use App\UsersSavedSearch;

class SentMailForMatchedSavedSearch extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $prodId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $prodId )
    {
        $this->prodId = $prodId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $savedSearch = UsersSavedSearch::getSavedSearch();

        $result = [];
        if( $savedSearch ) {
            foreach($savedSearch as $index => $ss) {
                $ssUrl = Hlpr::setArrayFromUrl( $ss->url );
                $searchResults = Products::getSavedSearch( 
                    (isset($ssUrl->search['keyword']) && $ssUrl->search['keyword'] ? $ssUrl->search['keyword'] : ''),
                    (isset($ssUrl->search['type']) && $ssUrl->search['type'] ? $ssUrl->search['type'] : 'all'),
                    (isset($ssUrl->search['location']) && $ssUrl->search['location'] ? $ssUrl->search['location'] : 'all'),
                    (isset($ssUrl->search['brand']) && $ssUrl->search['brand'] ? $ssUrl->search['brand'] : 'all'),
                    (isset($ssUrl->search['price_min']) && $ssUrl->search['price_min'] ? $ssUrl->search['price_min'] : ''),
                    (isset($ssUrl->search['price_max']) && $ssUrl->search['price_max'] ? $ssUrl->search['price_max'] : ''),
                    (isset($ssUrl->search['sort']) && $ssUrl->search['sort'] ? $ssUrl->search['sort'] : 'created_at|desc'),
                    $this->prodId
                );
                if( $searchResults ) {
                    $userLog = new Log('users');

                    try{
                        if( ! config('gp_conf.isLocal') ) {
                            Mail::send('mail.new-product', ['ss' => $ss, 'product' => $searchResults], function ($m) use ($ss) {
                                $m->from('gearplanet@info.com.au', 'Gear Planet');
                                $m->to($ss->user_email, $ss->user_name)->subject('New Product');
                                $m->cc('richmund@sushidigital.com.au', 'Richmund Lofranco')->subject('New Product');
                            });
                            $userLog->info(sprintf('New product has been sent to %s <%s> via %s', $ss->user_name, $ss->user_mail, $ss->url) );
                        }
                    } catch (Exception $e){
                        $userLog->error(sprintf('Error in sending email to %s <%s>', $ss->user_name, $ss->user_mail));
                    }
                }
            }
        }
    }
}
