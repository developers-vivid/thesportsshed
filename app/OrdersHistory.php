<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class OrdersHistory extends Model {
	protected $table = 'orders_history';
	protected $primaryKey = 'order_history_id';
	public $incrementing = false;
	protected $softDelete = true;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'order_history_id', 'order_id', 'description',
	];

	protected $dates = [
		'created_at', 'updated_at',
	];

	protected $hidden = [
	];

	/**
	 * [getHistory description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-05T11:04:45+0800]
	 * @param  [type] $orderId [description]
	 * @return [type]          [description]
	 */
	public static function getHistory( $orderId ) {
		return DB::table('orders_history as oh')
				->select('oh.order_history_id', 'oh.description', 'oh.created_at')
				->where('oh.order_id', $orderId)
				->orderBy('oh.created_at', 'desc')
				->get();
	}
}
