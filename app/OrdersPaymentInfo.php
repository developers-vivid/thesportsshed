<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdersPaymentInfo extends Model {
	protected $table = 'orders_payment_info';
	protected $primaryKey = 'order_payment_id';
	public $incrementing = false;
	protected $softDelete = true;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'order_payment_id',
		'order_id',
		'transaction_id',
		'transaction_status',
		'receiver_amount',
		'receiver_email',
		'receiver_primary',
		'receiver_invoice_id',
		'receiver_payment_type',
		'receiver_account_id',
		'refunded_amount',
		'pending_refund',
		'sender_transaction_id',
		'sender_transaction_status',
	];

	protected $dates = [
		'created_at', 'updated_at',
	];

	protected $hidden = [
	];
}
