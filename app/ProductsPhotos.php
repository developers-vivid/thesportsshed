<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsPhotos extends Model
{
  protected $table = 'products_photos';
  protected $primaryKey = 'photo_id';
  public $incrementing = false;
  protected $softDelete = true;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'photo_id', 
    'product_id', 
    'photo_title', 
    'photo_filename',
  ];

  protected $dates = [
  	'created_at', 'updated_at'
  ];

  protected $hidden = [
  	// 'photo_id', 'product_id',
  ];
}
