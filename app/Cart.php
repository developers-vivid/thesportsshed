<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Cart extends Model {
    protected $table = 'cart';
    protected $primaryKey = 'cart_id';
    public $incrementing = false;
    protected $softDelete = true;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'cart_id', 'token', 'product_id', 'price', 'shipping_cost', 'qty', 'expiration',
    ];

    protected $dates = [
    	'created_at', 'updated_at',
    ];

    protected $hidden = [
    	'cart_id', 'token',
    ];

    /**
     * To get cart details
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-17T11:47:09+0800]
     * @param  [type] $token [description]
     * @return [type]        [description]
     */
    public static function getCartDetails( $token ){
        $query = DB::table('cart')
                    ->select(
                        'cart.cart_id', 'cart.price as product_price', 'cart.shipping_cost', 'cart.qty', 'cart.expiration', 'cart.product_id',
                        'prod.product_title', 'prod.product_blurb',
                        'cond.name as product_condition',
                        'specs.product_brand', 'specs.product_model', 'specs.product_finish', 'specs.product_categories', 'specs.product_subcategories', 'specs.product_year', 'specs.product_colour', 'specs.product_made_in', 'specs.product_type',
                        'cat.name as category_name',
                        'photo.photo_filename'
                    )
                    ->leftJoin('products as prod', 'cart.product_id', '=', 'prod.product_id')
                    ->leftJoin('products_specs as specs', 'cart.product_id', '=', 'specs.product_id')
                    ->leftJoin('products_condition as cond', 'specs.product_condition', '=', 'cond.condition_id')
                    ->leftJoin('categories as cat', 'specs.product_categories', '=', 'cat.category_id')
                    ->leftJoin('products_photos as photo', 'prod.product_primary_photo', '=', 'photo.photo_id')
                    ->where('token', $token)
                    ->where(function($whereValidProduct){
                        $whereValidProduct
                        ->where('prod.status', 'active')
                        ->whereNull('prod.deleted_at')
                        ->whereNull('prod.cancelled_at')
                        ->whereNull('prod.sold_at');
                    })
                    ->groupBy('cart.product_id')
                    ->orderBy('cart.created_at', 'desc')
                    ->get();

        if( count($query) > 0 ){
            foreach($query as $que) {
                $que->subcategory_name = \App\Categories::whereCategoryId($que->product_subcategories)->value('name') ?: '';
            }
        }

        return $query;
    }

    /**
     * Get total cart price
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-17T14:10:52+0800]
     * @param  [type] $token [description]
     * @return [type]        [description]
     */
    public static function getTotalCartPrice( $token ){
        $query = DB::table('cart')
                    ->select(
                        DB::raw('SUM(('. config('gp_conf.db_prefix') .'cart.price + '. config('gp_conf.db_prefix') .'cart.shipping_cost) * '. config('gp_conf.db_prefix') .'cart.qty) as total')
                    )
                    ->where('token', $token)
                    ->leftJoin('products as prod', 'cart.product_id', '=', 'prod.product_id')
                    ->get();

        return isset($query[0]->total) ? $query[0]->total : 0;
    }
}
