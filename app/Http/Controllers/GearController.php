<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use Gate;
use Validator;
use Auth;
use Carbon\Carbon;

use App\Products;
use App\ProductsSpecs;
use App\ProductsPhotos;
use App\ProductsShipping;
use App\ProductsShippingCost;

class GearController extends Controller {

    protected $response;

    /**
     * [__construct description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-12T15:15:30+0800]
     */
    public function __construct(){
        $this->response = new \App\Message();
        $this->middleware('auth');
    }

    /**
     * User's Gears
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-29T13:47:10+0800]
     * @return [type] [description]
     */
    public function myGears(){
        return view('users.gears.my-gears');
    }

    /**
     * [showGearDetails description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-30T13:22:41+0800]
     * @param  [type] $gearID [description]
     * @return [type]         [description]
     */
    public function showGearDetails( $gearID ){
        $gear = Products::whereProductId($gearID);
        $gearDetails = $gear->first();
        return view('users.gears.gear-details', compact('gearDetails'));
    }

    /**
     * Edit gear form
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-31T14:01:02+0800]
     * @param  [type] $gearID [description]
     * @return [type]         [description]
     */
    public function showEditGearForm( $gearID ){
        $categories = \App\Helper::getCategories();
        $conditions = \App\Helper::getProductConditions();
        $gear       = Products::getProductDetails( $gearID, null, null, null, null, null, null, null, true );

        if( count((array)$gear) > 0 )
            return view('marketplace.gear-edit', compact('categories', 'conditions', 'gear'));
        else
            return view('errors.no-record-found', ['message' => trans('messages.gear.not_found')]);
    }

    /**
     * Update user's gear
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-31T15:36:51+0800]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updateGear( Request $request ){
        $updateAt = Carbon::now();

        if( $request->has('gear_id') && $request->get('gear_id') != '' ) {

            $validator = Validator::make((array)$request->all(),[
                'category'        => 'required',
                'type'            => 'required',
                'condition'       => 'required',
                'brand'           => 'required',
                'model'           => 'required',
                'made_in'         => 'required',
                'listing_title'   => 'required',
                'price'           => 'required',
                'description'     => 'required',
                'paypal_email'    => 'required|email|max:255',
                'city'            => 'required',
                'state'           => 'required'
            ]);
            $error = $validator->errors();

            if( count($error) > 0 )
                return $this->response->setMessage( $error->first() )->setData( ['target' => Hlpr::getFirstArrayOfErrorMessageBag($validator->errors()->toArray())] )->display();

            $updatedAt = Carbon::now();
            $gearID = $request->get('gear_id');

            $hasOtherListingTitle = Products::whereProductBlurb(str_slug($request->listing_title, "-"))->where('product_id', '!=', $gearID)->first();
            if( $hasOtherListingTitle )
                return $this->response->setMessage( trans('messages.api.gear.listing_title_taken') )->display();

            if( Products::whereProductId($gearID)->whereCreatedBy(Auth::user()->user_id)->count() > 0 ) {
                // update product/gear
                $gear = Products::where(['product_id' => $gearID])->update([
                    'product_title'       => $request->listing_title,
                    'product_blurb'       => str_slug($request->listing_title, "-"),
                    'product_description' => htmlspecialchars( $request->description, ENT_QUOTES ),
                    'product_price'       => $request->price,
                    'product_sale_price'  => ( is_numeric($request->sale_price) && $request->sale_price > 0 ? $request->sale_price : null ),
                    'shipping_cost'       => ( isset($request->shipping_costs) && is_numeric($request->shipping_costs) && floatval($request->shipping_costs) > 0 ? floatval($request->shipping_costs) : null ),
                    // 'accept_offers'       => $request->accept_offers,
                    'paypal_email'        => $request->paypal_email,
                    'shopping_policy'     => htmlspecialchars( $request->shopping_policy, ENT_QUOTES ),
                    'updated_at'          => $updatedAt
                ]);

                // update product specs
                $gearSpec = ProductsSpecs::where(['product_id' => $gearID])->update([
                    'product_condition'     => $request->condition,
                    'product_brand'         => $request->brand,
                    'product_model'         => $request->model,
                    'product_categories'    => $request->category,
                    'product_subcategories' => $request->sub_category,
                    'product_year'          => $request->year,
                    'product_colour'        => $request->colour,
                    'product_made_in'       => $request->made_in,
                    'product_type'          => $request->type,
                    'updated_at'            => $updatedAt
                ]);

                // update shipping
                $shippingInfoDetail = [
                    "country" => 'Australia',
                    "city"    => $request->city,
                    "state"   => $request->state,
                    "policy"  => $request->shipping_policy
                ];

                // get shipping method
                if( ($request->has('shipping') && $request->shipping == 'on') && $request->has('local_pickup') && $request->local_pickup == 'on' )
                    $shippingInfoDetail['method'] = 'all';

                else if( $request->has('shipping') && $request->shipping == 'on' )
                    $shippingInfoDetail['method'] = 'shipping';

                else if( $request->has('local_pickup') && $request->local_pickup == 'on' )
                    $shippingInfoDetail['method'] = 'local pickup';

                $productShippingInfo = ProductsShipping::where(['product_id' => $gearID])->update($shippingInfoDetail);

                // if product does not exist in shipping info
                if( ! $productShippingInfo ) {
                    $shippingInfoDetail['product_shipping_id'] = \App\Helper::getUID();
                    $shippingInfoDetail['product_id'] = $gearID;
                    $shippingInfoDetail['created_at'] = Carbon::now();

                    ProductsShipping::create($shippingInfoDetail);
                }

                // remove shipping cost before update
                ProductsShippingCost::where(['product_id' => $gearID])->delete();

                return $this->response->setMessage( trans('messages.gear.updated') )->setData( ['gear_id' => $gearID, 'redirect' => route('mygears::ads') ] )->setSuccess()->display();
            }
            return $this->response->setMessage( trans('messages.api.error.went_wrong') )->display();
        }
        return $this->response->setMessage( trans('messages.gear_not_updated') )->display();
    }

    /**
     * To remove gear photo
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-16T14:35:29+0800]
     * @return [type] [description]
     */
    public function removePhoto( Request $request ){
        if( $request->has('id') && $request->id != '' ) {
            $deletePhoto = \App\Helper::removeGearPhoto( $request->id );

            if( ! is_null( $deletePhoto ) )
                return $this->response->setMessage( trans('messages.gear.photo.removed') )->setSuccess()->display();
            else
                return $this->response->setMessage( trans('messages.gear.photo.not_removed') )->display();
        }
        return $this->response->setMessage( trans('messages.gear.photo.nothing_to_remove') )->display();
    }

    /**
     * To remove gear photo
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-16T14:35:29+0800]
     * @return [type] [description]
     */
    public function selectPrimaryPhoto( Request $request ){
        if( $request->has('id') && $request->id != '' ) {
            $photoDetails = ProductsPhotos::wherePhotoId( $request->id )->first();

            if( Products::whereProductId($photoDetails->product_id)->whereProductPrimaryPhoto($photoDetails->photo_id)->count() == 0 ){
                Products::where('product_id', $photoDetails->product_id )->update(['product_primary_photo' => $photoDetails->photo_id]);
                return $this->response->setMessage( trans('messages.gear.photo.primary.updated') )->setSuccess()->display();
            }
            return $this->response->setMessage( trans('messages.gear.photo.primary.not_updated') )->display();
        }
        return \App\Helper::handleResponse( 'Nothing to remove.' );
    }

    /**
     * [activateProduct description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-10T11:57:05+0800]
     * @param  [type] $productId [description]
     * @return [type]            [description]
     */
    public function activateProduct( $productId ) {
        $adDetail = Products::whereProductId( $productId )->first();

        if (Gate::denies('activate-ad', $adDetail)) {
            $this->response->setMessage( trans('messages.ad.not_activated') )->flash();
            return redirect()->route('mygears::ads');
        }

        $adDetail->status = 'active';
        $adDetail->sold_at = null;
        $adDetail->deleted_at = null;
        $adDetail->cancelled_at = null;
        $adDetail->updated_at = Carbon::now();

        $adDetail->save();

        $this->response->setMessage( trans('messages.ad.activated') )->setSuccess()->flash();
        return redirect()->back();
    }

    /**
     * [cancelProduct description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-10T11:57:33+0800]
     * @param  [type] $productId [description]
     * @return [type]            [description]
     */
    public function cancelProduct( $productId ) {
        $adDetail = Products::whereProductId( $productId )->first();

        if (Gate::denies('cancel-ad', $adDetail)) {
            $this->response->setMessage( trans('messages.ad.not_cancelled') )->flash();
            return redirect()->route('mygears::ads');
        }
        
        $adDetail->status = 'cancelled';
        $adDetail->updated_at = Carbon::now();
        $adDetail->cancelled_at = Carbon::now();

        $adDetail->save();

        $this->response->setMessage( trans('messages.ad.cancelled') )->setSuccess()->flash();
        return redirect()->back();
    }

    /**
     * [deleteProduct description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-10T11:57:37+0800]
     * @param  [type] $productId [description]
     * @return [type]            [description]
     */
    public function deleteProduct( $productId ) {
        $adDetail = Products::whereProductId( $productId )->first();

        if (Gate::denies('delete-ad', $adDetail)) {
            $this->response->setMessage( trans('messages.ad.not_deleted') )->flash();
            return redirect()->route('mygears::ads');
        }
        $adDetail->status = 'inactive';
        $adDetail->save();
        $adDetail->delete();

        $this->response->setMessage( trans('messages.ad.deleted') )->setSuccess()->flash();
        return redirect()->back();
    }

}
