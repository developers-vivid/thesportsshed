<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Carbon\Carbon;
use \App\Helper as Hlpr;

// Models
use App\User;

class AdminController extends Controller
{
	/**
	 * [__construct description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-10T11:58:55+0800]
	 */
  public function __construct(){
  	$this->middleware('auth');
  }

  /**
   * [index description]
   * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
   * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-10T11:59:06+0800]
   * @return [type] [description]
   */
  public function index(){
  	return view('admin.dashboard');
  }

  /**
   * Get users
   * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
   * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-10T14:11:07+0800]
   * @return [type] [description]
   */
  public function getUsers(){
  	$users = User::whereRoleId('56aebc46f2827')->paginate(10);
  	return view('admin.users.list', compact('users'));
  }

  /**
   * User registration
   * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
   * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-11T10:30:59+0800]
   * @return [type] [description]
   */
  public function userRegister(Request $request){
    if( $request->isMethod('post') ){
      
    }
    else {
      return view('admin.users.register');
    }
  }

  /**
   * To change user status
   * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
   * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-16T08:55:57+0800]
   * @param  [type] $userID        [description]
   * @return [type]                [description]
   */
  public function updateUserStatus( $userID ){
    $userDetails = User::whereUserId($userID)->first();
    if( count($userDetails) > 0 ) {
      User::whereUserId($userID)->update(["status" => ($userDetails->status == 'active' ? 'inactive' : 'active'),"updated_at" => Carbon::now()]);
      Hlpr::handleResponse($userDetails->name ." has been ". ($userDetails->status == 'active' ? 'DEACTIVATED' : 'ACTIVATED'), false, [], true);
    }
    return redirect()->back();
  }
}
