<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use PayPal\Core\PPHttpConfig;
use PayPal\Service\AdaptivePaymentsService;
use PayPal\Types\AP\PayRequest;
use PayPal\Types\AP\Receiver;
use PayPal\Types\AP\ReceiverList;
// use PayPal\Types\AP\ShippingAddressInfo;
use PayPal\Types\AP\AddressList;
use PayPal\Types\Common\RequestEnvelope;

use \App\GpLog as Log;
use \App\Helper as Hlpr;
use \App\Order as UserOrder;
use Carbon\Carbon;

use Session;
use Auth;
use PayPal;
use Redirect;
use DB;
use Validator;

use App\User;
use App\Products;
use App\Cart;
use App\Orders;
use App\OrdersProducts;
use App\OrdersHistory;
use App\OrdersPaymentInfo;
use App\UsersShippingAddress;

class CartController extends Controller {

    protected $gpPaypal;
	/**
	 * [__construct description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-17T09:40:27+0800]
	 */
    public function __construct(){
        $this->gpPaypal = new \App\PayPal();
    	  $this->middleware('auth', ['only' => []]);
        $this->response = new \App\Message();
    }

    /**
     * Checkout Form
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-17T11:17:16+0800]
     * @return [type] [description]
     */
    public function showCartForm(){
    	$cart = Cart::getCartDetails( (string)Session::get('_token') );
        $shipping = UsersShippingAddress::whereUserId( (string)Session::get('_token') )->first();
    	return view('cart.checkout', compact('cart', 'shipping'));
  	}

  	/**
  	 * To update cart -> product qty via checkout
  	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
  	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-17T13:46:09+0800]
  	 * @return [type] [description]
  	 */
  	public function updateProductQty( Request $request ) {
  		if( $request->has('id') && $request->has('qty') ){
  			if( $request->id != '' && (int)$request->qty > 0 ){
  				$cartDetails = Cart::whereToken(Session::get('_token'))->whereProductId($request->id)->first();

  				if( count($cartDetails) > 0 ){
  					$cartDetails->update(['qty' => $request->qty, 'updated_at' => Carbon::now()]);
  					$productDetails = Products::whereProductId($request->id)->first();

  					return Hlpr::handleResponse( trans('messages.cart.product_qty.updated'), false, [
  						'new_price' => number_format(($productDetails->product_price * $request->qty), 2),
  						'total_price' => number_format((Cart::getTotalCartPrice((string)Session::get('_token'))), 2)
  					]);
  				}
  				else {
  					return Hlpr::handleResponse( trans('messages.cart.gear_not_exist') );
  				}
  			}
  		}
  		return Hlpr::handleResponse( trans('messages.cart.product_qty.not_updated') );
  	}

    /**
     * Handles add to cart
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-17T09:40:42+0800]
     * @return [type] [description]
     */
    public function postAddToCart( Request $request ){
    	if( $request->has('gear') && $request->gear != '' ) {
    		$gearID = \Crypt::decrypt($request->gear);
    		$cartDetails = Cart::whereToken((string)Session::get('_token'))->whereProductId($gearID)->first();
    		$productDetails = Products::whereProductId($gearID)->first();
    		
    		if( count($cartDetails) >= 1 ) {
                if( config('gp_conf.isBuyNow') ) {
                    $this->insertProductToCart( $gearID, $productDetails );
                }
                else {
                    $cartDetails->increment('qty', 1, ['updated_at' => Carbon::now()]);
                    return Hlpr::handleResponse( trans('messages.cart.updated'), false, ['total' => \App\Cart::whereToken(Session::get('_token'))->count()] );                    
                }
    		}
    		else {
    			$this->insertProductToCart( $gearID, $productDetails );
    		}
    	}
    	return Hlpr::handleResponse( trans('messages.cart.gear_not_exist') );
    }

    /**
     * To insert product into cart
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-08T12:02:27+0800]
     * @return [type] [description]
     */
    private function insertProductToCart( $gearID, $productDetails, $qty = 1 ){
        $createdAt = Carbon::now();

        if( config('gp_conf.isBuyNow') )
            Cart::where(['token' => (string)Session::get('_token')])->delete();

        $cart = Cart::create([
            "cart_id"       => Hlpr::getUID(),
            "token"         => (string)Session::get('_token'),
            "product_id"    => $gearID,
            "price"         => ( !is_null($productDetails->product_sale_price) && floatval($productDetails->product_sale_price) > 0 ? floatval($productDetails->product_sale_price) : $productDetails->product_price ),
            "shipping_cost" => (! is_null($productDetails->shipping_cost) ? $productDetails->shipping_cost : 0),
            "qty"           => $qty,
            "expiration"    => $createdAt->addHours(24),
            "created_at"    => $createdAt
        ]);

        return Hlpr::handleResponse( trans('messages.cart.added'), false, ['total' => \App\Cart::whereToken(Session::get('_token'))->count()] );
    }

    /**
     * Remove product from cart
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T14:33:35+0800]
     * @return [type] [description]
     */
    public function removeProductFromCart( Request $request ){
    	if( $request->has('id') && $request->id != '' ){
    		$wasRemoved = Cart::where(['cart_id' => $request->id])->delete();

    		if( $wasRemoved )
    			return Hlpr::handleResponse( trans('messages.cart.product.removed'), false, [
    				'total' => \App\Cart::whereToken(Session::get('_token'))->count(),
    				'total_price' => number_format((Cart::getTotalCartPrice((string)Session::get('_token'))), 2)
    			]);
    	}
    	return Hlpr::handleResponse( trans('messages.cart.product.not_removed') );
    }

    /**
     * Checkout cart
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-19T15:00:58+0800]
     * @return [type] [description]
     */
    public function checkoutCart( Request $request ){
    	if( Auth::check() ){	// only for logged in users

            $validator = Validator::make(array(
                "address_1" => $request->address_1,
                "city"      => $request->city,
                "state"     => $request->state,
                "post_code" => $request->post_code
            ), array(
                "address_1" => 'required|min:3',
                "city"      => 'required',
                "state"     => 'required',
                "post_code" => 'required'
            ));

            // $error = $validator->errors()->all();
            $error = $validator->errors();

            if( count($error) > 0 )
                // dd( Hlpr::getArrayOfErrorMessageBag( $error->toArray() ) );
                // return Hlpr::handleResponse( $error[0] );
                return $this->response->setMessage( Hlpr::getAllValidationError($error, false) )->setData(['target' => Hlpr::getArrayOfErrorMessageBag( $error->toArray() ) ])->display();
                // return Hlpr::handleResponse( $error[0], true, ['target' => Hlpr::getFirstArrayOfErrorMessageBag($validator->errors()->toArray())] );
            
    		$totalItems = \App\Cart::whereToken(Session::get('_token'))->count();
    		if( $totalItems > 0 ) {
    			$ppLog = new Log('gp_paypal');

		        try {
                    // set order shipping and update user shipping info
                    $shippingInfoData = Hlpr::updateOrderShippingInfo(
                        $request->address_1,
                        $request->address_2,
                        $request->city,
                        $request->state,
                        $request->post_code,
                        Auth::user()->user_id,
                        null
                    );

                    DB::beginTransaction();

		        	$userOrder = new UserOrder( (string)Session::get('_token') );
		        	$order = $userOrder
		        				->create( ($request->has('notes') ? $request->notes : null) )
		        				->putItems()
		        				->putShipping((object)$shippingInfoData)
		        				->makeHistory('New order created');

		            PPHttpConfig::$DEFAULT_CURL_OPTS[CURLOPT_SSLVERSION] = 6;
		            $payRequest = new PayRequest();

                    // $shipping                = new ShippingAddressInfo();
                    // $shipping->addresseeName = Auth::user()->name;
                    // $shipping->street1       = $request->address_1;
                    // $shipping->street2       = $request->address_2;
                    // $shipping->city          = $request->city;
                    // $shipping->zip           = $request->post_code;
                    // $shipping->country       = 'Australia';

                    $shippingList            = new AddressList($shipping);
                    $payRequest->addressList = $shippingList;

                    $orderReceivers           = $order->getRecievers();
                    $receiverList             = new ReceiverList($orderReceivers->order->receivers);
                    $payRequest->receiverList = $receiverList;

		            $requestEnvelope = new RequestEnvelope("en_AU");
                    $payRequest->requestEnvelope    = $requestEnvelope;
                    $payRequest->actionType         = "PAY";
                    $payRequest->cancelUrl          = route('cart::cancel', [$order->order->order_id, $order->order->token]);
                    $payRequest->returnUrl          = route('cart::done', [$order->order->order_id, $order->order->token]);
                    $payRequest->currencyCode       = "AUD";
                    $payRequest->ipnNotificationUrl = route('cart::get-transaction-details', [$order->order->order_id, $order->order->token]);

		            $adaptivePaymentsService = new AdaptivePaymentsService( config('gp_conf.pp_adaptive_payment') );
		            $payResponse = $adaptivePaymentsService->Pay($payRequest);

		            if ($payResponse->responseEnvelope->ack == "Success") {
                        Orders::where(['order_id' => $order->order->order_id])->update(['status' => config('gp_conf.orders.status.processing'), 'paypal_correlation_id' => $payResponse->responseEnvelope->correlationId, 'paypal_pay_key' => $payResponse->payKey, 'updated_at' => Carbon::now()]);

		            	DB::commit();	// commit db transaction

		            	$ppLog->write('Adaptive Payments: ['. $payResponse->payKey .']');

		            	$order->removeItemsFromCart();	// remove items from cart
		                return Hlpr::handleResponse('PayPal success', false, ['redirectUrl' => $this->gpPaypal->getRedirectToPayPalUrl($payResponse->payKey)]);
		            }
		            else {
		            	DB::rollBack();		// rollback db transaction
		            	$err = [];

		            	$ppLog->error('Checked out by: '. Auth::user()->name .' <'. Auth::user()->email .'>');
		                foreach($payResponse->error as $error){
		                	$errMsg = $error->category .' '. $error->severity .'! '. $error->message;
		                	$err[] = $errMsg;

		                    $ppLog->error('Adaptive Payments: ['. $error->errorId .']['. $errMsg .']');
		                }

		                $ppLog->endLog();

		                return Hlpr::handleResponse( implode("<br/><br/>", $err) );
		            }
		        }   catch(\PayPal\Exception\PPConnectionException $e){
		            $ppLog->error( $e->getMessage() )->endLog();
		            return Hlpr::handleResponse( $e->getMessage() );
		        }
    		}
    		else {
    			return Hlpr::handleResponse( trans('messages.cart.empty') );
    		}
    	}
    	else {
    		return Hlpr::handleResponse( trans('messages.checkout.logged_in_users') );
    	}
    }

    /**
     * Handles done paypal chained transaction done
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-08T09:07:57+0800]
     * @param  [type] $orderId [description]
     * @return [type]          [description]
     */
    public function getTransactionDone( $orderId, $orderToken = '' ){
        $orderDetails = Orders::whereOrderId($orderId)->whereToken($orderToken)->first();
        $orderLog = new Log('order');

        if( is_object($orderDetails) ) {
            $response = $this->gpPaypal->getPaymentDetails( $orderDetails->paypal_pay_key );
            if( is_object($response) && $response->responseEnvelope->ack == "Success" ) {

                foreach($response->paymentInfoList->paymentInfo as $paymentInfo) {
                    $orderPaymentInfo = [
                        "order_payment_id"      => Hlpr::getUID(),
                        "order_id"              => $orderId,
                        "receiver_amount"       => $paymentInfo->receiver->amount,
                        "receiver_email"        => $paymentInfo->receiver->email,
                        "receiver_primary"      => $paymentInfo->receiver->primary,
                        "receiver_invoice_id"   => $paymentInfo->receiver->invoiceId,
                        "receiver_payment_type" => strtolower( $paymentInfo->receiver->paymentType ),
                        "receiver_account_id"   => $paymentInfo->receiver->accountId,
                        "pending_refund"        => $paymentInfo->pendingRefund,
                        "created_at"            => Carbon::now(),
                        "updated_at"            => Carbon::now()
                    ];

                    if( isset($paymentInfo->transactionId) && $paymentInfo->transactionId != '' )
                        $orderPaymentInfo['transaction_id'] = $paymentInfo->transactionId;

                    if( isset($paymentInfo->transactionStatus) && $paymentInfo->transactionStatus != '' )
                        $orderPaymentInfo['transaction_status'] = strtolower($paymentInfo->transactionStatus);

                    if( isset($paymentInfo->refundedAmount) && $paymentInfo->refundedAmount != '' )
                        $orderPaymentInfo['refunded_amount'] = $paymentInfo->refundedAmount;

                    if( isset($paymentInfo->senderTransactionId) && $paymentInfo->senderTransactionId != '' )
                        $orderPaymentInfo['sender_transaction_id'] = $paymentInfo->senderTransactionId;

                    if( isset($paymentInfo->senderTransactionStatus) && $paymentInfo->senderTransactionStatus != '' )
                        $orderPaymentInfo['sender_transaction_status'] = strtolower( $paymentInfo->senderTransactionStatus );

                    // Create order payment Info
                    OrdersPaymentInfo::create( $orderPaymentInfo );
                }

                $orderLog->success('Order has been successfully done.');
                return Hlpr::updateOrder( $orderId, true, $response );
            }
            else {
                $orderLog->error('Did not execute successfully. No response received from PayPal.');
            }
        }
        return view('errors.no-record-found', ['message' => 'Order does not exist or token mismatch!']);
    }

    /**
     * Handles done paypal chained transaction cancel
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-08T09:07:57+0800]
     * @param  [type] $orderId [description]
     * @return [type]          [description]
     */
    public function getTransactionCancel( $orderId, $orderToken = ''){
        $orderDetails = Orders::whereOrderId($orderId)->whereToken($orderToken)->first();
        $orderLog = new Log('order');

        if( is_object($orderDetails) ) {
            $response = $this->gpPaypal->getPaymentDetails( $orderDetails->paypal_pay_key );

            if( is_object($response) && $response->responseEnvelope->ack == "Success" ) {
                // $orderStatus = $this->gpPaypal->getPaypalResponseStatusValue( $response->status );

                if( strtolower($response->status) == "created" ) {
                    foreach($response->paymentInfoList->paymentInfo as $paymentInfo) {
                        $orderPaymentInfo = [
                            "order_payment_id"      => Hlpr::getUID(),
                            "order_id"              => $orderId,
                            "receiver_amount"       => $paymentInfo->receiver->amount,
                            "receiver_email"        => $paymentInfo->receiver->email,
                            "receiver_primary"      => $paymentInfo->receiver->primary,
                            "receiver_invoice_id"   => $paymentInfo->receiver->invoiceId,
                            "receiver_payment_type" => strtolower( $paymentInfo->receiver->paymentType ),
                            "receiver_account_id"   => $paymentInfo->receiver->accountId,
                            "pending_refund"        => $paymentInfo->pendingRefund,
                            "created_at"            => Carbon::now(),
                            "updated_at"            => Carbon::now()
                        ];

                        if( isset($paymentInfo->transactionId) && $paymentInfo->transactionId != '' )
                            $orderPaymentInfo['transaction_id'] = $paymentInfo->transactionId;

                        if( isset($paymentInfo->transactionStatus) && $paymentInfo->transactionStatus != '' )
                            $orderPaymentInfo['transaction_status'] = strtolower($paymentInfo->transactionStatus);
                        else
                            $orderPaymentInfo['transaction_status'] = 'created';

                        if( isset($paymentInfo->refundedAmount) && $paymentInfo->refundedAmount != '' )
                            $orderPaymentInfo['refunded_amount'] = $paymentInfo->refundedAmount;

                        if( isset($paymentInfo->senderTransactionId) && $paymentInfo->senderTransactionId != '' )
                            $orderPaymentInfo['sender_transaction_id'] = $paymentInfo->senderTransactionId;

                        if( isset($paymentInfo->senderTransactionStatus) && $paymentInfo->senderTransactionStatus != '' )
                            $orderPaymentInfo['sender_transaction_status'] = strtolower( $paymentInfo->senderTransactionStatus );

                        // Create order payment Info
                        OrdersPaymentInfo::create( $orderPaymentInfo );
                    }
                }

                $orderLog->success('Order has been successfully cancelled.');
                return Hlpr::updateOrder( $orderId );
            }
            else {
                $orderLog->error('Did not execute successfully. No response received from PayPal.');
            }
        }
        return view('errors.no-record-found', ['message' => 'Order does not exist or token mismatch!']);
    }

    /**
     * Handles get chained paypal transaction details
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-08T15:01:20+0800]
     * @param  [type] $orderId [description]
     * @return [type]          [description]
     */
    public function getTransaction( $orderId, $token = '' ){
        $ppLog = new Log('gp_paypal');
        $ppLog->write('Adaptive Payments: Sent thru IPN['. $orderId .']['. $token .']')->endLog();
    }
}
