<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use Auth;
use Mail;
use Carbon\Carbon;

use App\Helper as Hlpr;

use App\User;
use App\UsersProfile;
use App\UsersCompany;
use App\PasswordResets;


class UsersController extends Controller {

	protected $pusher, $response;

	/**
	 * Constructor
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-29T14:01:37+0800]
	 */
	public function __construct(){
		$this->middleware('auth', ['except' => [
			'forgotPassword',
			'forgotPasswordSendMail',
			'showChangePassForm',
			'updateChangePass',
		]]);

        $this->response = new \App\Message();
	}

	/**
	 * User's Profile
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-29T14:02:05+0800]
	 * @return [type] [description]
	 */
	public function myProfile(){
        $userDetails = User::getFullDetails( Auth::user()->user_id );
       
		return view('users.user.profile', compact('userDetails'));
	}

	/**
     * [forgotPassword description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-22T11:37:51+0800]
     * @return [type] [description]
     */
    public function forgotPassword(){
        return view('auth.forgot-password');
    }

    /**
     * [forgotPasswordSendMail description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-22T11:38:11+0800]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function forgotPasswordSendMail( Request $request ) {
        $validator = Validator::make((array)$request->all(), ['email' => 'required|email']);

        $error = $validator->errors();

        if( count($error) > 0 )
            return Hlpr::handleResponse( $error->first() );

        $userDetails = User::whereEmail( $request->email )->first();
        if( $userDetails ) {

            $hashToken = substr(str_shuffle(config('gp_conf.short_random')), 0, 3);
            $hashToken .= '-'. substr(str_shuffle(config('gp_conf.short_random')), 0, 32);

            try{
                if( ! config('gp_conf.isLocal') ) {
                    $sent = Mail::send('mail.change-password', ['user' => $userDetails, 'url' => route('user::changePass', [$hashToken])], function ($m) use ($userDetails) {
                        $m->from('gearplanet@info.com.au', 'Gear Planet');
                        $m->to($userDetails->email, $userDetails->name)->subject('Password Reset');
                        if( !config('gp_conf.allseller') ) {
                            // $m->bcc('richmundlofranco@gmail.com', 'Richmund Lofranco');
                        }
                    });
                }
            } catch (Exception $e){
              // dd($e->getMessage());
            }

            // update/store reset token
            $resetDetails = PasswordResets::whereEmail( $request->email )->first();
            if( $resetDetails ) {   // if existed, do update
                $resetDetails->token = $hashToken;
                $resetDetails->created_at = Carbon::now();
                $resetDetails->updated_at = Carbon::now();
                $resetDetails->save();
            }
            else {
                $resetDetails = PasswordResets::create([
                    "password_reset_id" => Hlpr::getUID(),
                    "email" => $request->email,
                    "token" => $hashToken,
                    "created_at" => Carbon::now()
                ]);
            }

            if( $resetDetails )
                return Hlpr::handleResponse( trans('messages.user.change_pass.email_sent'), false );
            else
                return Hlpr::handleResponse( trans('messages.user.change_pass.error'), false );
        }

        return Hlpr::handleResponse( trans('messages.user.change_pass.email_not_exist') );
    }

    /**
     * [showChangePassForm description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-22T15:31:01+0800]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function showChangePassForm( Request $request, $token ){
    	if( Auth::check() )
    		return redirect()->route('home');

    	$resetDetails = PasswordResets::whereToken( $token )->first();
    	if( $resetDetails ){
    		return view('auth.change-password', compact('token'));
    	}
    	return view('errors.no-record-found', ['message' => trans('messages.user.change_pass.invalid_token')]);    	
    }

    /**
     * [updateChangePass description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-22T15:49:14+0800]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updateChangePass( Request $request, $token ){
    	if( Auth::check() )
    		return Hlpr::handleResponse( 'Invalid request' );

    	$validator = Validator::make((array)$request->all(), ['password' => 'required|confirmed|min:6']);

        $error = $validator->errors();

        if( count($error) > 0 )
            return Hlpr::handleResponse( $error->first() );

    	$resetDetails = PasswordResets::whereToken( $token )->first();
    	if( $resetDetails ){
    		$userDetails = User::whereEmail( $resetDetails->email )->first();

    		if( $userDetails ) {
	    		$userDetails->password = bcrypt( $request->password );
	    		$userDetails->updated_at = Carbon::now();

	    		$updated = $userDetails->save();
	    		if( $updated ) {
	    			$resetDetails->delete();
	    			Auth::loginUsingId( $userDetails->user_id );
	    			return Hlpr::handleResponse( trans('messages.user.change_pass.success'), false, ['redirect' => route('mygears::ads')] );
	    		}
	    		else			
	    			return Hlpr::handleResponse( trans('messages.user.change_pass.error') );
    		}
    	}
    	return Hlpr::handleResponse( trans('messages.user.change_pass.invalid_token') );
    }

    public function saveSettings( Request $request ){

        // if( Auth::check() )
        //     return Hlpr::handleResponse( 'Invalid request' );

        $validator = Validator::make((array)$request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'company_name' => 'required',
            ]);

        $error = $validator->errors();

        if( count($error) > 0 )
            return $this->response->setMessage( $error->first() )->setData(['target' => Hlpr::getFirstArrayOfErrorMessageBag($validator->errors()->toArray())])->display();

        if( !$request->has( 'userid' ) )
            return $this->response->setMessage( 'User id is missing' )->display();

        $userDetails = User::where(['user_id' => $request->userid])->update([
            'name' => $request->first_name .' '. $request->last_name,
            'updated_at' => Carbon::now()
        ]);

        $userProfile = UsersProfile::where(['user_id' => $request->userid])->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'telephone' => $request->telephone,
            'updated_at' => Carbon::now(),
        ]);

        $usersCompany = UsersCompany::where(['user_id' => $request->userid])->update([
            'name' => $request->company_name,
            'state' => $request->company_state,
            'city' => $request->company_city,
            'post_code' => $request->company_postcode,
        ]);

        $usersSetting = new \App\Settings( $request->userid );
        $usersSetting->setMessageNotification( strtolower( $request->email_message_notification ) )->save();

        return $this->response->setMessage( trans('messages.account_settings.successfully_saved') )->setSuccess()->display();
    }
}
