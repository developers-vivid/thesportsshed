<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Validator;
use Input;

use App\Http\Requests;
use Eventviva\ImageResize;
use Carbon\Carbon;
use \App\Helper as Hlpr;

use App\Jobs\SentMailForMatchedSavedSearch;

use App\User;
use App\UsersSavedSearch;
use App\Products;
use App\ProductsSpecs;
use App\ProductsPhotos;
use App\ProductsShipping;
use App\ProductsShippingCost;
use App\Categories;

class MarketPlaceController extends Controller {

    protected $response;

    /**
     * [__construct description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-10T09:19:08+0800]
     */
    public function __construct() {
    	$this->middleware('auth', [
            'only' => ['postNewGear']
        ]);

        // instantiate response
        $this->response = new \App\Message();
    }

    /**
     * [index description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-10T09:19:11+0800]
     * @return [type] [description]
     */
    public function index() {
        return view('marketplace.home');
    }

    /**
     * [showMarketPlaceGearDetails description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-10T09:19:14+0800]
     * @param  [type] $gearTitle [description]
     * @return [type]            [description]
     */
    public function showMarketPlaceGearDetails( $gearTitle ){
        $gear = Products::getProductDetails(null, $gearTitle);

        if( count((array)$gear) > 0 ) {
            $relatedItems = Products::getRelatedItems( $gear->product_id, $gear->category_id );
            return view('marketplace.gear-details', compact('gear', 'relatedItems'));
        }
        return view('errors.no-record-found', ['message' => trans('messages.gear.not_found')]);
    }

    /**
     * Landing page to create gear
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-16T14:26:34+0800]
     * @return [type] [description]
     */
    public function showCreateGearForm(){
        if( Auth::check() ) {
            $categories = Hlpr::getCategories();
            $conditions = Hlpr::getProductConditions();
            return view('users.gears.sell', compact('categories', 'conditions'));      
        }
        return view('errors.no-record-found', ['message' => 'Please login to sell']);
    }

    /**
     * To display all gears
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-17T11:01:34+0800]
     * @param  [type] $category     [description]
     * @param  string $sub_category [description]
     * @return [type]               [description]
     */
    public function showAllGears( $category = null, $sub_category = 'all' ){
        $gears = Products::getGearsByCategory( 
                    $category, 
                    $sub_category, 
                    (Input::has('type') ? Input::get('type') : 'all'), 
                    (Input::has('location') ? Input::get('location') : 'all'), 
                    (Input::has('brand') ? Input::get('brand') : 'all'), 
                    (Input::has('price_min') ? Input::get('price_min') : ''), 
                    (Input::has('price_max') ? Input::get('price_max') : ''), 
                    (Input::has('sort') ? Input::get('sort') : 'created_at|desc'));

        return view('marketplace.products', compact('gears', 'category', 'sub_category'));
    }

    /**
     * Handles search keywords
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-04-29T09:54:31+0800]
     * @return [type] [description]
     */
    public function displaySearchedItems() {
        $gears = Products::searchGears( 
                    (Input::has('keyword') ? Input::get('keyword') : 'all'), 
                    (Input::has('type') ? Input::get('type') : 'all'),
                    (Input::has('location') ? Input::get('location') : 'all'),
                    (Input::has('brand') ? Input::get('brand') : 'all'),
                    (Input::has('price_min') ? Input::get('price_min') : ''),
                    (Input::has('price_max') ? Input::get('price_max') : ''),
                    (Input::has('sort') ? Input::get('sort') : 'created_at|desc'));
        return view('marketplace.search', compact('gears'));
    }

    /**
     * [postNewGear description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-10T09:21:13+0800]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postNewGear( Request $request ) {
        $theRequest = (array)$request->all();
        $theRequest['listing_title'] = str_slug($theRequest['listing_title'], "-");

        $validator = Validator::make($theRequest,[
            'category'      => 'required',
            'type'          => 'required',
            'condition'     => 'required',
            'brand'         => 'required',
            'model'         => 'required',
            'made_in'       => 'required',
            'listing_title' => 'required|unique:products,product_blurb',
            'price'         => 'required|numeric|min:1',
            'description'   => 'required',
            'paypal_email'  => 'required|email|max:255',
            'city'          => 'required',
            'state'         => 'required', 
            'photos'        => 'required|numeric|min:1'
        ]);
        $error = $validator->errors();

        if( count($error) > 0 )
            return $this->response->setMessage( $error->first() )->setData( ['target' => Hlpr::getFirstArrayOfErrorMessageBag($validator->errors()->toArray())] )->display();

        $createdAt = Carbon::now();

        // create product/gear
        $gear = Products::create([
            'product_id'          => Hlpr::getUID(),
            'product_title'       => $request->listing_title,
            'product_blurb'       => str_slug($request->listing_title, "-"),
            'product_description' => $request->description,
            'product_price'       => $request->price,
            'product_sale_price'  => ( is_numeric($request->sale_price) && $request->sale_price > 0 ? $request->sale_price : null ),
            'shipping_cost'       => ( isset($request->shipping_costs) && is_numeric($request->shipping_costs) && floatval($request->shipping_costs) > 0 ? floatval($request->shipping_costs) : null ),
            // 'accept_offers'       => $request->accept_offers,
            'paypal_email'        => $request->paypal_email,
            'shopping_policy'     => $request->shopping_policy,
            'created_by'          => Auth::user()->user_id,
            'created_at'          => $createdAt
        ]);

        // create product specs
        $gearSpec = ProductsSpecs::create([
            'product_specs_id'      => Hlpr::getUID(),
            'product_id'            => $gear->product_id,
            'product_condition'     => $request->condition,
            'product_brand'         => $request->brand,
            'product_model'         => $request->model,
            'product_categories'    => $request->category,
            'product_subcategories' => $request->sub_category,
            'product_year'          => $request->year,
            'product_colour'        => $request->colour,
            'product_made_in'       => $request->made_in,
            'product_type'          => $request->type,
            'created_at'            => $createdAt
        ]);

        // shipping
        $shippingInfoDetail = [
            "product_shipping_id" => Hlpr::getUID(),
            "product_id" => $gear->product_id,
            "country"    => 'Australia',
            "city"       => $request->city,
            "state"      => $request->state,
            "policy"     => $request->shipping_policy
        ];

        // get shipping method
        if( ($request->has('shipping') && $request->shipping == 'on') && $request->has('local_pickup') && $request->local_pickup == 'on' )
            $shippingInfoDetail['method'] = 'all';

        else if( $request->has('shipping') && $request->shipping == 'on' )
            $shippingInfoDetail['method'] = 'shipping';

        else if( $request->has('local_pickup') && $request->local_pickup == 'on' )
            $shippingInfoDetail['method'] = 'local pickup';

        $productShippingInfo = ProductsShipping::create( $shippingInfoDetail );

        // send an email that match to user saved search
        $this->dispatch( new SentMailForMatchedSavedSearch( $gear->product_id ) );

        return $this->response->setMessage( trans('messages.gear_created') )->setData( ['gear_id' => $gear->product_id, 'redirect_url' => route('mygears::ads')] )->setSuccess()->display();
        // return $this->response->setMessage( trans('messages.gear_created') )->setData( ['gear_id' => $gear->product_id, 'redirect_url' => route('mygears::edit', [$gear->product_id])] )->setSuccess()->display();
    }

    /**
     * Handles upload gear photos
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-30T13:13:38+0800]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function uploadGearPhotos( Request $request ){
        if( $request->hasFile('gearphoto') && $request->has('gear_id') ){
            $productID             = $request->input('gear_id');
            $theLogo               = $request->file('gearphoto');
            $gearPhotoOriginalName = $theLogo->getClientOriginalName();
            $gearPhotoName         = substr(str_shuffle(config('gp_conf.alpha_num')), 0, 20) .'.'. $theLogo->getClientOriginalExtension();
            $gearPhtotoStoragePath = config('gp_conf.gear_photo_url');
            $gearPhotoLocation     = $gearPhtotoStoragePath . $gearPhotoName;

            if( file_exists($gearPhtotoStoragePath) ) {
                // upload file
                $theLogo->move($gearPhtotoStoragePath, $gearPhotoName);

                if( file_exists($gearPhotoLocation) ) {
                    $photo = ProductsPhotos::create([
                        "photo_id"       => Hlpr::getUID(),
                        "product_id"     => $productID,
                        "photo_title"    => pathinfo( $gearPhotoOriginalName, PATHINFO_FILENAME ),
                        "photo_filename" => $gearPhotoName,
                        "created_at"     => Carbon::now(),
                    ]);

                    // Create the product's primary photo
                    Products::where('product_id', $productID)->whereNull('product_primary_photo')->update(['product_primary_photo' => $photo->photo_id]);

                    // create thumbnail 200x200
                    $image = new ImageResize($gearPhotoLocation);
                    $image->crop(200, 200);
                    $image->save( $gearPhtotoStoragePath. 'thumbs' .config('gp_conf.ds'). $gearPhotoName);          
                }

                return response()->json(["status" => 'success', "message" => 'Successfully uploaded.', "photo" => view('layouts.gears.single-photo', ['photo_id' => $photo->photo_id, 'photo_filename' => $photo->photo_filename])->render(), "filename" => $gearPhotoName]);
            }
            return response()->json(["status" => 'error', "message" => trans('messages.storage_not_found')]);
        }
        return response()->json(["status" => 'error', "message" => 'No file(s) to upload or cannot find item']);
    }

    /**
     * Save user search
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-08T16:02:02+0800]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function saveResults( Request $request ){
        if( Auth::check() ){
            if( ($request->has('url') && $request->url != '') && ($request->has('keyword') && $request->keyword != '') ){
                $searchDetails = UsersSavedSearch::where('keyword', 'like', '%'. $request->keyword .'%')->whereUrl($request->url)->whereCreatedBy(Auth::user()->user_id)->first();

                if( count($searchDetails) > 0 ){
                    UsersSavedSearch::where(['search_id' => $searchDetails->search_id, "created_by" => Auth::user()->user_id])->update([
                        "keyword"    => $request->keyword,
                        "url"        => $request->url,
                        "updated_at" => Carbon::now()
                    ]);
                }
                else {
                    UsersSavedSearch::create([
                        "search_id"  => Hlpr::getUID(),
                        "keyword"    => $request->keyword,
                        "url"        => $request->url,
                        "created_by" => Auth::user()->user_id,
                        "created_at" => Carbon::now(),
                        "updated_at" => Carbon::now()
                    ]);
                }
                return Hlpr::handleResponse( trans('messages.api.search.saved'), false, ['html' => view('marketplace.save-search-results', ['searches' => UsersSavedSearch::whereCreatedBy(Auth::user()->user_id)->orderBy('updated_at', 'desc')->get()])->render()]);
            }
            return Hlpr::handleResponse('URL and keyword is required');
        }
        return Hlpr::handleResponse('Please login to continue');
    }

    /**
     * [getSavedResults description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-10-25T11:59:54+0800]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getSavedResults( Request $request ) {
        return Hlpr::handleResponse( trans('messages.api.search.saved'), false, ['html' => view('marketplace.saved-search-results', ['searches' => UsersSavedSearch::whereCreatedBy(Auth::user()->user_id)->orderBy('updated_at', 'desc')->get()])->render()]);
    }

    /**
     * Remove user's save search
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-26T09:44:06+0800]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function removeSearch( Request $request ){
        if( $request->has('id') && $request->id ){
            $wasRemoved = UsersSavedSearch::where(['search_id' => $request->id])->delete();
            
            if( $wasRemoved ) {
                $search = UsersSavedSearch::whereCreatedBy(Auth::user()->user_id)->orderBy('updated_at', 'desc');
                $response = array('total' => $search->count());

                if( $search->count() == 0 )
                    $response['html'] = view('marketplace.save-search-results', ['searches' => $search->get()])->render();

                return Hlpr::handleResponse('Successfully removed.', false, $response);
            }
        }
        return Hlpr::handleResponse('Unable to remove saved search');
    }
}
