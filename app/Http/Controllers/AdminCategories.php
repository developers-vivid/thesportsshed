<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Http\Requests;
use App\Categories;

class AdminCategories extends Controller
{
  /**
   * [__construct description]
   * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
   * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-10T11:58:55+0800]
   */
  public function __construct(){
    $this->middleware('auth');
  }

  /**
   * List of categories
   * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
   * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-11T14:01:59+0800]
   * @return [type] [description]
   */
  public function index(){
    $categories = Categories::orderBy('name', 'asc')->paginate(20);
    return view('admin.categories.list', compact('categories'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(){
    $categories = Categories::whereParent('-')->orderBy('name', 'asc')->get();
    return view('admin.categories.create', compact('categories'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request){
    $this->validate($request, [
      "category_name" => 'required'
    ]);

    Categories::create([
      "category_id" => \App\Helper::getUID(),
      "name"        => $request->category_name,
      "slug"        => str_slug($request->category_name),
      "description" => $request->category_name,
      "parent"      => $request->category_parent,
      "created_at"  => Carbon::now(),
    ]);
    \App\Helper::handleResponse('New category has been created.', false, [], true);

    return redirect( route('admin::categories') );
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit( $categoryID ){
    $categories = Categories::whereParent('-')->orderBy('name', 'asc')->get();
    $category = Categories::whereCategoryId( $categoryID )->first();
    return view('admin.categories.edit', compact('categories', 'category'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update( Request $request, $categoryID ){
    $this->validate($request, [
      "category_name" => 'required'
    ]);

    Categories::where([
      'category_id' => $categoryID
    ])->update([
      "name"        => $request->category_name,
      "slug"        => str_slug($request->category_name),
      "description" => $request->category_description,
      "parent"      => $request->category_parent
    ]);

    \App\Helper::handleResponse('Category has been updated.', false, [], true);

    return redirect( route('admin::categories') );
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id){
    //
  }
}
