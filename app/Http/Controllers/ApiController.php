<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Auth;
use Validator;
use Image;
use Carbon\Carbon;
use Eventviva\ImageResize;
use Vinkla\Pusher\PusherManager;

use App\Http\Requests;

use \App\GpLog as Log;
use App\Helper as Hlpr;

use App\User;
use App\UsersProfile;
use App\UsersCompany;
use App\UsersMessages;
use App\UsersSavedSearch;
use App\Products;
use App\ProductsPhotos;
use App\ProductsCondition;
use App\ProductsSpecs;
use App\ProductsShipping;
use App\ProductsShippingCost;

class ApiController extends Controller {
    protected $postParams, $data, $wsUser, $apiAccessError, $apiName, $log, $pusher;

    /**
     * [__construct description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-04-27T14:01:38+0800]
     */
    public function __construct( PusherManager $pusher ) {
        $this->pusher = $pusher;
    	$this->middleware('authapi');
    }

    /**
     * Handles web services
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-24T13:08:40+0800]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getWebServiceResponse(Request $request){
        $this->postParams = json_decode($request->getContent());
        $this->data       = $this->postParams->data;
        $method           = studly_case(str_replace("-", "_", $this->postParams->api));
        $this->apiName    = $this->postParams->api;

        // Log
        $this->log = new Log('web-service');

        // Method to execute
        $this->log->info('Start exec ['. $this->apiName .'] method');

        if( count(config('gp_conf.require_token')) > 0 && in_array($method, config('gp_conf.require_token')) ){
            if( ! \Input::has('api_token') || \Input::get('api_token') == '' ) {
                $this->apiAccessError =  $this->apiResponse($this->postParams->api . ' requires token');
            }
            else {
                // check user validity
                $this->validateUser(\Input::get('api_token'));
            }
        }
        else if( \Input::has('api_token') && \Input::get('api_token') != '' ) {
            $this->validateUser(\Input::get('api_token'));
        }

        if( !is_null($this->apiAccessError) )
            return $this->apiAccessError;

        $method = 'gPlnt'.$method;
        if( method_exists($this, $method) )
            return $this->$method();
        else
            return $this->apiResponse(ucfirst($this->postParams->api) .' method does not exist', true, $this->data);
    }

    /**
     * Validate a user
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-25T11:34:35+0800]
     * @param  [type] $apiToken [description]
     * @return [type]           [description]
     */
    public function validateUser( $apiToken ){
        // check user validity
        $userDetails = User::whereApiToken(\Input::get('api_token'))->first();

        if( count($userDetails) > 0 && !is_null($userDetails) ) {
            if( $userDetails->role_id != '56aebc46f2827' ) {
                $this->apiAccessError = $this->apiResponse('You cannot perform this operation. Role must be a user.', true, $this->data);
            }
            elseif( ! is_null($userDetails->verification_token) || $userDetails->status == 'declined' ) {
                $this->apiAccessError =  $this->apiResponse('Account was declined.', true, $this->data);
            }
            elseif( ! is_null($userDetails->verification_token) || $userDetails->status != 'active' ) {
                $this->apiAccessError =  $this->apiResponse('Account was not yet verified.', true, $this->data);
            }
            else {
                $this->wsUser = User::getFullDetails($userDetails->user_id);
            }
        }
        else {
            $this->apiAccessError =  $this->apiResponse('Invalid API token.', true, $this->data);
        }
    }

    /**
     * [GetCategories description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-24T08:42:27+0800]
     */
    public function gPlntGetCategories(){
    	return $this->apiResponse('Categories has been loaded.', false, ['categories' => \App\Helper::getCategories()]);
    }

    /**
     * Gear gears by category
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-24T11:27:18+0800]
     * @return [type] [description]
     */
    public function gPlntGetGears(){
        return $this->apiResponse('Gears has been loaded.', false, Hlpr::spliceArr(Products::getGearsByCategory( $this->data->category, ($this->data->sub_category != '' ? $this->data->sub_category : null), $this->data->type, $this->data->location, $this->data->brand, $this->data->price_min, $this->data->price_max, $this->data->sort, true ), $this->data->page));
    }

    /**
     * Search gears
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-24T11:28:26+0800]
     */
    public function gPlntSearchGears(){
        return $this->apiResponse('Gears has been loaded.', false, Hlpr::spliceArr(Products::searchGears( $this->data->keyword, $this->data->type, $this->data->location, $this->data->brand, $this->data->price_min, $this->data->price_max, $this->data->sort, true), $this->data->page));
    }

    /**
     * To sign in a user
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-24T11:47:50+0800]
     * @return [type] [description]
     */
    public function gPlntSignIn(){
        $fields = ['email'    => 'required|email'];

        if( $this->data->type == 'local' )
            $fields['password'] = 'required';

        $validator = Validator::make((array)$this->data, $fields);

        $error = $validator->errors()->all();

        if( count($error) > 0 ) {
            return $this->apiResponse( $error[0], true, $this->data );
        }
        else {
            if( $this->data->email != '' && $this->data->type == 'local' && $this->data->password != '' ){
                if( Auth::once(['email' => $this->data->email, 'password' => $this->data->password, 'role_id' => '56aebc46f2827', 'status' => 'active']) )
                    return $this->apiResponse('User has been successfully signed in', false, User::getFullDetails(Auth::user()->user_id));
                else
                    return $this->apiResponse('These credentials do not match our records', true, $this->data);
            }
            else {
                $user = User::whereEmail($this->data->email)->first();
                
                if( $user && Auth::loginUsingId($user->user_id) )
                    return $this->apiResponse('User has been successfully signed in', false, User::getFullDetails(Auth::user()->user_id));
                else
                    return $this->apiResponse('These credentials do not match our records', true, $this->data);
            }
        }
    }

    /**
     * To sign up a user
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-24T13:57:36+0800]
     * @return [type] [description]
     */
    public function gPlntSignUp(){
        $validationRequest = array(
            'first_name' => 'required|max:100',
            'last_name'  => 'required|max:100',
            'email'      => 'required|email|max:255|unique:users'
        );

        if( $this->data->type == 'local' )
            $validationRequest['password'] = 'required|confirmed|min:6';

        if( ! config('gp_conf.allseller') && $this->data->seller == 'yes' ){
            $validationRequest['company_name']      = 'required|min:2|max:255';
            $validationRequest['company_state']     = 'required|max:10';
            $validationRequest['company_post_code'] = 'required|max:10';
        }

        $validator = Validator::make( (array)$this->data, $validationRequest );
        $error = $validator->errors()->all();

        if( count($error) > 0 ) {
            return $this->apiResponse( $error[0], true, $this->data );
        }
        else {
            if( $this->data->agreed == 'yes' ){
                $userLog = new Log('users');

                // create user account
                $userArr = [
                    "user_id"    => Hlpr::getUID(),
                    "name"       => ($this->data->first_name .' '. $this->data->last_name),
                    "email"      => $this->data->email,
                    "login_type" => $this->data->type,
                    "api_token"  => substr(str_shuffle(config('gp_conf.alpha_num')), 0, 60),
                    "role_id"    => '56aebc46f2827',    // users/customers
                    "seller"     => (config('gp_conf.allseller') ? 'yes' : $this->data->seller),
                    "created_at" => Carbon::now()
                ];

                if( $this->data->type == 'local' ) {
                    $userArr['password'] = bcrypt($this->data->password);
                    $userArr['verification_token'] = substr(str_shuffle(config('gp_conf.alpha_num')), 0, 30);                    
                }
                else {
                    $userArr['status'] = 'active';
                }

                $user = User::create($userArr);

                // create user profile
                $userProfile = UsersProfile::create([
                    "profile_id" => Hlpr::getUID(),
                    "user_id"    => $user->user_id,
                    "first_name" => $this->data->first_name,
                    "last_name"  => $this->data->last_name,
                    "telephone"  => $this->data->phone,
                    "mobile"     => $this->data->mobile,
                    "created_at" => Carbon::now()
                ]);

                if( config('gp_conf.allseller') || $this->data->seller == 'yes' ) {
                    $userCompany = UsersCompany::create([
                        "company_id" => Hlpr::getUID(),
                        "user_id"    => $user->user_id,
                        "name"       => (isset($this->data->company_name) && $this->data->company_name != '' ? $this->data->company_name : 'no company'),
                        "city"       => $this->data->company_city,
                        "state"      => $this->data->company_state,
                        "post_code"  => $this->data->company_post_code,
                        "phone"      => $this->data->phone,
                        "created_at" => Carbon::now()
                    ]);
                }

                $userLog->info('User has been successfully created: ['. $user->user_id .']['. $user->first_name .' '. $user->last_name .']');

                // send verification email
                try{
                    if( ! config('gp_conf.isLocal') ) {
                        Mail::send('mail.new-account-registered', ['user' => $user], function ($m) use ($user) {
                            $m->from('gearplanet@info.com.au', 'Gear Planet');

                            if( config('gp_conf.allseller') ) {
                                $m->to($user->email, $user->name)->subject('Account Registered');
                            }
                            else {
                                $m->to('richmund@sushidigital.com.au', $user->name)->subject('Account Registered');
                                // $m->bcc('ivane@sushidigital.com.au', 'Ivane Gesite');
                                // $m->bcc('richmundlofranco@gmail.com', 'Richmund Lofranco');                                
                            }
                        });
                    }
                } catch (Exception $e){
                    $userLog->error('Email not sent: '. $e->getMessage());
                }

                $userLog->endLog();
                
                return $this->apiResponse( 'You have successfully registered', false );
            }
            else {
                return $this->apiResponse('You need to agree to the terms of use and privacy policy', true, $this->data);
            }
        }
    }

    /**
     * Users ads/gears
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-24T16:44:12+0800]
     * @return [type] [description]
     */
    public function gPlntMyAds(){
        return $this->apiResponse("User's ads/gears has been loaded", false, Hlpr::spliceArr(Products::getMyAds($this->data->status, $this->data->type, $this->data->location, $this->data->brand, $this->data->price_min, $this->data->price_max, $this->data->sort, $this->wsUser->user_id, true), $this->data->page));
    }

    /**
     * Sell gear
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-25T09:59:28+0800]
     * @return [type] [description]
     */
    public function gPlntSellGear(){
        $validationRequest = array(
            "category"        => 'required',
            // sub_category is optional
            "condition"       => 'required',
            "brand"           => 'required|max:100',
            "model"           => 'required|max:100',
            "year"            => 'required|max:4',
            "colour"          => 'required|max:30',
            "made"            => 'required|max:100',
            "type"            => 'required',
            "listing_title"   => 'required|max:255|unique:products,product_blurb',
            "price"           => 'required',
            "sale_price"      => 'required',
            "paypal_email"    => 'required|email|max:255',
            "description"     => 'required|min:10',
            "shopping_policy" => 'required|min:10',
            "shipping_city"   => 'required|max:100',
            "shipping_state"  => 'required|max:100',
            "shipping_policy" => 'required|min:10',
            "shipping_method" => 'required',
            "shipping_cost"   => 'required',
            "photos"          => 'required',
            "agree"           => 'required'
        );

        if( ! is_null($this->apiAccessError) || (! \Input::has('api_token') || \Input::get('api_token') == '') ) {
            $validationRequest['account.first_name'] = 'required|max:100';
            $validationRequest['account.last_name']  = 'required|max:100';
            $validationRequest['account.email']      = 'required|email|max:255|unique:users';
            $validationRequest['account.password']   = 'required';
        }

        if( $this->data->listing_title != '' )
            $this->data->listing_title = str_slug($this->data->listing_title, "-");

        $validator = Validator::make((array)$this->data, $validationRequest);
        $error = $validator->errors()->all();

        if( count($error) > 0 ) {
            return $this->apiResponse( $error[0], true, $this->data );
        }
        else {
            if( $this->data->agree == 'yes' ) {
                $createdAt = Carbon::now();
                $userID = 0;

                if( isset($this->wsUser->user_id) ) {
                    $userID = $this->wsUser->user_id;
                }
                else {
                    $user = User::create([
                        "user_id"    => Hlpr::getUID(),
                        "name"       => ($this->data->account->first_name .' '. $this->data->account->last_name),
                        "email"      => $this->data->account->email,
                        "password"   => bcrypt($this->data->account->password),
                        "login_type" => 'local',
                        "api_token"  => substr(str_shuffle(config('gp_conf.alpha_num')), 0, 60),
                        "status"     => 'inactive',
                        "role_id"    => '56aebc46f2827',    // as user by default
                        "seller"     => 'yes'
                    ]);
                    $userID = $user->user_id;
                }

                // create product/gear
                $gear = Products::create([
                    'product_id'          => Hlpr::getUID(),
                    'product_title'       => $this->data->listing_title,
                    'product_blurb'       => str_slug($this->data->listing_title, "-"),
                    'product_description' => $this->data->description,
                    'product_price'       => $this->data->price,
                    'product_sale_price'  => $this->data->sale_price,
                    'accept_offers'       => $this->data->accept_offers,
                    'paypal_email'        => $this->data->paypal_email,
                    'shopping_policy'     => $this->data->shopping_policy,
                    'created_by'          => $userID,
                    'created_at'          => $createdAt
                ]);

                // photos
                if( $this->data->photos != '' && count($this->data->photos) > 0 ) {
                    for($i = 0; $i < count($this->data->photos); $i++){
                        $ext = Hlpr::getMimeTypeFromBase64( $this->data->photos[$i] );
                        if( ! is_null( $ext ) ) {
                            $img           = Image::make($this->data->photos[$i]);
                            $gearPhotoName = substr(str_shuffle(config('gp_conf.alpha_num')), 0, 20) .'.'. $ext;
                            $gearPath      = config('gp_conf.gear_photo_url');
                            $gearImage     = $gearPath . $gearPhotoName;

                            // save image
                            $img->save($gearImage);

                            if( file_exists( $gearImage ) ) {
                                $photo = ProductsPhotos::create([
                                    "photo_id"       => Hlpr::getUID(),
                                    "product_id"     => $gear->product_id,
                                    "photo_title"    => $gear->product_title .' - '. $i,
                                    "photo_filename" => $gearPhotoName,
                                    "created_at"     => Carbon::now(),
                                ]);

                                // Create the product's primary photo
                                Products::where('product_id', $gear->product_id)->whereNull('product_primary_photo')->update(['product_primary_photo' => $photo->photo_id]);
                                
                                // create thumbnail 200x200
                                $image = new ImageResize($gearImage);
                                $image->crop(200, 200);
                                $image->save( $gearPath . 'thumbs' .config('gp_conf.ds'). $gearPhotoName);  
                            }
                        }
                    }                    
                }

                // create product specs
                $gearSpec = ProductsSpecs::create([
                    'product_specs_id'      => Hlpr::getUID(),
                    'product_id'            => $gear->product_id,
                    'product_condition'     => $this->data->condition,
                    'product_brand'         => $this->data->brand,
                    'product_model'         => $this->data->model,
                    // 'product_finish'     => $this->data->finish,
                    'product_categories'    => $this->data->category,
                    'product_subcategories' => $this->data->sub_category,
                    'product_year'          => $this->data->year,
                    'product_colour'        => $this->data->colour,
                    'product_made_in'       => $this->data->made,
                    'type'                  => $this->data->type,
                    'created_at'            => $createdAt
                ]);

                // shipping
                $productShippingInfo = ProductsShipping::create([
                    "product_shipping_id" => Hlpr::getUID(),
                    "product_id"          => $gear->product_id,
                    "country"             => 'Australia',
                    "city"                => $this->data->shipping_city,
                    "state"               => $this->data->shipping_state,
                    "policy"              => $this->data->shipping_policy,
                    "method"              => $this->data->shipping_method,
                    "created_at"          => $createdAt
                ]);

                // Shipping Cost
                if( isset($this->data->shipping_cost) && $this->data->shipping_cost != '' ) {
                    if( count($this->data->shipping_cost) > 0 ) {
                        foreach((array)$this->data->shipping_cost as $index => $cost) {
                            ProductsShippingCost::create([
                                "product_shipping_cost_id" => Hlpr::getUID(),
                                "product_id" => $gear->product_id,
                                "location" => $cost->location,
                                "cost" => (float)$cost->cost,
                                "created_at" => $createdAt
                            ]);
                        }
                    }
                }

                return $this->apiResponse( trans('messages.gear_created'), false, ['gear_id' => $gear->product_id] );
            }
            else {
                return $this->apiResponse('You need to agree to the terms of use and privacy policy', true, $this->data);
            }
        }
    }

    /**
    * To get App settings
    * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
    * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-25T10:09:25+0800]
    * @return [type] [description]
    */
    public function gPlntSettings(){
        return $this->apiResponse('Settings has been loaded.', false, [
            'mode'       => env('APP_ENV'),
            // 'msgevnt' => config('gp_conf.pusher.new_message.'. strtolower(env('APP_ENV'))),
            'pusher_key' => config('gp_conf.pusher_id'),
            'msgcntr'   => [
                'channel' => config('gp_conf.pusher_msg_counter_id'),
                'event'   => config('gp_conf.pusher.msg_counter.'. strtolower(env('APP_ENV')))
            ],
            'sort'       => config('gp_conf.sort'),
            'brands'     => Hlpr::getBrands(),
            'conditions' => ProductsCondition::getConditionsForApp(), 
            'countries'  => Hlpr::getCountries(), 
            'states'     => Hlpr::getStates(), 
            'categories' => \App\Helper::getCategories()
        ]);
    }

    /**
     * Gear details
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-14T09:46:06+0800]
     * @return [type] [description]
     */
    public function gPlntGearDetails() {
        $validator = Validator::make((array)$this->data, ["product_blurb" => 'required']);
        $error = $validator->errors()->all();

        if( count($error) > 0 ) {
            return $this->apiResponse( $error[0], true, $this->data );
        }
        else {
            $gear = Products::getProductDetails(null, $this->data->product_blurb);
            if( is_object($gear) && count((array)$gear) > 0 ) {
                $relatedItems = Products::getRelatedItems( $gear->product_id, $gear->category_id );
                return $this->apiResponse( 'Gear details has been loaded', false, ['gears_details' => $gear, 'related_items' => $relatedItems] );
            }
            else {
                return $this->apiResponse( "Gear does not exist", true, $this->data );
            }
        }
    }

    /**
     * To contact seller
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T10:15:42+0800]
     * @return [type] [description]
     */
    public function gPlntContactSeller(){
        $validate = $this->validateRequest(["seller_id" => 'required', "message" => 'required', "socket_id" => 'required']);

        if( count($validate) > 0 )
            return $this->apiResponse( $validate[0], true, $this->data );

        return \App\Messaging::contactSeller( $this->pusher, $this->wsUser->user_id, $this->data->seller_id, $this->wsUser->name, $this->data->socket_id, $this->data->message, 'contact-seller' );
    }

    /**
     * To send message
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T13:40:44+0800]
     * @return [type] [description]
     */
    public function gPlntSendMessage() {
        $validate = $this->validateRequest(["inbox_id" => 'required', "seller_id" => 'required', "message" => 'required', "socket_id" => 'required']);

        if( count($validate) > 0 )
            return $this->apiResponse( $validate[0], true, $this->data );

        return \App\Messaging::sendMessage( $this->pusher, $this->data->inbox_id, $this->wsUser->user_id, $this->data->seller_id, $this->wsUser->name, $this->data->socket_id, $this->data->message, true, 'send-message' );
    }

    public function gPlntKeepOrders(){
        
    }

    /**
     * User's inbox
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-14T14:07:53+0800]
     * @return [type] [description]
     */
    public function gPlntInbox() {
        $userInbox = UsersMessages::getUserInbox( null, $this->wsUser->user_id );
        $inbox = [];

        if( count($userInbox) > 0 ) {
            foreach($userInbox as $inbx) {
                $userDetails = User::whereUserId( ($this->wsUser->user_id == $inbx->receiver_id ? $inbx->sender_id : $inbx->receiver_id ) )->first();
                $inbox[] = array(
                    "inbox_id" => $inbx->inbox_id,
                    "channel_id" => $inbx->channel_id,
                    "sender_id" => $inbx->sender_id,
                    "photo" => url('images/thumb-pic.png'),
                    "name" => $userDetails->name,
                    "email" => $userDetails->email
                );
            }
        }

        return $this->apiResponse( trans('messages.api.user.inbox'), false, array('event' => config('gp_conf.pusher.new_message.'. strtolower(env('APP_ENV'))), 'inbox' => $inbox) );
    }

    /**
     * [gPlntInboxMessages description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-14T16:58:30+0800]
     * @return [type] [description]
     */
    public function gPlntInboxMessages(){
        $validate = $this->validateRequest(["inbox_id" => 'required']);

        if( count($validate) > 0 )
            return $this->apiResponse( $validate[0], true, $this->data );
        
        $totalUnread = UsersMessages::whereInboxId( $this->data->inbox_id )->whereReceiverId( $this->wsUser->user_id )->whereStatus('un-read')->count();
        
        $messages = UsersMessages::getInboxMessages( $this->data->inbox_id, null, true );

        // update to read
        if( $totalUnread > 0 )
            UsersMessages::where(['inbox_id' => $this->data->inbox_id, 'receiver_id' => $this->wsUser->user_id, 'status' => 'un-read'])->update(['status' => 'read', 'updated_at' => Carbon::now()]);
        
        return $this->apiResponse( trans('messages.api.user.inbox_message'), false, array('unread_messages' => $totalUnread, 'messages' => $messages) );
    }

    /**
     * [gPlntInboxMessageDetails description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-18T15:09:33+0800]
     * @return [type] [description]
     */
    public function gPlntInboxMessageDetails(){
        $validate = $this->validateRequest(["message_id" => 'required']);

        if( count($validate) > 0 )
            return $this->apiResponse( $validate[0], true, $this->data );

        $messageDetails = UsersMessages::getInboxMessages( null, $this->data->message_id, true );
        if( $messageDetails && $messageDetails[0]->status == 'unread' )
            UsersMessages::where(['message_id' => $this->data->message_id])->update(['status' => 'read', 'updated_at' => Carbon::now()]);

        return $this->apiResponse( trans('messages.api.user.message_details'), false, ($messageDetails ? $messageDetails[0] : []) );
    }

    /**
     * [gPlntSaveSearch description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-19T15:04:50+0800]
     * @return [type] [description]
     */
    public function gPlntSaveSearch() {
        $validate = $this->validateRequest(["url" => 'required|url', "keyword" => 'required']);

        if( count($validate) > 0 )
            return $this->apiResponse( $validate[0], true, $this->data );

        $searchDetails = UsersSavedSearch::where('keyword', 'like', '%'. $this->data->keyword .'%')->whereUrl($this->data->url)->whereCreatedBy($this->wsUser->user_id)->first();
        if( count($searchDetails) > 0 ){
            UsersSavedSearch::where(['search_id' => $searchDetails->search_id, "created_by" => $this->wsUser->user_id])->update([
                "keyword"    => $this->data->keyword,
                "url"        => $this->data->url,
                "updated_at" => Carbon::now()
            ]);
        }
        else {
            UsersSavedSearch::create([
                "search_id"  => Hlpr::getUID(),
                "keyword"    => $this->data->keyword,
                "url"        => $this->data->url,
                "created_by" => $this->wsUser->user_id,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now()
            ]);
        }

        return $this->apiResponse( trans('messages.api.search.saved'), false );
    }

    /**
     * [gPlntDeleteSearch description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-19T15:19:14+0800]
     * @return [type] [description]
     */
    public function gPlntDeleteSearch(){
        $validate = $this->validateRequest(["search_id" => 'required']);

        if( count($validate) > 0 )
            return $this->apiResponse( $validate[0], true, $this->data );

        $userDetails = UsersSavedSearch::where(['search_id' => $this->data->search_id, 'created_by' => $this->wsUser->user_id])->first();
        if( $userDetails ){
            UsersSavedSearch::where(['search_id' => $this->data->search_id, 'created_by' => $this->wsUser->user_id])->delete();
            return $this->apiResponse( trans('messages.api.search.deleted'), false );
        }
        return $this->apiResponse( trans('messages.api.search.not_exist') );
    }

    /**
     * [gPlntSearchList description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-19T15:35:57+0800]
     * @return [type] [description]
     */
    public function gPlntSearchList() {
        return $this->apiResponse( trans('messages.api.search.loaded'), false, UsersSavedSearch::whereCreatedBy($this->wsUser->user_id)->orderBy('updated_at', 'desc')->get() );
    }

    /**
     * To validate request
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-15T13:27:34+0800]
     * @param  [type] $condition [description]
     * @return [type]            [description]
     */
    private function validateRequest( $condition ){
        $validator = Validator::make((array)$this->data, $condition);
        return $validator->errors()->all();
    }


    //-----------------------------------------------//

    /**
     * Handles API Response
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-27T11:39:11+0800]
     * @param  string  $message [description]
     * @param  boolean $error   [description]
     * @param  array   $data    [description]
     * @param  string  $apiName [description]
     * @return [type]           [description]
     */
    public function apiResponse( $message = 'Oops! Something went wrong.', $error = true, $data = array() ){
        $type = ($error ? 'error' : 'success');
        $this->log->$type($message);

        $this->log->info('End exec ['. $this->apiName .'] method')->endLog();

        return response()->json(array(
            "status"  => ($error ? 'error' : 'success'),
            "api"     => $this->apiName,
            "message" => $message,
            "data"    => $data
        ));
    }


    /*
    |--------------------------------------------------------------------------
    | Reserve for testing purposes only
    |--------------------------------------------------------------------------
    */
    public function gPlntReList(){
        $validate = $this->validateRequest(["product_blurb" => 'required']);

        if( count($validate) > 0 )
            return $this->apiResponse( $validate[0], true, $this->data );

        $wasReList = Products::where(['product_blurb' => $this->data->product_blurb])->update(['status' => 'active', 'sold_at' => null, 'updated_at' => Carbon::now()]);
        return $this->apiResponse( ($wasReList ? 'Product has been re-listed' : 'Unable to re-list the product.' ), $wasReList );
    }
   

    /**
     * Send Email
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-12T14:01:58+0800]
     * @return [type] [description]
     */
    public function sendEmail(){
        // send verification email
        $user = User::whereEmail($this->postParams->data->email_address)->first();
        
        try{
            if( ! config('gp_conf.isLocal') ) {
                Mail::send('mail.new-account-registered', ['user' => $user], function ($m) use ($user) {
                    $m->from('gearplanet@info.com.au', 'Gear Planet');
                    $m->to($user->email, $user->name)->subject('New Account Registered');
                });
            }
        } catch (Exception $e){
          // dd($e->getMessage());
        }

        return $this->apiResponse( 'You have successfully registered', false );
    }
}
