<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;
use Auth;
use Carbon\Carbon;

// helper
use App\Helper as Hlpr;
use App\GpLog as Log;
use App\Settings as Settings;

use App\User;
use App\UsersProfile;
use App\UsersCompany;

class ApiUsersController extends Controller {
    protected $data, $response, $user, $notification;

	/**
	 * [__construct description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T08:31:08+0800]
	 * @param  Request $request [description]
	 */
    public function __construct( Request $request ){
        $content = json_decode($request->getContent());
        $this->data = $content->data;
        $this->response = new \App\Message( (isset($content->api) && $content->api != '' ? $content->api : '') );

        if( $request->has('api_token') && $request->api_token != '' )
            $this->user = User::getFullDetails( Auth::guard('api')->user()->user_id );

        // $this->notification = new \App\Notif();
    }
    	
    /**
     * [postSignIn description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T08:31:13+0800]
     * @return [type] [description]
     */
	public function postSignIn() {
        $validationFields = ['email'    => 'required|email'];

        if( $this->data->type == 'local' )
            $validationFields['password'] = 'required';

        $validator = Validator::make((array)$this->data, $validationFields);

        $error = $validator->errors();

        if( count($error) > 0 )
            return $this->response->setMessage( $error->first() );

        if( $this->data->email != '' && $this->data->type == 'local' && $this->data->password != '' ){
            if( Auth::once(['email' => $this->data->email, 'password' => $this->data->password, 'role_id' => '56aebc46f2827', 'status' => 'active']) )
            	return $this->response->setMessage( trans('messages.api.user.login') )->setData( User::getFullDetails(Auth::user()->user_id) )->setSuccess()->display();
            else
            	return $this->response->setMessage( trans('messages.api.user.not_found') )->display();
        }
        else {
            $user = User::whereEmail( $this->data->email )->first();
            
            if( $user && Auth::loginUsingId($user->user_id) )
            	return $this->response->setMessage( trans('messages.api.user.login') )->setData( User::getFullDetails(Auth::user()->user_id) )->setSuccess()->display();
            else
            	return $this->response->setMessage( trans('messages.api.user.not_found') )->display();
        }
	}

	/**
	 * [postSignUp description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-04T08:58:46+0800]
	 * @return [type] [description]
	 */
	public function postSignUp() {
		$validationRequest = array(
            'first_name' => 'required|max:100',
            'last_name'  => 'required|max:100',
            'email'      => 'required|email|max:255|unique:users'
        );

        if( $this->data->type == 'local' )
            $validationRequest['password'] = 'required|confirmed|min:6';

        if( ! config('gp_conf.allseller') && $this->data->seller == 'yes' ){
            $validationRequest['company_name']      = 'required|min:2|max:255';
            $validationRequest['company_state']     = 'required|max:10';
            $validationRequest['company_post_code'] = 'required|max:10';
        }

        $validator = Validator::make( (array)$this->data, $validationRequest );
        $error = $validator->errors();

        if( count($error) > 0 )
        	return $this->response->setMessage( $error->first() )->display();

        if( $this->data->agreed == 'yes' ){
            $userLog = new Log('users');

            // create user account
            $userArr = [
                "user_id"    => Hlpr::getUID(),
                "name"       => ($this->data->first_name .' '. $this->data->last_name),
                "email"      => $this->data->email,
                "login_type" => $this->data->type,
                "api_token"  => substr(str_shuffle(config('gp_conf.alpha_num')), 0, 60),
                "role_id"    => '56aebc46f2827',    // users/customers
                "seller"     => (config('gp_conf.allseller') ? 'yes' : $this->data->seller),
                "created_at" => Carbon::now()
            ];

            if( $this->data->type == 'local' ) {
                $userArr['password'] = bcrypt($this->data->password);
                $userArr['verification_token'] = substr(str_shuffle(config('gp_conf.alpha_num')), 0, 30);                    
            }
            else {
                $userArr['status'] = 'active';
            }

            $user = User::create($userArr);

            $userSetting = new Settings( $user->user_id );
            $userSetting->setMessageNotification( 'on' )->save();

            // create user profile
            $userProfile = UsersProfile::create([
                "profile_id" => Hlpr::getUID(),
                "user_id"    => $user->user_id,
                "first_name" => $this->data->first_name,
                "last_name"  => $this->data->last_name,
                "telephone"  => $this->data->phone,
                "mobile"     => $this->data->mobile,
                "created_at" => Carbon::now()
            ]);

            if( config('gp_conf.allseller') || $this->data->seller == 'yes' ) {
                $userCompany = UsersCompany::create([
                    "company_id" => Hlpr::getUID(),
                    "user_id"    => $user->user_id,
                    "name"       => (isset($this->data->company_name) && $this->data->company_name != '' ? $this->data->company_name : 'no company'),
                    "city"       => $this->data->company_city,
                    "state"      => $this->data->company_state,
                    "post_code"  => $this->data->company_post_code,
                    "phone"      => $this->data->phone,
                    "created_at" => Carbon::now()
                ]);
            }

            $userLog->info('User has been successfully created: ['. $user->user_id .']['. $user->first_name .' '. $user->last_name .']');

            // send verification email
            if( $this->data->type == 'local' ) {
	            try{
	                if( ! config('gp_conf.isLocal') ) {
	                    Mail::send('mail.new-account-registered', ['user' => $user], function ($m) use ($user) {
	                        $m->from('gearplanet@info.com.au', 'Gear Planet');

	                        if( config('gp_conf.allseller') ) {
	                            $m->to($user->email, $user->name)->subject('Account Registered');
	                        }
	                        else {
	                            $m->to('richmund@sushidigital.com.au', $user->name)->subject('Account Registered');
	                            // $m->bcc('ivane@sushidigital.com.au', 'Ivane Gesite');
	                            // $m->bcc('richmundlofranco@gmail.com', 'Richmund Lofranco');                                
	                        }
	                    });
	                }
	            } catch (Exception $e){
	                $userLog->error('Email not sent: '. $e->getMessage());
	            }            	
            }

            $userLog->endLog();
            
            $this->response->setMessage( trans('messages.api.user.sign_up.'. ($this->data->type == 'local' ? 'local' : 'social_media') ) )->setSuccess();

            if( $this->data->type != 'local' )
            	$this->response->setData( User::getFullDetails( $user->user_id ) );

            return $this->response->display();
        }
        else
        	return $this->response->setMessage( trans('messages.api.agreement') )->display();
	}

}
