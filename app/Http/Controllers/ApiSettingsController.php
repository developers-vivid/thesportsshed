<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use Validator;
use Carbon\Carbon;

use App\User;
use App\Build;

class ApiSettingsController extends Controller {
    protected $data, $response, $user, $notification;

	/**
	 * [__construct description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-24T08:49:49+0800]
	 */
    public function __construct( Request $request ){
        $content = json_decode($request->getContent());
        $this->data = $content->data;
        $this->response = new \App\Message( (isset($content->api) && $content->api != '' ? $content->api : '') );

        if( $request->has('api_token') && $request->api_token != '' )
            $this->user = User::getFullDetails( Auth::guard('api')->user()->user_id );

        // $this->notification = new \App\Notif();
    }

    /**
     * [showSettings description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-03T14:00:42+0800]
     * @return [type] [description]
     */
    public function showSettings(){
    	return $this->response->setMessage( trans('messages.api.settings.all') )->setData([
    		"env" => config('app.env'),
            "timezone" => config('app.timezone'),
    		"api" => [
    			"version" => env('API_VERSION', 1),
    			"url" => env('APP_URL_'. strtoupper(env('APP_ENV', 'local')), 'LOCAL') .'api/v'. env('API_VERSION', 1) .'/',
    		],
            "can" => config('gp_conf.can'),
    		"images" => [
    			"gear" => [
    				"original" => env('APP_URL_'. strtoupper(env('APP_ENV', 'local')), 'LOCAL') . 'images/gears/',
    				"thumbs" => env('APP_URL_'. strtoupper(env('APP_ENV', 'local')), 'LOCAL') . 'images/gears/thumbs',
    			],
    		],
            'mode'       => env('APP_ENV'),
            'pusher_key' => config('gp_conf.pusher_id'),
            'msgcntr'    => [
                'channel' => config('gp_conf.pusher_msg_counter_id'),
                'event' => config('gp_conf.pusher.msg_counter.'. strtolower(env('APP_ENV')))
            ],
            'sort'       => config('gp_conf.sort'),
            'brands'     => \App\Helper::getBrands( false ),
            'conditions' => \App\ProductsCondition::getConditionsForApp(), 
            'countries'  => \App\Helper::getCountries(), 
            'states'     => \App\Helper::getStates(), 
            'categories' => \App\Helper::getCategories(),
            'order_statuses' => \App\OrdersStatus::getStatuses()
    	])->setSuccess()->display();
    }

    /**
     * [getCategories description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-03T13:32:19+0800]
     * @return [type] [description]
     */
    public function getCategories(){
    	return $this->response->setMessage( trans('messages.api.settings.categories_loaded') )->setData( \App\Helper::getCategories() )->setSuccess()->display();
    }

    /**
     * [updateAppBuildVersion description]
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-12T11:22:50+0800]
     * @return [type] [description]
     */
    public function updateAppBuildVersion() {
        $validator = Validator::make((array)$this->data, ["device" => 'required', "version" => 'required']);
        $error = $validator->errors();

        if( count($error) > 0 )
            return $this->response->setMessage( $error->first() )->display();

        // allows `android` and `ios` only
        if( !in_array(strtolower( $this->data->device ), ['android', 'ios']) )
            return $this->response->setMessage( trans('messages.api.request.device.invalid') )->display();

        if( Auth::guard('api')->user()->cannot('update-build') )
            return $this->response->setMessage( trans('messages.api.request.build.update.not_allowed') )->display();

        // do update
        $updated = Build::where(['device' => $this->data->device])->update(['version' => $this->data->version, 'updated_at' => Carbon::now()]);

        if( $updated )
            return $this->response->setMessage( trans('messages.api.request.build.update.success') )->display();
        else
            return $this->response->setMessage( trans('messages.api.request.build.update.denied') )->display();
    }

}
