<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Socialite;
use Auth;
use Mail;
use Session;
use Validator;
use Carbon\Carbon;
use Eventviva\ImageResize;

use App\User;
use App\UsersProfile;
use App\UsersCompany;
use App\PasswordResets;
use App\UserSettings;

use App\Helper as Hlpr;
use App\Settings as Settings;

class AuthUserController extends Controller
{
  protected $response;

	/**
	 * [__construct description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-09T09:27:51+0800]
	 */
	public function __construct(){
		$this->middleware('auth', ['except' => [
      'redirectToProvider', 
      'handleProviderCallback', 
      'authenticateUser', 
      'getLoginModalForm', 
      'authenticateAdmin', 
      'showAdminAuthenticationForm', 
      'postRegister',
      'getLogout',
      'verifyUserAccount',

      // new
      'postLocalSignUp',
      'resendVerificationEmail'
    ]]);

    $this->response = new \App\Message();
	}

  /**
   * To verify user account
   * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
   * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-16T10:15:59+0800]
   * @return [type] [description]
   */
  public function verifyUserAccount(){
    if( \Input::has('token') && \Input::get('token') != '' ) {
      $userDetails = User::where('verification_token', \Input::get('token'))->whereStatus('inactive')->first();
      $verifiedByAdmin = (\Input::has('admin') && \Input::get('admin') == "true");
      // dd($userDetails);
      if( count($userDetails) > 0 ) {
        $verified = User::where(['user_id' => $userDetails->user_id])->update(['verification_token' => null, 'status' => 'active', 'updated_at' => Carbon::now()]);
        
        // Send account verified email
        try{
          if( ! config('gp_conf.isLocal') ) {
            Mail::send('mail.account-verified', ['user' => $userDetails], function ($m) use ($userDetails) {
              $m->from('gearplanet@info.com.au', 'Gear Planet');
              $m->to($userDetails->email, $userDetails->name)->subject('Account Verified');
              if( !config('gp_conf.allseller') ) {
                // $m->bcc('richmundlofranco@gmail.com', 'Richmund Lofranco');
              }
            });
          }
        } catch (Exception $e){
          // dd($e->getMessage());
        }

        Auth::loginUsingId( $userDetails->user_id );

        $msg = new \App\Message();
        $msg->setMessage(($verified ? (($verifiedByAdmin ? 'User' : 'Your account') . ' has been successfully verified.') : 'Unable to verify '.($verifiedByAdmin ? ' user account.' : 'your account.').' Please try again.'))
          ->setSuccess()
          ->flash();

        return redirect()->route('home');
      }
    }

    abort(404);
  }

  /**
   * To decline user account
   * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
   * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-16T11:14:24+0800]
   * @return [type] [description]
   */
  public function declineUserAccount(){
    if( \Input::has('token') && \Input::get('token') != '' ) {
      $userDetails = User::where('verification_token', \Input::get('token'))->whereStatus('inactive')->first();

      if( count($userDetails) > 0 ) {
        $declined = User::where(['user_id' => $userDetails->user_id])->update(['verification_token' => '', 'status' => 'declined', 'updated_at' => Carbon::now()]);
        
        // Send account declined email
        try{
          if( ! config('gp_conf.isLocal') ) {
            Mail::send('mail.account-declined', ['user' => $userDetails], function ($m) use ($userDetails) {
              $m->from('gearplanet@info.com.au', 'Gear Planet');
              $m->to($userDetails->email, $userDetails->name)->subject('Account Declined');
              if( !config('gp_conf.allseller') ) {
                // $m->bcc('richmundlofranco@gmail.com', 'Richmund Lofranco');
              }
            });
          }
        } catch (Exception $e){
          // dd($e->getMessage());
        }

        return view('messages.account-verification', [
          "title"   => ($declined ? 'Account Declined' : 'Error'),
          "message" => ($declined ? 'Account has been successfully declined.' : 'Unable to decline user account'),
          "status"  => $declined
        ]);
      }
    }

    abort(404);
  }

  /**
   * Handle local sign up
   * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
   * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-12T13:51:43+0800]
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function postLocalSignUp( Request $request ){
    $validationRequest = [
      'first_name' => 'required|min:3',
      'last_name'  => 'required|min:3',
      'email'      => 'required|email|max:255|unique:users',
      'password'   => 'required|confirmed|min:6'
    ];

    if( !config('gp_conf.allseller') && ($request->has('registered_seller') && $request->get('registered_seller') == "on") ) {
      $validationRequest['company_name'] = 'required';
      $validationRequest['company_city'] = 'required';
    }

    $validator = Validator::make((array)$request->all(), $validationRequest);

    $error = $validator->errors()->all();

    if( count($error) > 0 ) {
      return Hlpr::handleResponse( $error[0], true, ['target' => Hlpr::getFirstArrayOfErrorMessageBag($validator->errors()->toArray())] );
    }
    else {
      if( $request->has('agree') && (int)$request->get('agree') == 1 ) {

        // create user account
        $user = User::create([
          "user_id"            => Hlpr::getUID(),
          "name"               => ($request->first_name .' '. $request->last_name),
          "email"              => $request->email,
          "password"           => bcrypt($request->password),
          "login_type"         => 'local',
          "api_token"          => substr(str_shuffle(config('gp_conf.alpha_num')), 0, 60),
          "role_id"            => '56aebc46f2827',
          "seller"             => (config('gp_conf.allseller') ? 'yes' : (( $request->has('registered_seller') && $request->get('registered_seller') == "on" ) ? 'yes' : 'no') ),
          "verification_token" => substr(str_shuffle(config('gp_conf.alpha_num')), 0, 30),
          "created_at"         => Carbon::now()
        ]);

        $userSetting = new Settings( $user->user_id );
        $userSetting->setMessageNotification( 'on' )->save();


        // create user profile
        $userProfile = UsersProfile::create([
          "profile_id" => Hlpr::getUID(),
          "user_id"    => $user->user_id,
          "first_name" => $request->first_name,
          "last_name"  => $request->last_name,
          "telephone"  => $request->telephone,
          "mobile"     => $request->mobile,
          "created_at" => Carbon::now()
        ]);

        if( config('gp_conf.allseller') || ($request->has('registered_seller') && $request->get('registered_seller') == "on") ) {
          $userCompany = UsersCompany::create([
            "company_id" => Hlpr::getUID(),
            "user_id"    => $user->user_id,
            "name"       => ($request->has('company_name') && $request->company_name != '' ? $request->company_name : 'no company'),
            "abn"        => $request->company_abn,
            "street"     => $request->company_street,
            "city"       => $request->company_city,
            "state"      => $request->company_state,
            "post_code"  => $request->company_post_code,
            "phone"      => $request->telephone,
            "created_at" => Carbon::now()
          ]);
        }

        // send verification email
        try{
          if( ! config('gp_conf.isLocal') ) {
            Mail::send('mail.verify-account', ['user' => $user], function ($m) use ($user) {
              $m->from('gearplanet@info.com.au', 'Gear Planet');
              
              if( config('gp_conf.allseller') ) {
                $m->to($user->email, $user->name)->subject('Account Registered');
              }
              else {
                $m->to('richmund@sushidigital.com.au', $user->name)->subject('Account Registered');
                // $m->bcc('richmundlofranco@gmail.com', 'Richmund Lofranco');                
              }
            });
          }
        } catch (Exception $e){
          // dd($e->getMessage());
        }

        return $this->response->setMessage( trans('messages.user_signup.signedup') )->setData(['signedup' => true])->setSuccess()->flash()->display();
      }
      else {
        return Hlpr::handleResponse( trans('messages.user.must_agree.on_signup') );
      }
    }
  }

  public function showAdminAuthenticationForm(){
    if( Auth::check() )
      return redirect('dashboard');
    else
      return view('auth.admin-login');
  }

  public function getLoginModalForm( Request $request ){
    return response()->json(['html' => view('auth.modal-login', ['is_cart' => ($request->has('is_cart') && (string)$request->is_cart == 'true'), 'redirectUrl' => $request->redirect_url])->render()]);
  }

	/**
	 * Handle local user authentication
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-09T09:27:54+0800]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function authenticateUser( Request $request ){
        $validator = Validator::make((array)$request->all(),[
            'email'    => 'required|email',
            'password' => 'required',
        ]);

        $error = $validator->errors()->all();

        if( count($error) > 0 ) {
            return Hlpr::handleResponse( $error[0], true );
        }
        else {
            $userDetails = User::whereEmail($request->input('email'))->first();

            if( $userDetails ) {
                if( $userDetails->status == 'inactive' ){
                    return Hlpr::handleResponse(trans('messages.user_not_activated'));
                }
                else if( $userDetails->status == 'declined' ) {
                    return Hlpr::handleResponse(trans('messages.user_declined'));
                }
                else if( $userDetails->role_id != '56aebc46f2827' ) { // means not a user
                    return response()->json(['status' => 'error', 'message' => trans('messages.user_type_not_allowed')]);
                } 
                else {
                    if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
                        $prevUrl = Session::get('_previous.url');
                        $isVerificationPage = strpos($prevUrl, 'verify-account');
                        return Hlpr::handleResponse( trans('messages.user_signup.loggedin') , false, ['redirect' => ($isVerificationPage !== false ? route('mygears::ads') : $prevUrl)]);
                        // return Hlpr::handleResponse('Successfully login. Redirecting...', false, ['redirect' => route('mygears::ads')]);
                    }
                    return Hlpr::handleResponse(trans('messages.account_not_match'));
                }                
            }
            else {
                return Hlpr::handleResponse(trans('messages.account_not_match'));
            }
        }
    }

  /**
   * Handle local user authentication
   * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
   * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-09T09:27:54+0800]
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function authenticateAdmin( Request $request ){
    $this->validate($request, [
      'email'    => 'required|email',
      'password' => 'required',
    ]);

    $userDetails = User::whereEmail($request->email)->first();
    if( $userDetails ) {
      if( $userDetails->status == 'inactive' ) {
        return redirect()->back()->withInput()->withErrorMessage(trans('messages.account_not_match')); 
      }
      else if( $userDetails->role_id == '56aebc46f2827' ) { // means user
        return redirect()->back()->withInput()->withErrorMessage(trans('messages.user_type_not_allowed')); 
      }
      else {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
          return redirect()->route('admin::dashboard');
        }
        return redirect()->back()->withInput()->withErrorMessage(trans('messages.account_not_match'));        
      }
    }
    else {
      return redirect()->back()->withInput()->withErrorMessage(trans('messages.account_not_match'));
    }
    return redirect()->back()->withInput()->withErrorMessage(trans('messages.account_not_match'));      
  }

  /**
   * [postRegister description]
   * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
   * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-09T09:29:31+0800]
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function postRegister( Request $request ){
  	$validationDetails = [
			'first_name' => 'required|max:100',
			'last_name'  => 'required|max:100',
			'email'      => 'required|email|max:255|unique:users',
			'password'   => 'required|confirmed|min:6',
      'agree'      => 'required',
      'brandname'  => 'required'
    ];

    // attach company validation rules if seller
    if( ! config('gp_conf.allseller') && $request->has('registered_seller') ){
      $validationDetails['company_name'] = 'required';
      $validationDetails['state']        = 'required';
      $validationDetails['company_city'] = 'required';
    }

    $this->validate($request, $validationDetails);

    // upload logo first if has
    $logoName = '';
    if( $request->hasFile('logo') ){
			$theLogo         = $request->file('logo');
			$logoName        = Hlpr::getUID().'.'.$theLogo->getClientOriginalExtension();
			$logoStoragePath = storage_path() .config('gp_conf.ds'). 'company_logos' .config('gp_conf.ds');
			$logoLocation    = $logoStoragePath . $logoName;

  		// upload file
  		$theLogo->move($logoStoragePath, $logoName);
  	}

    \App\Users::createUser($request->all(), null, $logoName);

    return redirect('/register');
  }

  /**
   * Logout
   * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
   * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-11T13:56:08+0800]
   * @return [type] [description]
   */
  public function getLogout(){
    Session::forget('searchview');

    // empty cart
    \App\Cart::whereToken((string)Session::get('_token'))->delete();

  	Auth::logout();
  	return redirect('/');
  }

  /**
   * Controls redirection to provider
   * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
   * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-09T09:35:33+0800]
   * @param  [type] $provider [description]
   * @return [type]           [description]
   */
  public function redirectToProvider( $provider ){
  	if( in_array(strtolower($provider), ['github', 'facebook', 'google']) )
    	return Socialite::driver($provider)->redirect();
    else
    	return abort(404);
  }

  /**
   * Handles the provider callback
   * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
   * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-03-09T09:35:21+0800]
   * @param  [type] $provider [description]
   * @return [type]           [description]
   */
  public function handleProviderCallback( $provider, Request $request ){
    if( $request->has('error') && $request->error == 'access_denied' ) {
      return redirect('/');
    }
    else {
    	try {
  	    $user = Socialite::driver($provider)->user();
        $userDetails = User::whereEmail($user->email)->first();
        
        // create the user if not exist
        if( count($userDetails) == 0 ) {            

            // create user account
            $user = User::create([
                "user_id"    => Hlpr::getUID(),
                "name"       => $user->name,
                "email"      => $user->email,
                "login_type" => $provider,
                "api_token"  => substr(str_shuffle(config('gp_conf.alpha_num')), 0, 60),
                "role_id"    => '56aebc46f2827',
                "status"     => 'active',
                "seller"     => (config('gp_conf.allseller') ? 'yes' : 'no'),
                "created_at" => Carbon::now()
            ]);

            $userSetting = new Settings( $user->user_id );
            $userSetting->setMessageNotification( 'on' )->save();

            // create user profile
            $userProfile = UsersProfile::create([
                "profile_id" => Hlpr::getUID(),
                "user_id"    => $user->user_id,
                "first_name" => $user->name,
                "created_at" => Carbon::now()
            ]);

            if( config('gp_conf.allseller') ) {
              UsersCompany::create([
                "company_id" => \App\Helper::getUID(),
                "user_id"    => $user->user_id,
                "name"       => 'no company',
                "created_at" => Carbon::now(),
              ]);
            }

          $userDetails = $user;
        }
        /*else if( $userDetails->login_type != strtolower($provider) ) {
          abort(403, trans('messages.email_taken'));
        }*/

        // Login User
        Auth::login($userDetails);

        // then, redirect
        return redirect()->route('mygears::ads');
  		}
  		catch (GuzzleHttp\Exception\ClientException $e) {
       	abort(403, $e->response);
  		}      
    }
  }

  public function testUpload( Request $request ){
  	if( $request->hasFile('logo') ){
			$theLogo         = $request->file('logo');
			$logoName        = Hlpr::getUID().'.'.$theLogo->getClientOriginalExtension();
			$logoStoragePath = storage_path() .config('gp_conf.ds'). 'company_logos' .config('gp_conf.ds');
			$logoLocation    = $logoStoragePath . $logoName;

  		// upload file
  		$theLogo->move($logoStoragePath, $logoName);

  		// create thumbnail 200x200
  		$image = new ImageResize($logoLocation);
  		$image->crop(200, 200);
  		$image->save( $logoStoragePath. 'thumbs' .config('gp_conf.ds'). $logoName);

  		dd($request->all());
  	}
  	else {
  		dd('No logo');
  	}
  }

  /**
  * [forgotPasswordSendMail description]
  * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
  * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-07-22T11:38:11+0800]
  * @param  Request $request [description]
  * @return [type]           [description]
  */
  public function resendVerificationEmail( Request $request ) {
    $validator = Validator::make((array)$request->all(),[
      'email'    => 'required|email',
      'password' => 'required',
    ]);

    $error = $validator->errors()->all();

    if( count($error) > 0 ) {
      return Hlpr::handleResponse( $error[0], true );
    } else {
      $user = User::whereEmail( $request->email )->first();

      if( $user ) {
        
        $userUpdate = User::whereEmail(['email' => $request->email])->update([
          'verification_token' => substr(str_shuffle(config('gp_conf.alpha_num')), 0, 30),
          'updated_at' => Carbon::now()
        ]);

        $userDetails = User::getFullDetailsEmail($user->email); 

        // send verification email
        try{
          if( ! config('gp_conf.isLocal') ) {
            Mail::send('mail.verify-account', ['user' => $userDetails], function ($m) use ($userDetails) {
              $m->from('gearplanet@info.com.au', 'Gear Planet');

              if( config('gp_conf.allseller') ) {
                $m->to($userDetails->email, $userDetails->name)->subject('Account Verification Email');
              } else {
                $m->to('richmund@sushidigital.com.au', $userDetails->name)->subject('Account Verification Email');               
              }
            });
          }
        } catch (Exception $e){
        }

        return $this->response->setMessage( trans('messages.resend_verification_email') )->setSuccess()->display();
      }
    }
  }

}
