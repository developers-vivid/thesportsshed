<?php

Route::group(['middleware' => 'web'], function () {
  Route::auth();

  Route::group(['as' => 'newsletter::'], function(){
    Route::post('/subscribe', ['as' => 'subscribe', 'uses' => 'NewsletterController@subscribeUser']);
  });

  Route::get('/search', ['as' => 'search', 'uses' => 'MarketPlaceController@displaySearchedItems']);

  /*
  |--------------------------------------------------------------------------
  | Home Routes
  |--------------------------------------------------------------------------
  */
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
    Route::get('/splash', ['as' => 'splash', 'uses' => 'HomeController@splash']);

  /*
  |--------------------------------------------------------------------------
  | Pages Routes
  |--------------------------------------------------------------------------
  */
    Route::group(['as' => 'pages::'], function(){
      Route::get('/about-us', ['as' => 'aboutUs', 'uses' => 'PagesController@showAboutUs']);
      Route::get('/blogs', ['as' => 'blogs', 'uses' => 'PagesController@showBlogs']);
      Route::get('/faq', ['as' => 'faqs', 'uses' => 'PagesController@showFaqs']);
      Route::get('/why-use-gear-planet', ['as' => 'whyUseGP', 'uses' => 'PagesController@showWhyUseGP']);
      Route::get('/policy', ['as' => 'policy', 'uses' => 'PagesController@showPolicy']);
      Route::get('/shipping', ['as' => 'shipping', 'uses' => 'PagesController@showShipping']);
      Route::get('/packing-items', ['as' => 'packingItems', 'uses' => 'PagesController@showPackingItems']);
      Route::get('/gear-photography', ['as' => 'gearPhotography', 'uses' => 'PagesController@showGearPhotography']);
      Route::get('/terms-and-conditions', ['as' => 'termsAndCondition', 'uses' => 'PagesController@showGearTermsAndCondition']);
      Route::get('/selling-music-gear', ['as' => 'sellingMusicGear', 'uses' => 'PagesController@showSellingMusicGear']);
    });

  /*
  |--------------------------------------------------------------------------
  | Page Redirection Confirmation Routes
  |--------------------------------------------------------------------------
  */

    Route::group(['as' => 'messages::'], function(){
      Route::get('/signed-up', ['as' => 'messageSuccess', 'uses' => 'PagesController@showMessageSuccess']);
    });

  /*
  |--------------------------------------------------------------------------
  | Cart Routes
  |--------------------------------------------------------------------------
  */
  Route::group(['as' => 'cart::'], function(){
    Route::get('/cart', ['as' => 'my-cart', 'uses' => 'CartController@showCartForm']);    
    Route::post('/cart/add', ['as' => 'add', 'uses' => 'CartController@postAddToCart']);
    Route::post('/cart/buy', ['as' => 'add', 'uses' => 'CartController@postAddToCart']);
    Route::post('/cart/update-qty', ['as' => 'update-cart-qty', 'uses' => 'CartController@updateProductQty']);
    Route::post('/cart/remove-product', ['as' => 'remove-product', 'uses' => 'CartController@removeProductFromCart']);
    Route::post('/cart/checkout', ['as' => 'checkout', 'uses' => 'CartController@checkoutCart']);

    Route::get('/txn/{order_id}/done/{token}', ['as' => 'done', 'uses' => 'CartController@getTransactionDone']);
    Route::get('/txn/{order_id}/cancel/{token}', ['as' => 'cancel', 'uses' => 'CartController@getTransactionCancel']);
    Route::get('/txn/{order_id}/get-transaction-details/{token}', ['as' => 'get-transaction-details', 'uses' => 'CartController@getTransaction']);
  });

  /*
  |--------------------------------------------------------------------------
  | Users Authentication and Registration Routes
  |--------------------------------------------------------------------------
  */
    Route::post('/get-login', 'AuthUserController@getLoginModalForm');
    Route::post('/post-login', 'AuthUserController@authenticateUser');
    Route::post('/sign-up', 'AuthUserController@postRegister');
    Route::get('/sign-out', 'AuthUserController@getLogout');
    Route::get('/verify-account', 'AuthUserController@verifyUserAccount');
    Route::get('/decline-account', 'AuthUserController@declineUserAccount');
    Route::post('/resend-verification', ['as' => 'resend-verification', 'uses' => 'AuthUserController@resendVerificationEmail']);
    Route::group(['as' => 'sl::'], function(){
      Route::post('/process-signup', ['as' => 'register', 'uses' => 'AuthUserController@postLocalSignUp']);
    });

    Route::group(['as' => 'user::'], function(){
        Route::get('/accounts/forgot-password', ['as' => 'forgotPassword', 'uses' => 'UsersController@forgotPassword']);
        Route::post('/accounts/forgot-password/send-mail', ['as' => 'forgotPassSendMail', 'uses' => 'UsersController@forgotPasswordSendMail']);
        Route::get('/accounts/password/reset/confirm/{token}', ['as' => 'changePass', 'uses' => 'UsersController@showChangePassForm']);
        Route::post('/accounts/password/reset/confirm/{token}/update', ['as' => 'updateAccountPassword', 'uses' => 'UsersController@updateChangePass']);
    });

  /*
  |--------------------------------------------------------------------------
  | Admin Authentication Routes
  |--------------------------------------------------------------------------
  */
    Route::get('/admin', 'AuthUserController@showAdminAuthenticationForm');
    Route::post('/admin', 'AuthUserController@authenticateAdmin');

  /*
  |--------------------------------------------------------------------------
  | Social Login Routes
  |--------------------------------------------------------------------------
  */
    Route::get('/auth/{provider}', 'AuthUserController@redirectToProvider');
    Route::get('/auth/{provider}/callback', 'AuthUserController@handleProviderCallback');

  /*
  |--------------------------------------------------------------------------
  | MarketPlace Routes
  |--------------------------------------------------------------------------
  */
    Route::group(['as' => 'gears::'], function () {
      Route::get('/sell', ['as' => 'sell', 'uses' => 'MarketPlaceController@showCreateGearForm']);
      Route::post('/gears/create', ['as' => 'create', 'uses' => 'MarketPlaceController@postNewGear']);
      Route::post('/gears/upload-photos', ['as' => 'uploadgearphotos', 'uses' => 'MarketPlaceController@uploadGearPhotos']);
      Route::post('/gears/update-gear-photos', ['as' => 'updateGearPhotos', 'uses' => 'MarketPlaceController@uploadGearPhotos']);

      Route::get('/gear/{gear_title}', ['as' => 'marketplace', 'uses' => 'MarketPlaceController@showMarketPlaceGearDetails']);
      Route::get('/bzr/{category}/{sub_category?}', ['as' => 'gearmarketplace', 'uses' => 'MarketPlaceController@showAllGears']);
    });

  /*
  |--------------------------------------------------------------------------
  | Search Routes
  |--------------------------------------------------------------------------
  */
    Route::group(['as' => 'search::'], function(){
      Route::post('/search/save-results', ['as' => 'save', 'uses' => 'MarketPlaceController@saveResults']);
      Route::post('/search/get-save-results', ['as' => 'getSavedResults', 'uses' => 'MarketPlaceController@getSavedResults']);
      Route::post('/search/remove-search', ['as' => 'remove', 'uses' => 'MarketPlaceController@removeSearch']);
    });



  /*
  |--------------------------------------------------------------------------
  | User's Gears Routes
  |--------------------------------------------------------------------------
  */
    Route::group(['as' => 'mygears::'], function () {
      // Route::get('/mygears/list/', ['as' => 'list',    'uses' => 'GearController@myGears']);
      Route::get('/mygears/{gear_id}/edit', ['as' => 'edit',    'uses' => 'GearController@showEditGearForm']);
      Route::get('/mygears/{gear_id}/details', ['as' => 'details', 'uses' => 'GearController@showGearDetails']);
      Route::post('/mygears/update-gear', ['as' => 'updateGear', 'uses' => 'GearController@updateGear']);
      Route::post('/mygears/photo/primary', ['as' => 'primary-photo', 'uses' => 'GearController@selectPrimaryPhoto']);
      Route::post('/mygears/photo/remove', ['as' => 'remove-photo', 'uses' => 'GearController@removePhoto']);
      Route::get('/my-ads', ['as' => 'ads', 'uses' => 'AdsController@myAds']);

      Route::get('/mygears/{productId}/activate', ['as' => 'activateAd', 'uses' => 'GearController@activateProduct']);
      Route::get('/mygears/{productId}/cancel', ['as' => 'cancelAd', 'uses' => 'GearController@cancelProduct']);
      Route::get('/mygears/{productId}/delete', ['as' => 'deleteAd', 'uses' => 'GearController@deleteProduct']);
    });


  /*
  |--------------------------------------------------------------------------
  | Messages Routes
  |--------------------------------------------------------------------------
  */ 
    Route::group(['as' => 'messages::'], function(){
      Route::get('/messages', ['as' => 'messages', 'uses' => 'MessageController@showMessages']);
      Route::post('/messages/senders', ['as' => 'inbox-senders', 'uses' => 'MessageController@getUserSenders']);
      Route::post('/messages/inbox', ['as' => 'inbox-message', 'uses' => 'MessageController@displayInboxMessages']);
      Route::get('/inbx/{channelId}', ['as' => 'channelMessages', 'uses' => 'MessageController@showChat']);
      Route::post('/messages/send', ['as' => 'send-message', 'uses' => 'MessageController@sendMessage']);
      Route::post('/messages/received', ['as' => 'received-message', 'uses' => 'MessageController@getSingleMessage']);
      Route::post('/messages/contact-seller', ['as' => 'contact-seller', 'uses' => 'MessageController@showContactSellerForm']);
      Route::post('/messages/send-message-to-seller', ['as' => 'send-msg-to-seller', 'uses' => 'MessageController@sendMessageToSeller']);
      Route::post('/messages/get-message-counter', ['as' => 'message-counter', 'uses' => 'MessageController@getMessageCounter']);
    });

  /*
  |--------------------------------------------------------------------------
  | Orders Routes
  |--------------------------------------------------------------------------
  */
    Route::group(['as' => 'order::'], function(){
      // Route::get('orders/cancel')
    });

  /*
  |--------------------------------------------------------------------------
  | Users Routes
  |--------------------------------------------------------------------------
  */
    Route::get('/account-settings', ['as' => 'user-profile', 'uses' => 'UsersController@myProfile']);
    Route::post('/save-settings', ['as' => 'save-settings', 'uses' => 'UsersController@saveSettings']);

  /*
  |--------------------------------------------------------------------------
  | Testing Purposes Routes
  |--------------------------------------------------------------------------
  */
    Route::post('/upload-logo', 'AuthUserController@testUpload');

  /*
  |--------------------------------------------------------------------------
  | User's saved search settings
  |--------------------------------------------------------------------------
  */
  Route::group(['as' => 'settings::'], function(){
    Route::post('/settings/change-search-view', ['as' => 'searchview', 'uses' => 'SettingsController@changeSearchView']);
    Route::post('/settings/get-subcategory', ['as' => 'get-subcategory', 'uses' => 'SettingsController@getSubCategory']);
  });

  Route::group(['prefix' => sprintf('/logs/v%d', env('APP_VERSION', 1))], function(){
    Route::get( sprintf('/%s', config('gp_conf.access.logs')), '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::get( sprintf('/%s/{logFile}/clean', config('gp_conf.access.logs')), function($logFile){
      if( strtolower( $logFile ) == "all" ) {
        if( count(($files = array_values(array_diff(scandir( ($logPath = storage_path('/logs')) ), array('.', '..'))))) > 0 ) {
          foreach( $files as $file ) {
            $filePath = $logPath .DIRECTORY_SEPARATOR. $file;
            $ext = pathinfo($filePath, PATHINFO_EXTENSION);
            if( strtolower($ext) == "log" && file_exists( $filePath ) ) {
              $byteSize = sprintf('%u', filesize($filePath));
              $logFileHandler = @fopen( $filePath, "r+");
              @ftruncate($logFileHandler, 0);
              \Log::info( 'Done cleaning ('. $filePath. ') of a size ('. number_format( $byteSize ). ') bytes' );
            }
          }
        }
      }
      else {
        $logFile = storage_path('logs/'.$logFile.'.log');
        if( file_exists( $logFile ) ) {
          $byteSize = sprintf('%u', filesize($logFile));
          $logFileHandler = @fopen( $logFile, "r+");
          @ftruncate($logFileHandler, 0);
          \Log::info( 'Done cleaning ('. $logFile. ') of a size ('. number_format( $byteSize ). ') bytes' );
        }     
      }
    });
  });
 
});


Route::group(['middleware' => ['web','admin'], 'prefix' => 'admin'], function () {
  Route::auth();


  Route::group(['as' => 'admin::'], function(){
    /*
    |--------------------------------------------------------------------------
    | Admin Dashboard Routes
    |--------------------------------------------------------------------------
    */
      Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'AdminController@index']);

    /*
    |--------------------------------------------------------------------------
    | Admin Users Routes
    |--------------------------------------------------------------------------
    */
      Route::get('/users',                    ['as' => 'users',            'uses' => 'AdminController@getUsers']);
      Route::get('/users/register',           ['as' => 'user-register',          'uses' => 'AdminController@userRegister']);
      Route::get('/users/{userid}/change',    ['as' => 'change-user-status',     'uses' => 'AdminController@updateUserStatus']);

    /*
    |--------------------------------------------------------------------------
    | Admin Categories Routes
    |--------------------------------------------------------------------------
    */
      Route::get('/categories', ['as' => 'categories', 'uses' => 'AdminCategories@index']);
      Route::get('/category/create', ['as' => 'create-category', 'uses' => 'AdminCategories@create']);
      Route::post('/categories/create', ['as' => 'post-category', 'uses' => 'AdminCategories@store']);
      Route::get('/category/{category_id}/edit', ['as' => 'edit-category', 'uses' => 'AdminCategories@edit']);
      Route::put('/category/{category_id}/update', ['as' => 'update-category', 'uses' => 'AdminCategories@update']);

      Route::get('/blogs', ['as' => 'blogs', 'uses' => 'AdminBlogsController@index']);
      Route::match(['get', 'post'], '/blogs/create', ['as' => 'createBlog', 'uses' => 'AdminBlogsController@createBlog']);
      Route::match(['get', 'post'], '/blogs/{blogId}/update', ['as' => 'updateBlog', 'uses' => 'AdminBlogsController@updateBlog']);
      Route::post('/blogs/{blogId}/remove', ['as' => 'deleteBlog', 'uses' => 'AdminBlogsController@deleteBlog']);
  });

});

Route::group(['prefix' => 'api/v1'], function () {
  Route::post('/web-service', 'ApiController@getWebServiceResponse');
});

/*
|--------------------------------------------------------------------------
| API Reference Routes
|--------------------------------------------------------------------------
*/
Route::group(['as' => 'api::'], function(){
  Route::get('/api/docs/', ['as' => 'apiReference', 'uses' => 'ApiReferenceController@index']);
  Route::any('/api/docs/{method}', ['as' => 'apiReferenceDetails', 'uses' => 'ApiReferenceController@showApiDetails']);
});


/*
|--------------------------------------------------------------------------
| API v1.1 Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'api/doc'], function(){
    Route::any('/{api?}', ['as' => 'apiDocHome', 'uses' => 'ApiRefController@home']);
});

// settings
Route::post('/EoUvsyjX0xYcSpIRWTew3ni9GrJda7PhmAZt6OKCgHuN4B2LfV', ['as' => 'settings', 'uses' => 'ApiSettingsController@showSettings']);

// Version 1.1 API Route
Route::group(['prefix' => 'api/v'. env('API_VERSION', 1.1), 'middleware' => 'apiAccess'], function () {
    /*
    |--------------------------------------------------------------------------
    | Settings Routes
    |--------------------------------------------------------------------------
    */
    Route::post('/categories', ['as' => 'categories', 'uses' => 'ApiSettingsController@getCategories']);

    /*
    |--------------------------------------------------------------------------
    | Gears Routes
    |--------------------------------------------------------------------------
    */
    Route::post('/gears', ['as' => 'gears', 'uses' => 'ApiGearsController@getGearsList']);
    Route::post('/gears/search', ['as' => 'searchGears', 'uses' => 'ApiGearsController@getSearchedGear']);
    Route::post('/gears/my-ads', ['as' => 'myAds', 'uses' => 'ApiGearsController@getMyGears']);
    Route::post('/gears/details', ['as' => 'gearDetails', 'uses' => 'ApiGearsController@getGearDetails']);
    Route::post('/gears/sell', ['as' => 'sellGear', 'uses' => 'ApiGearsController@sellGear']);
    Route::post('/gears/update', ['as' => 'updateGear', 'uses' => 'ApiGearsController@updateGear']);
    Route::post('/gears/cancel', ['as' => 'cancelGear', 'uses' => 'ApiGearsController@cancelGear']);
    Route::post('/gears/delete', ['as' => 'deleteGear', 'uses' => 'ApiGearsController@deleteGear']);
    Route::post('/gears/activate', ['as' => 'activateGear', 'uses' => 'ApiGearsController@activateGear']);

    /*
    |--------------------------------------------------------------------------
    | Sign in Routes
    |--------------------------------------------------------------------------
    */
    Route::post('/users/sign-in', ['as' => 'signIn', 'uses' => 'ApiUsersController@postSignIn']);
    Route::post('/users/sign-up', ['as' => 'signUp', 'uses' => 'ApiUsersController@postSignUp']);

    /*
    |--------------------------------------------------------------------------
    | Messages Routes
    |--------------------------------------------------------------------------
    */
    Route::post('/messages/contact/seller', ['as' => 'contactSeller', 'uses' => 'ApiMessageController@contactSeller']);
    Route::post('/messages/send', ['as' => 'sendMessage', 'uses' => 'ApiMessageController@sendAMessage']);
    Route::post('/messages/inbox', ['as' => 'inbox', 'uses' => 'ApiMessageController@getUserInbox']);
    Route::post('/messages/inbox-messages', ['as' => 'inboxMessages', 'uses' => 'ApiMessageController@getUserInboxMessages']);
    Route::post('/messages/details', ['as' => 'inboxMessageDetails', 'uses' => 'ApiMessageController@getUserInboxMessageDetails']);

    /*
    |--------------------------------------------------------------------------
    | Save Search Routes
    |--------------------------------------------------------------------------
    */
    Route::post('/search/save', ['as' => 'saveSearch', 'uses' => 'ApiSearchController@saveASearch']);
    Route::post('/search/delete', ['as' => 'deleteSearch', 'uses' => 'ApiSearchController@deleteSearch']);
    Route::post('/search/list', ['as' => 'searchList', 'uses' => 'ApiSearchController@getSearchList']);

    /*
    |--------------------------------------------------------------------------
    | Order Routes
    |--------------------------------------------------------------------------
    */
    Route::post('/orders/create', ['as' => 'createOrder', 'uses' => 'ApiOrderController@putOrders']);

    /*
    |--------------------------------------------------------------------------
    | Order Routes
    |--------------------------------------------------------------------------
    */
    Route::post('/settings/set-build', ['as' => 'updateBuildVersion', 'uses' => 'ApiSettingsController@updateAppBuildVersion']);

});










/*
|--------------------------------------------------------------------------
| Testing Routes
|--------------------------------------------------------------------------
*/
Route::get('/generate-random-string/{total_string}/{total_copies}', function($totalStr, $totalCopies){
  for( $i = 0; $i < $totalCopies; $i++ ){
    echo substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, $totalStr) .'<br/>';
  }
});

Route::get('/generate-uid/{total_copies}', function($totalCopies){
  for( $i = 0; $i < $totalCopies; $i++ ){
    echo \App\Helper::getUID() .'<br/>';
  }
});

Route::get('/pay-key/{payKey}', function($payKey){
  $pp = new \App\PayPal();
  $response = $pp->getPaymentDetails( $payKey );
  dd($response);
});