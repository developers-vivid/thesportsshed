<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'api/v*',
        'EoUvsyjX0xYcSpIRWTew3ni9GrJda7PhmAZt6OKCgHuN4B2LfV'
    ];
}
