<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AuthenticateApiUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $postParams = json_decode($request->getContent());
        if( isset($postParams->api) && $postParams->api != '' ) {
            if( isset($postParams->data) )
                return $next($request);
            else
                return \App\Helper::handleResponse( 'Missing data object' );
        }
        else {
            return \App\Helper::handleResponse( trans('messages.api.method_not_found'), true, $postParams->data );
        }
    }
}
