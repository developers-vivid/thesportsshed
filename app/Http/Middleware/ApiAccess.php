<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Auth;
use Validator;
use Carbon\Carbon;

class ApiAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $postParams   = json_decode($request->getContent());
        $responseTime = array('datetime' => Carbon::now());
        $msg          = new \App\Message( (( ! isset($postParams->api) || $postParams->api == '' ) ? 'unknown' : $postParams->api) );

        if( strpos($request->headers->get('Content-Type'), 'application/json') !== 0 )
            return $msg->setMessage( trans('messages.api.request.not_json') )->setData(['contentType' => $request->headers->get('Content-Type')])->display();

        if( !is_null($postParams) && is_object($postParams) ) {
            if( ! isset($postParams->api) || $postParams->api == '' )   // required to indentify api and method
                return $msg->setMessage( trans('messages.api.request.unknown_method') )->setData($responseTime)->display();

            if( !in_array(strtolower($postParams->api), ['update-build-version']) &&! isset($postParams->build) )
                return $msg->setMessage( trans('messages.api.request.build.missing') )->setData($responseTime)->display();

            if( ! isset($postParams->data) )    // if has data as index in post params. Can have empty value
                return $msg->setMessage( trans('messages.api.request.no_data') )->setData($responseTime)->display();

            else {
                $method = camel_case( str_replace("-", "_", $postParams->api) );

                if( !in_array(strtolower($postParams->api), ['update-build-version']) ) {
                    // validate build params
                    $validator = Validator::make((array)$postParams->build, ["device" => 'required', "version" => 'required']);
                    $error = $validator->errors();

                    if( count($error) > 0 )
                        return $msg->setMessage( $error->first() )->display();

                    if( !in_array(strtolower( $postParams->build->device ), ['android', 'ios']) )
                        return $msg->setMessage( trans('messages.api.request.device.invalid') )->display();

                    // validate build version
                    $build = \App\Build::whereDevice( strtolower( $postParams->build->device ) )->first();

                    if( ! $build )
                        return $msg->setMessage( trans('messages.api.request.build.unknown') )->display();

                    if( $build && $build->version !== $postParams->build->version )
                        return $msg->setMessage( trans('messages.api.request.build.mismatched') )->setData(['isMatched' => false])->display();
                }

                // please refer to the routes
                if( (string)\Route::currentRouteName() === $method ) {
                    if( ! in_array(strtolower($postParams->api), config('gp_conf.apiguest')) ) {
                        if( $request->has('api_token') && $request->api_token != '' ) {
                            $userDetails = User::whereApiToken((string)$request->api_token)->first();

                            if( $userDetails ) {    // if user exist
                                if( $userDetails->status != 'active' || !is_null( $userDetails->verification_token ) )  // if user is verified
                                    return $msg->setMessage( trans('messages.api.user.not_verified') )->setData($responseTime)->display();

                                else if( !in_array(strtolower($postParams->api), ['update-build-version']) && $userDetails->role_id != '56aebc46f2827' )   // is user role is 56aebc46f2827, means user type
                                    return $msg->setMessage( trans('messages.api.user.role_not_allowed') )->setData($responseTime)->display();
                                    
                                else
                                    return $next($request);
                            }
                            else
                                return $msg->setMessage( trans('messages.api.user.not_found') )->setData($responseTime)->display();
                        }
                        else
                            return $msg->setMessage( trans('messages.api.method.requires_auth_users') )->setData($responseTime)->display();
                    }
                    else
                        return $next($request);
                }
                else
                    return $msg->setMessage( trans('messages.api.method.mismatched') )->setData($responseTime)->display();
            }
        }
        else
            return $msg->setMessage( trans('messages.api.request.invalid') )->setData($responseTime)->display();
    }
}
