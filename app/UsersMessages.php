<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class UsersMessages extends Model {
	protected $table = 'users_messages';
	protected $primaryKey = 'message_id';
	public $incrementing = false;
	protected $softDelete = true;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'message_id', 'inbox_id', 'sender_id', 'receiver_id', 'message',
	];

	protected $dates = [
		'created_at', 'updated_at',
	];

	protected $hidden = [
	];

	/**
     * To get user Inbox
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-10T15:31:55+0800]
     * @param  [type] $inboxId [description]
     * @return [type]          [description]
     */
    public static function getUserInbox( $inboxId = null, $userId = null, $isMobile = false ){
        $query = DB::table('users_messages as um')
                    ->when( ($isMobile), function($whenIsMobile){
                        return $whenIsMobile->select('ui.inbox_id', 'ui.channel_id', 'ui.created_at', 'um.sender_id', 'um.receiver_id', 'rec_user.name as receiver_name', 'rec_user.email as receiver_email');
                    })
                    ->when( (!$isMobile), function($whenNotIsMobile){
                        return $whenNotIsMobile->select('ui.*', 'um.sender_id', 'um.receiver_id', 'rec_user.name as receiver_name', 'rec_user.email as receiver_email');
                    })
                    ->when((!is_null($inboxId)), function($whenInbox) use($inboxId){
                    	return $whenInbox->where('ui.inbox_id', $inboxId);
                    })
                    ->leftJoin('users_inbox as ui', 'um.inbox_id', '=', 'ui.inbox_id')
                    ->where(function($whereUser) use($userId){
                        $whereUser
                            ->where('um.receiver_id', (is_null($userId) ? Auth::user()->user_id : $userId) )
                            ->orWhere('um.sender_id', (is_null($userId) ? Auth::user()->user_id : $userId) );
                    })
                    ->join('users as rec_user', 'um.receiver_id', '=', 'rec_user.user_id')
                    ->groupBy('um.inbox_id')
                    ->orderBy('ui.last_message_received', 'desc')
                    ->get();

        return !is_null($inboxId) ? $query[0] : $query;
    }

    /**
     * To get inbox messages
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-11T09:33:22+0800]
     * @param  [type] $inboxID [description]
     * @return [type]          [description]
     */
    public static function getInboxMessages( $inboxID = null, $messageID = null, $isMobile = false ) {
    	return DB::table("users_messages as um")
                    ->when( ($isMobile), function($whenIsMobile){
                        return $whenIsMobile
                                ->select('um.message_id', 'um.sender_id', 'um.receiver_id', 'um.message', 'um.status', 'um.created_at as sent_at');
                    })
                    ->when( (! $isMobile), function($whenNotIsMobile){
                        return $whenNotIsMobile
                                ->select(
                                    'um.*', 'ui.channel_id', 
                                    'urec.name as receiver_name', 'urec.email as receiver_email',
                                    'usend.name as sender_name', 'usend.email as sender_email'
                                );
                    })
                    ->when((! is_null($messageID)), function($whenMessageId) use($messageID){
                        return $whenMessageId->where('um.message_id', $messageID);
                    })
                    ->when((! is_null($inboxID)), function($whenInboxId) use ($inboxID){
                        return $whenInboxId->where('um.inbox_id', $inboxID);
                    })
    				->leftJoin('users_inbox as ui', 'um.inbox_id', '=', 'ui.inbox_id')
    				->leftJoin('users as urec', 'um.receiver_id', '=', 'urec.user_id')
    				->leftJoin('users as usend', 'um.sender_id', '=', 'usend.user_id')
    				->orderBy('um.created_at', 'asc')
    				->get();
    }

    /**
     * To get inbox details
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-06-16T15:48:46+0800]
     * @param  [type] $senderId   [description]
     * @param  [type] $receiverId [description]
     * @return [type]             [description]
     */
    public static function getInboxDetails( $senderId, $receiverId ){
        $result = DB::table('users_messages as um')
                    ->select('um.*')
                    ->where(function($query) use($senderId, $receiverId){
                        $query->where('um.sender_id', $senderId)->where('um.receiver_id', $receiverId);
                    })
                    ->orWhere(function($query) use($senderId, $receiverId){
                        $query->where('um.sender_id', $receiverId)->where('um.receiver_id', $senderId);
                    })
                    ->get();

        return count($result) > 0 ? $result[0] : null;
    }

    /**
     * To get user's total un-read messages
     * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
     * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-05-16T08:52:07+0800]
     * @return [type] [description]
     */
    public static function getTotalUsersUnReadMessages( $userId = null ){
        $query = DB::table("users_messages as um")
                    // ->when(( is_null($userId) ), function($whenUserId){
                    //     return $whenUserId->where('um.receiver_id', Auth::user()->user_id);
                    // })
                    // ->when(( ! is_null($userId) ), function($whenUserId) use ($userId){
                    //     return $whenUserId->where('um.receiver_id', $userId);
                    // })
                    ->where('um.receiver_id', $userId)
                    ->where('um.status', 'un-read')
                    ->get();

        return count($query);
    }
}
