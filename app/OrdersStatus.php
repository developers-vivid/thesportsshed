<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class OrdersStatus extends Model {
    protected $table = 'orders_status';
	protected $primaryKey = 'order_status_id';
	public $incrementing = false;

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'order_status_id', 'name',
	];

	protected $dates = [
		'created_at', 'updated_at',
	];

	protected $hidden = [

	];

	/**
	 * [getStatuses description]
	 * Developed by Richmund M. Lofranco <richmundlofranco@gmail.com>
	 * @author RichmundLofranco <richmundlofranco@gmail.com> [@date 2016-08-05T09:59:17+0800]
	 * @return [type] [description]
	 */
	public static function getStatuses() {
		return DB::table('orders_status as os')
				->select('os.order_status_id', 'os.name as status_name')
				->orderBy('name', 'asc')
				->get();
	}

}
