var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    // api scss
    mix.sass([
      'fonts/material-icons.scss',
      'fonts/lato.scss',
      'bootstrap/bootstrap.min.scss',
      'api/ie10-viewport-bug-workaround.scss',
      'api/dashboard.scss',
    ], 'public/css/api.css');

    // app.css 
    mix.sass([
    	'app.scss',
      'style.scss',
      'global.scss',
      'inner.scss',
    	'media.scss',
    ], 'public/css/app.css');

    // admin app.css
    mix.sass([
      'app.scss',
    ], 'public/css/admin.css');

    // plugins.css
    mix.sass([
      'plugins/bootstrap-slider.min.scss',
      'plugins/font-awesome.min.scss',
      'plugins/toastr.min.scss',
      'plugins/chosen.scss',
      'plugins/dropzone.min.scss',
      'plugins/slick.scss',
      'plugins/slick-theme.scss',
      'plugins/jquery.bxslider.scss',
      'plugins/jquery.fancybox.scss',
      'plugins/jquery.fancybox-thumbs.scss',
      'plugins/jquery.fancybox-buttons.scss',
      'plugins/trumbowyg.min.scss',
      'plugins/drawer.min.scss',
      'plugins/jquery.mmenu.all.scss',
    ], 'public/css/plugins.css');
    

    // admin-plugins.css
    mix.sass([
      'plugins/font-awesome.min.scss',
      'plugins/toastr.min.scss',
    ], 'public/css/admin-plugins.css');

    // header.js
    mix.scripts([
      'jquery.min.js',
      'bootstrap.min.js',
      'plugins/jquery-form.js',
      'func.js',
    ], 'public/js/master.js');

    // api
    mix.scripts([
      'api/ie-emulation-modes-warning.js',
      'api/holder.min.js',
      'api/ie10-viewport-bug-workaround.js',
    ], 'public/js/api.js');

    // plugins.js
    mix.scripts([
      'plugins/jquery.bxslider.min.js',
      'plugins/bootstrap-slider.min.js',
      'plugins/jquery.mixitup.min.js',
      'plugins/moment-with-locales.js',
      'plugins/livestamp.min.js',
      'plugins/toastr.min.js',
      'plugins/chosen.jquery.js',
      'plugins/dropzone.min.js',
      'plugins/bootbox.js',
      'plugins/slick.min.js',
      'plugins/heightEqualizaer.js',
      'plugins/masonry.pkgd.min.js',
      'plugins/imagesloaded.pkgd.min.js',
      'plugins/jquery.lazyload.js',
      'plugins/trumbowyg.min.js',
      'plugins/jquery.zoom.min.js',
      'plugins/jquery.fancybox.js',
      'plugins/jquery.fancybox-thumbs.js',
      'plugins/jquery.fancybox-buttons.js',
      'plugins/iscroll.min.js',
      'plugins/drawer.min.js',
      'plugins/jquery.mmenu.all.min.js',
    ], 'public/js/plugins.js');

    // admin plugins.js
    mix.scripts([
      'plugins/moment-with-locales.js',
      'plugins/livestamp.min.js',
      'plugins/toastr.min.js',
      'plugins/bootbox.js',
    ], 'public/js/admin-plugins.js');

    // app.js
    mix.scripts([
    	'global.js',
    ], 'public/js/app.js');

    // app.js
    mix.scripts([
      'admin-global.js',
    ], 'public/js/admin-app.js');

    // versioning
    mix.version([
      'css/api.css', 
      'css/app.css', 
      'css/admin.css', 
      'css/plugins.css', 
      'css/admin-plugins.css', 
      'js/master.js', 
      'js/api.js', 
      'js/plugins.js', 
      'js/admin-plugins.js', 
      'js/app.js',
      'js/admin-app.js'
    ]);
});
